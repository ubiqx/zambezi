/* ========================================================================== **
 *                                 smb2_io.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Fundamental I/O operations: read, write, close.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_io.c; 2024-11-25 21:03:56 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <assert.h>       /* For debugging; see assert(3).            */

#include "smb_endian.h"   /* Convert between host and SMB byte order. */
#include "smb2_header.h"  /* SMB2 Header header.                      */
#include "smb2_io.h"      /* Module Header.                           */


/* -------------------------------------------------------------------------- **
 * Functions
 */

int smb2_parseReadReq( uint16_t      const dialect,
                       uint8_t      *const msg,
                       size_t        const msgLen,
                       smb2_ReadReq *const readReq )
  /** Unpack an SMB2 Read Request message.
   *
   * @param[in]   dialect   The dialect to use when parsing the message.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB Read Request message, at
   *                        least 48 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  readReq   A (constant) pointer to an #smb2_ReadReq
   *                        structure into which the message will be parsed.
   *
   * @returns   On error, a negative value is returned.  Otherwise, the
   *            return value will be the number of bytes consumed by parsing
   *            the message.  This will always be 48 bytes.  If the message
   *            includes a ChannelInfoBlock pointer, #smb2_ReadReq.Buffer
   *            will point to the location in \p msg at which the
   *            ChannelInfoBlock begins.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c #smb2_ReadReq.StructureSize
   *                  field did not contain the required value.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain a complete Read Request message.
   *                  A minimum of \c #SMB2_READ_REQ_SIZE bytes are required.
   *
   * \b Notes
   *  - This function expects that the \p msg buffer will contain the entire
   *    fixed portion of the Read Request message; \p msg must point to the
   *    message location immediately following the SMB2 header (offset
   *    64).  It is also expected that \p msgLen will be the length of the
   *    message excluding the header.  Those are stock standard expectations
   *    for this project.
   *  - If the message contains a ChannelInfoBlock, this function will
   *    attempt to set \c readReq->Buffer to point to the start of that
   *    block.  The attempt will succeed IFF:
   *    - The dialect is #SMB2_V0300 or higher.
   *    - The ChannelInfo block follows the fixed portion of the message.
   *    - The ChannelInfo block is fully contained within the message, as
   *      indicated by \p msg and \p msglen.
   *  - Given all of that, if you don't want this function to even look for
   *    the ChannelInfoBlock, set \p msgLen to the size of the fixed length
   *    portion of the Read Request: <tt>#SMB2_READ_REQ_SIZE</tt>.
   *  - For information on the behavior of read operations over RDMA, see the
   *    following:
   *    - <a
   *      href="@msdocs/ms-smb2/21e8b343-34e9-4fca-8d93-03dd2d3e961e">[MS-SMB2;
   *      3.3.5.12]: Receiving an SMB2 READ Request</a>
   *    - <a
   *      href="@msdocs/ms-smbd/c8a3df70-3c52-4ac3-9cb9-acbe2886130a">[MS-SMBD;
   *      3.1.4.5]: RDMA Write to Peer Buffer</a>
   */
  {
  int bufrOffset;

  /* The usual sanity checks. */
  assert( NULL != msg );
  assert( NULL != readReq );
  /* Check buffer size.   */
  if( msgLen < SMB2_READ_REQ_SIZE )
    return( -SMB2_READ_REQ_SIZE );
  /* Test StructureSize.  */
  readReq->StructureSize = smbget16( msg );
  if( readReq->StructureSize != SMB2_READ_REQ_STRUCT_SIZE )
    return( -1 );

  /* Deconstruct the message. */
  readReq->Padding                = msg[2];
  readReq->Flags                  = msg[3];
  readReq->Length                 = smbget32( &msg[4] );
  readReq->Offset                 = smbget64( &msg[8] );
  readReq->FileId                 = &msg[16];
  readReq->MinimumCount           = smbget32( &msg[32] );
  readReq->Channel                = smbget32( &msg[36] );
  readReq->RemainingBytes         = smbget32( &msg[40] );
  readReq->ReadChannelInfoOffset  = smbget16( &msg[44] );
  readReq->ReadChannelInfoLength  = smbget16( &msg[46] );
  readReq->Buffer                 = NULL;

  /* Some obfuscated logic to attempt to set Buffer to point to the
   * ChannelInfo block, if there is one.
   *
   * Dialects prior to 3.0 cannot have channel info.  For SMBv3, the
   * msgLen must indicate that there's room for a channel info block.
   * If the Length or Offset are zero, the channel info can't be valid.
   */
  if( (dialect < SMB2_V0300)
   || (msgLen < SMB2_READ_REQ_STRUCT_SIZE)
   || (0 == readReq->ReadChannelInfoLength)
   || (0 == readReq->ReadChannelInfoOffset) )
    {
    return( SMB2_READ_REQ_SIZE );
    }

  /* The offset must be beyond the end of the fixed part of the message.  */
  bufrOffset = (SMB2_HEADER_SIZE + SMB2_READ_REQ_SIZE);
  if( readReq->ReadChannelInfoOffset < bufrOffset )
    return( SMB2_READ_REQ_SIZE );

  /* If the message buffer we were given isn't large enough to contain the
   * ChannelInfoBlock, then we cannot trust pointing to it.
   */
  bufrOffset = (readReq->ReadChannelInfoOffset - SMB2_HEADER_SIZE);
  if( (msgLen <= (bufrOffset + readReq->ReadChannelInfoLength)) )
    return( SMB2_READ_REQ_SIZE );

  /* We seem to have a Buffer.  */
  readReq->Buffer = &msg[bufrOffset];
  return( SMB2_READ_REQ_SIZE );
  } /* smb2_parseReadReq */

int smb2_packReadReq( uint16_t      const dialect,
                      smb2_ReadReq *const readReq,
                      uint8_t      *const bufr,
                      uint32_t      const bSize )
  /** Compose an SMB2 Read Request message from parts.
   *
   * @param[in]   dialect   The SMB2 dialect to use as a guide when composing
   *                        the outgoing message.
   * @param[in]   readReq   Provides the content that will be used to
   *                        compose the outgoing message.
   * @param[out]  bufr      A pointer to a memory buffer into which the
   *                        composed message will be written.
   * @param[in]   bSize     The size, in bytes, of \p bufr.
   *
   * @returns   The number of bytes of \p bufr that were filled, which is
   *            always 48.  48 is the size of the fixed-length portion of
   *            the Read Request payload.
   *
   * \b Notes
   *  - The #smb2_ReadReq.Buffer field is ignored.
   *    The Channel Info Block, if any, must be added separately.
   */
  {
  uint8_t *p = bufr;

  /* Sanity cheques. */
  assert( NULL != bufr );
  assert( SMB2_READ_REQ_SIZE <= bSize );

  /* Pack. */
  p = smbset16( p, readReq->StructureSize );
  p = smbset8(  p, readReq->Padding );
  p = smbset8(  p, readReq->Flags );
  p = smbset32( p, readReq->Length );
  p = smbset64( p, readReq->Offset );
  p = smbcpmem( p, readReq->FileId, 16 );
  p = smbset32( p, readReq->MinimumCount );
  p = smbset32( p, readReq->Channel );
  p = smbset32( p, readReq->RemainingBytes );
  p = smbset16( p, readReq->ReadChannelInfoOffset );
  p = smbset16( p, readReq->ReadChannelInfoLength );
  return( SMB2_READ_REQ_SIZE );
  } /* smb2_packReadReq */

int smb2_parseReadResp( uint16_t        const dialect,
                        uint8_t        *const msg,
                        size_t          const msgLen,
                        smb2_ReadResp  *const readResp )
  /** Unpack an SMB2 Read Response message.
   *
   * @param[in]   dialect   The dialect to use when parsing the message.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB2 Read Response message,
   *                        and be at least 16 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  readResp  A (constant) pointer to an #smb2_ReadResp
   *                        structure into which the message will be parsed.
   *
   * @returns   On error, a negative number is returned.  Otherwise, the
   *            return value is the number of bytes of \p msg that were
   *            read, which should always be 16.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the received \c StructureSize was
   *                  not the required value.  The invalid value will be
   *                  returned via the \c readResp->StructureSize field.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain a complete Read Response message.
   *                  A minimum of \c #SMB2_READ_RESP_SIZE bytes are needed.
   *
   * \b Notes
   *  - Upon return, the \c readResp->Buffer field will either be NULL or
   *    will point to the location within \c msg at which the return buffer
   *    (the data that was read) begins.
   *
   * @todo
   *  - Understand better how RDMA reads work, and how the Read Response
   *    parameters change when RDMA transfers are in use.
   */
  {
  int bufrOffset;

  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != readResp );
  /* Check buffer size.   */
  if( msgLen < SMB2_READ_RESP_SIZE )
    return( -SMB2_READ_RESP_SIZE );
  /* Test StructureSize.  */
  readResp->StructureSize = smbget16( msg );
  if( readResp->StructureSize != SMB2_READ_RESP_STRUCT_SIZE )
    return( -1 );

  /* Basic unpacking. */
  readResp->DataOffset    = msg[2];
  readResp->Reserved1[0]  = msg[3];
  readResp->DataLength    = smbget32( &msg[4] );
  readResp->DataRemaining = smbget32( &msg[8] );
  (void)memcpy( readResp->Reserved2, &msg[12], 4 );
  readResp->Buffer        = NULL;

  /* Attempt to point Buffer in the right direction. */
  bufrOffset = readResp->DataOffset - SMB2_HEADER_SIZE;
  if( bufrOffset >= msgLen )
    readResp->Buffer = &msg[bufrOffset];

  return( SMB2_READ_RESP_SIZE );
  } /* smb2_parseReadResp */

int smb2_packReadResp( uint16_t       const dialect,
                       smb2_ReadResp *const readResp,
                       uint8_t       *const bufr,
                       uint32_t       const bSize )
  /** Pack an SMB2 Read Response message.
   *
   * @param[in]   dialect   The dialect used when composing the message.
   * @param[in]   readResp  A pointer to an \c #smb2_ReadResp structure
   *                        containing the values to be used to compose
   *                        the wire format message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the base message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   The function returns the number of bytes of \p bufr that
   *            were used to store the message (always 16).
   *
   * \b Notes
   *  - The \c readResp.Buffer field is ignored.  This function packs only
   *    the fixed portion of the SMB2 Read Response message.
   */
  {
  uint8_t *p = bufr;

  /* Sanity cheques. */
  assert( NULL != bufr );
  assert( SMB2_READ_RESP_SIZE <= bSize );

  p = smbset16( p, readResp->StructureSize );
  p = smbset8(  p, readResp->DataOffset );
  p = smbset8(  p, readResp->Reserved1[0] );
  p = smbset32( p, readResp->DataLength );
  p = smbset32( p, readResp->DataRemaining );
  p = smbcpmem( p, readResp->Reserved2, 4 );

  return( SMB2_READ_RESP_SIZE );
  } /* smb2_packReadResp */


int smb2_parseWriteReq( uint16_t        const dialect,
                        uint8_t        *const msg,
                        size_t          const msgLen,
                        smb2_WriteReq  *const writeReq )
  /** Unpack an SMB2 Write Request message.
   *
   * @param[in]   dialect   The dialect to use when parsing the message.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB Write Request message, at
   *                        least 48 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  writeReq  A (constant) pointer to an #smb2_WriteReq
   *                        structure into which the message will be parsed.
   *
   * @returns   On error, a negative value is returned.  Otherwise, the
   *            return value will be the number of bytes consumed by parsing
   *            the message.  This will always be 48 bytes.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c #smb2_WriteReq.StructureSize
   *                  field did not contain the required value.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain a complete Write Request message.
   *                  At least \c #SMB2_WRITE_REQ_SIZE are needed.
   *
   * \b Notes
   *  - If the message (as identified by [\p msg, \p msglen]) includes an
   *    RDMA ChannelInfoBlock, \c writeReq->Buffer will be set to point to
   *    the location in \p msg at which the ChannelInfoBlock begins.
   *  - If there is no ChannelInfoBlock, and the offset of the start of the
   *    write buffer is within the \p msg buffer, \c writeReq->Buffer will
   *    be set to point to the start of the write buffer.
   *  - If neither the ChannelInfoBlock or write buffer can be identified
   *    within \p msg, \c writeReq->Buffer will be NULL.
   */
  {
  int bOff1, bOff2;

  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != writeReq );
  /* Check buffer size.   */
  if( msgLen < SMB2_WRITE_REQ_SIZE )
    return( -SMB2_WRITE_REQ_SIZE );
  /* Test StructureSize.  */
  writeReq->StructureSize = smbget16( msg );
  if( SMB2_WRITE_REQ_STRUCT_SIZE != writeReq->StructureSize )
    return( -1 );

  /* Your basic parsing of things.  */
  writeReq->DataOffset              = smbget16( &msg[2] );
  writeReq->Length                  = smbget32( &msg[4] );
  writeReq->Offset                  = smbget64( &msg[8] );
  writeReq->FileId                  = &msg[16];
  writeReq->Channel                 = smbget32( &msg[32] );
  writeReq->RemainingBytes          = smbget32( &msg[36] );
  writeReq->WriteChannelInfoOffset  = smbget16( &msg[40] );
  writeReq->WriteChannelInfoLength  = smbget16( &msg[42] );
  writeReq->Flags                   = smbget32( &msg[44] );
  writeReq->Buffer                  = NULL;

  /* Quick check. */
  if( msgLen < SMB2_WRITE_REQ_STRUCT_SIZE )
    return( SMB2_WRITE_REQ_SIZE );

  /* Look for a Channel Info Block. */
  if( (dialect >= SMB2_V0300)
   && (writeReq->Channel != SMB2_CHANNEL_NONE)
   && (0 != writeReq->WriteChannelInfoOffset)
   && (0 != writeReq->WriteChannelInfoLength) )
    {
    /* If we got this far we should have a valid Channel Info Block,
     * but double-triple-check.
     */
    bOff1 = writeReq->WriteChannelInfoOffset - SMB2_HEADER_SIZE;
    bOff2 = bOff1 + writeReq->WriteChannelInfoLength;
    if( (bOff1 >= SMB2_WRITE_REQ_SIZE)          /* Beyond the fixed part. */
     && (bOff1 < msgLen)                        /* Starts within the msg. */
     && (bOff2 <= msgLen) )                     /* Ends within the msg.   */
      {
      writeReq->Buffer = &msg[bOff1];           /* Set InfoBlock pointer. */
      return( SMB2_WRITE_REQ_SIZE );
      }
    }

  /* No Channel Info Block, so look for the start of the write buffer. */
  bOff1 = writeReq->DataOffset - SMB2_HEADER_SIZE;
  if( bOff1 < msgLen )
    writeReq->Buffer = &msg[bOff1];               /* Set WriteBuffer pointer. */

  return( SMB2_WRITE_REQ_SIZE );
  } /* smb2_parseWriteReq */

int smb2_packWriteReq( uint16_t       const dialect,
                       smb2_WriteReq *const writeReq,
                       uint8_t       *const bufr,
                       uint32_t       const bSize )
  /** Compose an SMB2 Write Request message.
   *
   * @param[in]   dialect   The dialect used when composing the message.
   * @param[in]   writeReq  A pointer to an \c #smb2_WriteReq structure
   *                        containing the values to be used to compose the
   *                        wire format message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the base message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   The function returns the number of bytes of \p bufr that
   *            were used to store the message (always 48).
   *
   * \b Notes
   *  - The \c writeReq->Buffer value is ignored.  A ChannelInfoBlock or
   *    write buffer should be handled in a separate step.
   */
  {
  uint8_t *p = bufr;

  /* Sanity checks. */
  assert( NULL != bufr );
  assert( SMB2_WRITE_REQ_SIZE <= bSize );

  /* Pack the message from the provided data. */
  p = smbset16( p, writeReq->StructureSize );
  p = smbset16( p, writeReq->DataOffset );
  p = smbset32( p, writeReq->Length );
  p = smbset64( p, writeReq->Offset );
  p = smbcpmem( p, writeReq->FileId, 16 );
  p = smbset32( p, writeReq->Channel );
  p = smbset32( p, writeReq->RemainingBytes );
  p = smbset16( p, writeReq->WriteChannelInfoOffset );
  p = smbset16( p, writeReq->WriteChannelInfoLength );
  p = smbset32( p, writeReq->Flags );

  return( SMB2_WRITE_REQ_SIZE );
  } /* smb2_packWriteReq */

int smb2_parseWriteResp( uint16_t        const dialect,
                         uint8_t        *const msg,
                         size_t          const msgLen,
                         smb2_WriteResp *const writeResp )
  /** Unpack an SMB2 Write Response message.
   *
   * @param[in]   dialect   The dialect used when composing the message.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB Write Response message, at
   *                        least 16 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  writeResp A (constant) pointer to an #smb2_WriteResp
   *                        structure into which the message will be parsed.
   *
   * @returns   On error, a negative value is returned.  Otherwise, the
   *            return value will be the number of bytes consumed by parsing
   *            the message.  This will always be 16.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c #smb2_WriteResp.StructureSize
   *                  field did not contain the required value.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain a complete Write Response message.
   *                  At least \c #SMB2_WRITE_RESP_SIZE bytes are required.
   *
   * \b Notes
   *  - There are three named fields in the Write Response structure that
   *    are actually Reserved-MB0 fields.  Someone at Microsoft must've
   *    allocated them, but they were never used for the designated purpose.
   *    This happens.  The fields in question are:
   *    - \c #smb2_WriteResp.Remaining,
   *    - \c #smb2_WriteResp.WriteChannelInfoOffset
   *    - \c #smb2_WriteResp.WriteChannelInfoLength
   *    .
   *    ...and, of course, there's also a \c #smb2_WriteResp.Reserved field,
   *    but that one's fairly straight-forward.  The important point here is
   *    that the named fields are presented in [MS-SMB2] as being 16 and 32
   *    bit unsigned integers.  They are also presented as being reserved.
   *    With that in mind, they are treated exactly like any other Reserved
   *    field.  That is:
   *    - The fields are defined as arrays of bytes, rather than unsigned
   *      multi-byte integers.
   *    - They should be zero-filled, but...
   *    - The values are transferred in wire order, not converted between
   *      SMB and host byte order.
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != writeResp );
  /* Check buffer size.   */
  if( msgLen < SMB2_WRITE_RESP_SIZE )
    return( -SMB2_WRITE_RESP_SIZE );
  /* Test StructureSize.  */
  writeResp->StructureSize = smbget16( msg );
  if( SMB2_WRITE_RESP_STRUCT_SIZE != writeResp->StructureSize )
    return( -1 );

  /* Copy the buffer content into the write request data structure. */
  (void)memcpy( writeResp->Reserved,  &msg[2], 2 );
  (void)memcpy( writeResp->Remaining, &msg[8], 4 );
  (void)memcpy( writeResp->WriteChannelInfoOffset, &msg[12], 2 );
  (void)memcpy( writeResp->WriteChannelInfoLength, &msg[14], 2 );
  writeResp->Count = smbget32( &msg[4] );     /* The only useful information. */

  return( SMB2_WRITE_RESP_SIZE );
  } /* smb2_parseWriteResp */

int smb2_packWriteResp( uint16_t        const dialect,
                        smb2_WriteResp *const writeResp,
                        uint8_t        *const bufr,
                        uint32_t        const bSize )
  /** Pack an SMB2 Write Response message.
   *
   * @param[in]   dialect   The dialect to use when packing the message.
   * @param[in]   writeResp A pointer to an \c #smb2_WriteResp structure
   *                        containing the values to be used to compose the
   *                        wire format message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the base message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   The function returns the number of bytes of \p bufr that
   *            were used to store the message (always 16).
   */
  {
  uint8_t *p = bufr;

  /* Sanity checks. */
  assert( NULL != bufr );
  assert( SMB2_WRITE_RESP_SIZE <= bSize );

  p = smbset16( p, writeResp->StructureSize );
  p = smbcpmem( p, writeResp->Reserved, 2 );
  p = smbset32( p, writeResp->Count );
  p = smbcpmem( p, writeResp->Remaining, 4 );
  p = smbcpmem( p, writeResp->WriteChannelInfoOffset, 2 );
  p = smbcpmem( p, writeResp->WriteChannelInfoLength, 2 );

  return( SMB2_WRITE_RESP_SIZE );
  } /* smb2_packWriteResp */


int smb2_parseFlushReq( uint16_t        const dialect,
                        uint8_t        *const msg,
                        size_t          const msgLen,
                        smb2_FlushReq  *const flushReq )
  /** Unpack an SMB2 Flush Request message.
   *
   * @param[in]   dialect   The dialect to use when parsing the message.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB2 Flush Request message, at
   *                        least 24 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  flushReq  A (constant) pointer to an #smb2_FlushReq
   *                        structure into which the message will be parsed.
   *
   * @returns   On error, a negative number is returned.  Otherwise, the
   *            return value is the number of bytes of \p msg that were
   *            read, which should always be 24.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c StructureSize field did not
   *                  contain the required value.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain a complete Flush Request message.
   *                  At least \c #SMB2_FLUSH_REQ_SIZE bytes are required.
   *
   * \b Notes
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message.
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != flushReq );
  /* Check buffer size.   */
  if( msgLen < SMB2_FLUSH_REQ_SIZE )
    return( -SMB2_FLUSH_REQ_SIZE );
  /* Test StructureSize.  */
  flushReq->StructureSize = smbget16( msg );
  if( SMB2_FLUSH_REQ_STRUCT_SIZE != flushReq->StructureSize )
    return( -1 );

  /* Copy the reserved bytes into the flush request data structure. */
  (void)memcpy( flushReq->Reserved1, &msg[2], 2 );
  (void)memcpy( flushReq->Reserved2, &msg[4], 4 );

  /* The prize inside.  */
  flushReq->FileId = &msg[8];
  return( SMB2_FLUSH_REQ_SIZE );
  } /* smb2_parseFlushReq */

int smb2_packFlushReq( uint16_t       const dialect,
                       smb2_FlushReq *const flushReq,
                       uint8_t       *const bufr,
                       uint32_t       const bSize )
  /** Pack an SMB2 Flush Request message.
   *
   * @param[in]   dialect   The dialect used when composing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value has no
   *                        actual impact.  The parameter is included for
   *                        interface consistency.
   * @param[in]   flushReq  A pointer to an \c #smb2_FlushReq structure
   *                        containing the values to be used to compose the
   *                        wire format message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the base message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   The function returns the number of bytes of \p bufr that
   *            were used to store the message (always 24).
   *
   * \b Notes
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   */
  {
  uint8_t *p = bufr;

  /* Sanity checks. */
  assert( NULL != bufr );
  assert( SMB2_FLUSH_REQ_SIZE <= bSize );

  /* Pack the message from the provided data. */
  p = smbset16( p, flushReq->StructureSize );     /* Yes, allow bogus values. */
  p = smbcpmem( p, flushReq->Reserved1, 2 );
  p = smbcpmem( p, flushReq->Reserved2, 4 );
  p = smbcpmem( p, flushReq->FileId,   16 );
  return( SMB2_FLUSH_REQ_SIZE );
  } /* smb2_packFlushReq */

int smb2_parseFlushResp( uint16_t         const dialect,
                         uint8_t         *const msg,
                         size_t           const msgLen,
                         smb2_FlushResp  *const flushResp )
  /** Parse an SMB2 Flush Response message.
   *
   * @param[in]   dialect   The dialect used when parsing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB Flush Response message,
   *                        at least 4 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  flushResp A (constant) pointer to an #smb2_FlushResp
   *                        structure into which the message will be parsed.
   *
   * @returns   On error, a negative number is returned.  Otherwise, the
   *            return value is the number of bytes of \p msg that were
   *            read, which should always be 4.
   *
   * \b Errors
   *  - \b See #smb2_parseBaseMsg()
   *
   * \b Notes
   *  - This function is a wrapper for #smb2_parseBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_parseBaseMsg( dialect, msg, msgLen, flushResp ) );
  } /* smb2_parseFlushResp */

int smb2_packFlushResp( uint16_t        const dialect,
                        smb2_FlushResp *const flushResp,
                        uint8_t        *const bufr,
                        uint32_t        const bSize )
  /** Pack an SMB2 Flush Response message.
   *
   * @param[in]   dialect   The dialect used when packing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   flushResp Either NULL, in which case appropriate default
   *                        values are used, or a pointer to an
   *                        \c #smb2_FlushResp structure containing the
   *                        values to be used to compose the wire format
   *                        message.
   * @param[out]  bufr      Pointer to a data buffer into which the wire
   *                        format of the base message will be written.
   * @param[in]   bSize     Number of bytes available in \p bufr.
   *
   * @returns   The function returns the number of bytes of \p bufr that
   *            were used to store the message (always 4).
   *
   * \b Notes
   *  - This function is a wrapper for #smb2_packBaseMsg().
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in this message type.
   *  - Passing NULL for \p flushResp is recommended.  The body of the
   *    Flush Request message is pre-defined.
   *
   * @see #smb2_BaseMsg.
   */
  {
  return( smb2_packBaseMsg( dialect, flushResp, bufr, bSize ) );
  } /* smb2_packFlushResp */


/* ============================== Nothing More ============================== */
