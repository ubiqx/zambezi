#ifndef SMB2_BASEMSG_H
#define SMB2_BASEMSG_H
/* ========================================================================== **
 *                               smb2_baseMsg.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Implementation of basic (empty) SMB2 messages.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_baseMsg.h; 2021-05-05 12:22:31 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    smb2_baseMsg.h
 * @author  Christopher R. Hertel
 * @date    8 Mar 2020
 * @version \$Id: smb2_baseMsg.h; 2021-05-05 12:22:31 -0500; crh$
 * @brief   Primative data types used in SMB2 protocol messaging.
 * @details
 *  This module provides basic data types and methods for handing constructs
 *  that are common to multiple SMB2 messages.  In doing so, it provides
 *  support for several message types.
 */

#include <stddef.h>     /* For size_t and other standard types. */
#include <stdint.h>     /* Standard integer types.              */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *//**
 * @def     SMB2_BASEMSG_SIZE
 * @brief   The actual size of the fixed-length portion of the base message.
 * @details The base message has no variable portion, so the message size is
 *          the same as the \c StructureSize.
 *
 * @def     SMB2_BASEMSG_STRUCT_SIZE
 * @brief   The size of the fixed-length portion of an SMB2/3 base message.
 * @details The size, in bytes, of the fixed-length portion of an SMB2/3
 *          base message.  What we are calling "base messages" all have the
 *          same \c StructureSize.  Since there is no variable part to this
 *          message type, the \c StructureSize is also the length of the
 *          message payload (following the header).
 */

#define SMB2_BASEMSG_SIZE        4
#define SMB2_BASEMSG_STRUCT_SIZE 4


/* -------------------------------------------------------------------------- **
 * Enternal Constants
 *//**
 * @var     smb2_BaseMsg_Data
 * @brief   Wire-format content of a base message.
 * @details The base message is four bytes in length, consisting of a USHORT
 *          \c StructureSize field followed by two reserved bytes.  The
 *          \c StructureSize is always 4, which is represented as [0x04,0x00]
 *          in SMB byte order.  The reserved bytes are "Reserved, Must Be
 *          Zero".  Thus, the four bytes on the wire are: [04 00, 00, 00].
 *
 *          This constant contains that string of bytes.
 */

extern uint8_t const smb2_BaseMsg_Data[];


/* -------------------------------------------------------------------------- **
 * Typedefs
 *//**
 * @struct  smb2_BaseMsg
 * @brief   Generic message type.
 * @details
 *  Several SMB2 messages use this minimal structure, which transfers no
 *  actual information.  For these messages, the relevant information is
 *  carried within the header.  The SMB2 messages that use this format are:
 *  - LOGOFF Request
 *  - LOGOFF Response
 *  - TREE_DISCONNECT Request
 *  - TREE_DISCONNECT Response
 *  - ECHO Request
 *  - ECHO Response
 *  - CANCEL Request
 *  - LOCK Response
 *  - FLUSH Response
 *
 * That's nine message types for the price of one.  Not bad.  We call this
 * a "base message" because it is the base-line minimal message structure
 * for SMB2/3.  The only thing smaller would be no message at all.
 *
 * @var smb2_BaseMsg::StructureSize
 *      Two bytes.  Must be 4 (in SMB byte order, obviously).
 * @var smb2_BaseMsg::Reserved
 *      Two bytes.  Reserved; must be zero.
 */
typedef struct
  {
  uint16_t StructureSize;   /**< Must be 4; 2 bytes.    */
  uint8_t  Reserved[2];     /**< Reserved-MB0; 2 bytes. */
  } smb2_BaseMsg;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int smb2_parseBaseMsg( uint16_t      const dialect,
                       uint8_t      *const msg,
                       size_t        const msgLen,
                       smb2_BaseMsg *const baseMsg );

int smb2_packBaseMsg( uint16_t      const dialect,
                      smb2_BaseMsg *const baseMsg,
                      uint8_t      *const bufr,
                      uint32_t      const bSize );


/* ============================= smb2_baseMsg.h ============================= */
#endif /* SMB2_BASEMSG_H */
