/* ========================================================================== **
 *                               smb2_baseMsg.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Implementation of basic (empty) SMB2 messages.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_baseMsg.c; 2024-03-11 18:13:21 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <assert.h>         /* For debugging; see assert(3).  */

#include "smb_endian.h"     /* Convert between host and SMB byte order. */
#include "smb2_baseMsg.h"   /* Module header.                           */


/* -------------------------------------------------------------------------- **
 * External Constants
 *
 *  smb2_BaseMsg_Data - The fixed content of all base messages.
 *                      See the declaration in the header for more detail.
 */

uint8_t const smb2_BaseMsg_Data[SMB2_BASEMSG_SIZE] = { 0x04, 0x00, 0x00, 0x00 };


/* -------------------------------------------------------------------------- **
 * Functions
 */

int smb2_parseBaseMsg( uint16_t      const dialect,
                       uint8_t      *const msg,
                       size_t        const msgLen,
                       smb2_BaseMsg *const baseMsg )
  /** Parse a simple SMB2/3 base message.
   *
   * @param[in]   dialect   The dialect to use when parsing the message.
   * @param[in]   msg       A (constant) pointer to an array of bytes, which
   *                        \b MUST contain an SMB base message structure at
   *                        least 4 bytes in length.
   * @param[in]   msgLen    The size, in bytes, of \p msg.
   * @param[out]  baseMsg   A (constant) pointer to an #smb2_BaseMsg
   *                        structure into which the message will be parsed.
   *
   * @returns   On error, a negative number is returned.  Otherwise, the
   *            return value is the number of bytes of \p msg that were
   *            read, which should always be \c #SMB2_BASEMSG_SIZE.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c StructureSize field did not
   *                  contain the required value.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain a complete base message.  At
   *                  least #SMB2_BASEMSG_SIZE bytes are required.
   *
   * \b Notes
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in a message as basic as this.
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != baseMsg );
  if( msgLen < SMB2_BASEMSG_SIZE )                    /* Check buffer size.   */
    return( -SMB2_BASEMSG_SIZE );
  /* Read and test the StructureSize. */
  if( (baseMsg->StructureSize = smbget16( msg )) != SMB2_BASEMSG_STRUCT_SIZE )
    return( -1 );

  /* Copy the reserved bytes into the base message data structure.  */
  (void)memcpy( baseMsg->Reserved, &msg[2], 2 );
  return( SMB2_BASEMSG_SIZE );          /* ...and that's all there is to it.  */
  } /* smb2_parseBaseMsg */

int smb2_packBaseMsg( uint16_t      const dialect,
                      smb2_BaseMsg *const baseMsg,
                      uint8_t      *const bufr,
                      uint32_t      const bSize )
  /** Pack a simple SMB2/3 base message.
   *
   *  @param[in]  dialect The dialect used when composing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value has no actual
   *                      impact.  The parameter is included for interface
   *                      consistency.
   *  @param[in]  baseMsg Either NULL, or a pointer to an \c #smb2_BaseMsg
   *                      structure containing the values to be used to
   *                      create the base message wire format.  If NULL, the
   *                      values required by the specification will be
   *                      copied into \p bufr.
   *  @param[out] bufr    Pointer to a data buffer into which the wire
   *                      format of the base message will be written.
   *  @param[in]  bSize   Number of bytes available in \p bufr.
   *
   *  @return   The function returns the number of bytes of \p bufr that were
   *            used to store the message (always \c #SMB2_BASEMSG_SIZE).
   *
   * \b Notes
   *  - The \p dialect is ignored, since there are no dialect-specific
   *    fields in a message as basic as this.
   */
  {
  uint8_t *p = bufr;

  /* Sanity checks. */
  assert( NULL != bufr );
  assert( SMB2_BASEMSG_SIZE <= bSize );

  /* Write the wire format base message using default values. */
  if( NULL == baseMsg )
    {
    (void)smbcpmem( bufr, smb2_BaseMsg_Data, SMB2_BASEMSG_SIZE );
    return( SMB2_BASEMSG_SIZE );
    }

  /* Use the input provided via <baseMsg> to write the wire format message. */
  p = smbset16( p, baseMsg->StructureSize );
  p = smbcpmem( p, baseMsg->Reserved, 2 );
  return( SMB2_BASEMSG_SIZE );
  } /* smb2_packBaseMsg */


/* ========================== Relative Generality =========================== */
