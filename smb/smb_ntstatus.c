/* ========================================================================== **
 *                               smb_ntstatus.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Windows NT Status Code support.
 *
 * Copyright (C) 2019 by Christopher R. Hertel
 * $Id: smb_ntstatus.c; 2024-11-28 13:36:44 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <stddef.h>         /* NULL is defined here (per POSIX).  */
#include "smb_ntstatus.h"   /* Module header.                     */


/* -------------------------------------------------------------------------- **
 * Functions
 */

ntstatus_Text ntstatus_getInfo( const NTSTATUS ntstatus )
  /** Given an #NTSTATUS, look up the associated name and message.
   *
   * @param[in] ntstatus  An #NTSTATUS code value, used as a lookup key.
   *
   * @returns   An #ntstatus_Text structure, which contains all of the
   *            basic information about the NTSTATUS code, including the
   *            original status code.
   */
  {
  ntstatus_Text tmp = { ntstatus, NULL, NULL };

  switch( ntstatus )
    {
    case STATUS_SUCCESS:
      tmp.msg  = "The operation completed successfully.";
      tmp.name = "STATUS_SUCCESS";
      break;
    case STATUS_PENDING:
      tmp.msg  = "The operation that was requested is pending completion.";
      tmp.name = "STATUS_PENDING";
      break;
    case STATUS_REPARSE:
      tmp.msg  = "A reparse should be performed by the Object Manager because "
                 "the name of the file resulted in a symbolic link.";
      tmp.name = "STATUS_REPARSE";
      break;
    case STATUS_NOTIFY_CLEANUP:
      tmp.msg  = "Indicates that a notify change request has been completed "
                 "due to closing the handle that made the notify change "
                 "request.";
      tmp.name = "STATUS_NOTIFY_CLEANUP";
      break;
    case STATUS_NOTIFY_ENUM_DIR:
      tmp.msg  = "Indicates that a notify change request is being completed "
                 "and that the information is not being returned in the "
                 "caller's buffer. The caller now needs to enumerate the files "
                 "to find the changes.";
      tmp.name = "STATUS_NOTIFY_ENUM_DIR";
      break;
    case STATUS_BUFFER_OVERFLOW:
      tmp.msg  = "Buffer Overflow; The data was too large to fit into the "
                 "specified buffer.";
      tmp.name = "STATUS_BUFFER_OVERFLOW";
      break;
    case STATUS_NO_MORE_FILES:
      tmp.msg  = "No more files were found which match the file specification.";
      tmp.name = "STATUS_NO_MORE_FILES";
      break;
    case STATUS_EA_LIST_INCONSISTENT:
      tmp.msg  = "The extended attribute (EA) list is inconsistent.";
      tmp.name = "STATUS_EA_LIST_INCONSISTENT";
      break;
    case STATUS_NO_MORE_ENTRIES:
      tmp.msg  = "No more entries are available from an enumeration operation.";
      tmp.name = "STATUS_NO_MORE_ENTRIES";
      break;
    case STATUS_STOPPED_ON_SYMLINK:
      tmp.msg  = "The create operation stopped after reaching a symbolic link.";
      tmp.name = "STATUS_STOPPED_ON_SYMLINK";
      break;
    case STATUS_UNSUCCESSFUL:
      tmp.msg  = "Operation Failed; The requested operation was unsuccessful.";
      tmp.name = "STATUS_UNSUCCESSFUL";
      break;
    case STATUS_INVALID_INFO_CLASS:
      tmp.msg  = "Invalid Parameter; The specified information class is not a "
                 "valid information class for the specified object.";
      tmp.name = "STATUS_INVALID_INFO_CLASS";
      break;
    case STATUS_INFO_LENGTH_MISMATCH:
      tmp.msg  = "The specified information record length does not match the "
                 "length that is required for the specified information class.";
      tmp.name = "STATUS_INFO_LENGTH_MISMATCH";
      break;
    case STATUS_INVALID_HANDLE:
      tmp.msg  = "An invalid HANDLE was specified.";
      tmp.name = "STATUS_INVALID_HANDLE";
      break;
    case STATUS_INVALID_PARAMETER:
      tmp.msg  = "An invalid parameter was passed to a service or function.";
      tmp.name = "STATUS_INVALID_PARAMETER";
      break;
    case STATUS_NO_SUCH_FILE:
      tmp.msg  = "File Not Found; The file does not exist.";
      tmp.name = "STATUS_NO_SUCH_FILE";
      break;
    case STATUS_INVALID_DEVICE_REQUEST:
      tmp.msg  = "The specified request is not a valid operation for the "
                 "target device.";
      tmp.name = "STATUS_INVALID_DEVICE_REQUEST";
      break;
    case STATUS_END_OF_FILE:
      tmp.msg  = "The end-of-file marker has been reached. There is no valid "
                 "data in the file beyond this marker.";
      tmp.name = "STATUS_END_OF_FILE";
      break;
    case STATUS_MORE_PROCESSING_REQUIRED:
      tmp.msg  = "Still Busy; The specified I/O request packet (IRP) cannot be "
                 "disposed of because the I/O operation is not complete.";
      tmp.name = "STATUS_MORE_PROCESSING_REQUIRED";
      break;
    case STATUS_NO_MEMORY:
      tmp.msg  = "Insufficient Quota; Not enough virtual memory or paging file "
                 "quota is available to complete the specified operation.";
      tmp.name = "STATUS_NO_MEMORY";
      break;
    case STATUS_ACCESS_DENIED:
      tmp.msg  = "A process has requested access to an object but has not been "
                 "granted those access rights.";
      tmp.name = "STATUS_ACCESS_DENIED";
      break;
    case STATUS_BUFFER_TOO_SMALL:
      tmp.msg  = "The buffer is too small to contain the entry.  No "
                 "information has been written to the buffer.";
      tmp.name = "STATUS_BUFFER_TOO_SMALL";
      break;
    case STATUS_OBJECT_NAME_INVALID:
      tmp.msg  = "The object name is invalid.";
      tmp.name = "STATUS_OBJECT_NAME_INVALID";
      break;
    case STATUS_OBJECT_NAME_NOT_FOUND:
      tmp.msg  = "The object name is not found.";
      tmp.name = "STATUS_OBJECT_NAME_NOT_FOUND";
      break;
    case STATUS_OBJECT_NAME_COLLISION:
      tmp.msg  = "The object name already exists.";
      tmp.name = "STATUS_OBJECT_NAME_COLLISION";
      break;
    case STATUS_EAS_NOT_SUPPORTED:
      tmp.msg  = "An operation involving EAs failed because the file system "
                 "does not support EAs.";
      tmp.name = "STATUS_EAS_NOT_SUPPORTED";
      break;
    case STATUS_NONEXISTENT_EA_ENTRY:
      tmp.msg  = "An EA operation failed because the name or EA index is "
                 "invalid.";
      tmp.name = "STATUS_NONEXISTENT_EA_ENTRY";
      break;
    case STATUS_FILE_LOCK_CONFLICT:
      tmp.msg  = "A requested read/write cannot be granted due to a "
                 "conflicting file lock.";
      tmp.name = "STATUS_FILE_LOCK_CONFLICT";
      break;
    case STATUS_LOCK_NOT_GRANTED:
      tmp.msg  = "A requested file lock cannot be granted due to other "
                 "existing locks.";
      tmp.name = "STATUS_LOCK_NOT_GRANTED";
      break;
    case STATUS_NO_SUCH_LOGON_SESSION:
      tmp.msg  = "A specified logon session does not exist. It may already "
                 "have been terminated.";
      tmp.name = "STATUS_NO_SUCH_LOGON_SESSION";
      break;
    case STATUS_NO_SUCH_USER:
      tmp.msg  = "The specified account does not exist.";
      tmp.name = "STATUS_NO_SUCH_USER";
      break;
    case STATUS_WRONG_PASSWORD:
      tmp.msg  = "When trying to update a password, this return status "
                 "indicates that the value provided as the current password is "
                 "not correct.";
      tmp.name = "STATUS_WRONG_PASSWORD";
      break;
    case STATUS_PASSWORD_RESTRICTION:
      tmp.msg  = "When trying to update a password, this status indicates "
                 "that some password update rule has been violated.  For "
                 "example, the password may not meet length criteria.";
      tmp.name = "STATUS_PASSWORD_RESTRICTION";
      break;
    case STATUS_LOGON_FAILURE:
      tmp.msg  = "The attempted logon is invalid.  This is either due to a bad "
                 "username or authentication information.";
      tmp.name = "STATUS_LOGON_FAILURE";
      break;
    case STATUS_INVALID_LOGON_HOURS:
      tmp.msg  = "The user account has time restrictions and may not be logged "
                 "onto at this time.";
      tmp.name = "STATUS_INVALID_LOGON_HOURS";
      break;
    case STATUS_INVALID_WORKSTATION:
      tmp.msg  = "The user account is restricted so that it may not be used to "
                 "log on from the source workstation.";
      tmp.name = "STATUS_INVALID_WORKSTATION";
      break;
    case STATUS_PASSWORD_EXPIRED:
      tmp.msg  = "The user account password has expired.";
      tmp.name = "STATUS_PASSWORD_EXPIRED";
      break;
    case STATUS_NONE_MAPPED:
      tmp.msg  = "None of the information to be translated has been "
                 "translated.";
      tmp.name = "STATUS_NONE_MAPPED";
      break;
    case STATUS_NO_TOKEN:
      tmp.msg  = "An attempt was made to reference a token that does not "
                 "exist.  This is typically done by referencing the token "
                 "that is associated with a thread when the thread is not "
                 "impersonating a client.";
      tmp.name = "STATUS_NO_TOKEN";
      break;
    case STATUS_RANGE_NOT_LOCKED:
      tmp.msg  = "The range specified in NtUnlockFile was not locked.";
      tmp.name = "STATUS_RANGE_NOT_LOCKED";
      break;
    case STATUS_DISK_FULL:
      tmp.msg  = "An operation failed because the disk was full.";
      tmp.name = "STATUS_DISK_FULL";
      break;
    case STATUS_INSUFFICIENT_RESOURCES:
      tmp.msg  = "Insufficient system resources exist to complete the API.";
      tmp.name = "STATUS_INSUFFICIENT_RESOURCES";
      break;
    case STATUS_IO_TIMEOUT:
      tmp.msg  = "Device Timeout; The specified I/O operation was not "
                 "completed before the time-out period expired.";
      tmp.name = "STATUS_IO_TIMEOUT";
      break;
    case STATUS_FILE_FORCED_CLOSED:
      tmp.msg  = "The specified file has been closed by another process.";
      tmp.name = "STATUS_FILE_FORCED_CLOSED";
      break;
    case STATUS_FILE_IS_A_DIRECTORY:
      tmp.msg  = "The file that was specified as a target is a directory, and "
                 "the caller specified that it could be anything but a "
                 "directory.";
      tmp.name = "STATUS_FILE_IS_A_DIRECTORY";
      break;
    case STATUS_NOT_SUPPORTED:
      tmp.msg  = "The request is not supported.";
      tmp.name = "STATUS_NOT_SUPPORTED";
      break;
    case STATUS_INVALID_NETWORK_RESPONSE:
      tmp.msg  = "The network responded incorrectly.";
      tmp.name = "STATUS_INVALID_NETWORK_RESPONSE";
      break;
    case STATUS_NETWORK_NAME_DELETED:
      tmp.msg  = "The network name was deleted.";
      tmp.name = "STATUS_NETWORK_NAME_DELETED";
      break;
    case STATUS_BAD_NETWORK_NAME:
      tmp.msg  = "The share name cannot be found on the server.";
      tmp.name = "STATUS_BAD_NETWORK_NAME";
    case STATUS_REQUEST_NOT_ACCEPTED:
      tmp.msg  = "No more connections can be made to this remote computer at "
                 "this time because the computer has already accepted the "
                 "maximum number of connections.";
      tmp.name = "STATUS_REQUEST_NOT_ACCEPTED";
      break;
    case STATUS_NO_SUCH_DOMAIN:
      tmp.msg  = "The specified domain did not exist.";
      tmp.name = "STATUS_NO_SUCH_DOMAIN";
      break;
    case STATUS_INVALID_OPLOCK_PROTOCOL:
      tmp.msg  = "An error status returned when an invalid opportunistic lock "
                 "(OpLock) acknowledgment is received by a file system.";
      tmp.name = "STATUS_INVALID_OPLOCK_PROTOCOL";
      break;
    case STATUS_INTERNAL_ERROR:
      tmp.msg  = "An internal error occurred.";
      tmp.name = "STATUS_INTERNAL_ERROR";
      break;
    case STATUS_FILE_CORRUPT_ERROR:
      tmp.msg  = "Corrupt File; The file or directory is corrupt and "
                 "unreadable.";
      tmp.name = "STATUS_FILE_CORRUPT_ERROR";
      break;
    case STATUS_NOT_A_DIRECTORY:
      tmp.msg  = "A requested opened file is not a directory.";
      tmp.name = "STATUS_NOT_A_DIRECTORY";
      break;
    case STATUS_CANCELLED:
      tmp.msg  = "The I/O request was canceled.";
      tmp.name = "STATUS_CANCELLED";
      break;
    case STATUS_FILE_CLOSED:
      tmp.msg  = "An I/O request other than close and several other special "
                 "case operations was attempted using a file object that had "
                 "already been closed.";
      tmp.name = "STATUS_FILE_CLOSED";
      break;
    case STATUS_PIPE_BROKEN:
      tmp.msg  = "The pipe operation has failed because the other end of the "
                 "pipe has been closed.";
      tmp.name = "STATUS_PIPE_BROKEN";
      break;
    case STATUS_LOGON_TYPE_NOT_GRANTED:
      tmp.msg  = "A user has requested a type of logon (for example, "
                 "interactive or network) that has not been granted. An "
                 "administrator has control over who may logon interactively "
                 "and through the network.";
      tmp.name = "STATUS_LOGON_TYPE_NOT_GRANTED";
      break;
    case STATUS_INVALID_DEVICE_STATE:
      tmp.msg  = "The device is not in a valid state to perform this request.";
      tmp.name = "STATUS_INVALID_DEVICE_STATE";
      break;
    case STATUS_TRUSTED_RELATIONSHIP_FAILURE:
      tmp.msg  = "The logon request failed because the trust relationship "
                 "between this workstation and the primary domain failed.";
      tmp.name = "STATUS_TRUSTED_RELATIONSHIP_FAILURE";
      break;
    case STATUS_TRUST_FAILURE:
      tmp.msg  = "The network logon failed.  This may be because the "
                 "validation authority cannot be reached.";
      tmp.name = "STATUS_TRUST_FAILURE";
      break;
    case STATUS_NETLOGON_NOT_STARTED:
      tmp.msg  = "An attempt was made to logon, but the NetLogon service was "
                 "not started.";
      tmp.name = "STATUS_NETLOGON_NOT_STARTED";
      break;
    case STATUS_FS_DRIVER_REQUIRED:
      tmp.msg  = "A volume has been accessed for which a file system driver is "
                 "required that has not yet been loaded.";
      tmp.name = "STATUS_FS_DRIVER_REQUIRED";
      break;
    case STATUS_USER_SESSION_DELETED:
      tmp.msg  = "The remote user session has been deleted.";
      tmp.name = "STATUS_USER_SESSION_DELETED";
      break;
    case STATUS_CONNECTION_DISCONNECTED:
      tmp.msg  = "The transport connection is now disconnected.";
      tmp.name = "STATUS_CONNECTION_DISCONNECTED";
      break;
    case STATUS_PASSWORD_MUST_CHANGE:
      tmp.msg  = "The user password must be changed before logging on the "
                 "first time.";
      tmp.name = "STATUS_PASSWORD_MUST_CHANGE";
      break;
    case STATUS_DUPLICATE_OBJECTID:
      tmp.msg  = "The attempt to insert the ID in the index failed because the "
                 "ID is already in the index.";
      tmp.name = "STATUS_DUPLICATE_OBJECTID";
      break;
    case STATUS_DOMAIN_CONTROLLER_NOT_FOUND:
      tmp.msg  = "A domain controller for this domain was not found.";
      tmp.name = "STATUS_DOMAIN_CONTROLLER_NOT_FOUND";
      break;
    case STATUS_NETWORK_UNREACHABLE:
      tmp.msg  = "The remote network is not reachable by the transport.";
      tmp.name = "STATUS_NETWORK_UNREACHABLE";
      break;
    case STATUS_VOLUME_DISMOUNTED:
      tmp.msg  = "An operation was attempted to a volume after it was "
                 "dismounted.";
      tmp.name = "STATUS_VOLUME_DISMOUNTED";
      break;
    case STATUS_PKINIT_NAME_MISMATCH:
      tmp.msg  = "The client certificate does not contain a valid UPN, or does "
                 "not match the client name in the logon request.";
      tmp.name = "STATUS_PKINIT_NAME_MISMATCH";
      break;
    case STATUS_PKINIT_FAILURE:
      tmp.msg  = "The Kerberos protocol encountered an error while validating "
                 "the KDC certificate during smart card logon.";
      tmp.name = "STATUS_PKINIT_FAILURE";
      break;
    case STATUS_NETWORK_SESSION_EXPIRED:
      tmp.msg  = "The client session has expired; The client must "
                 "re-authenticate to continue accessing the remote resources.";
      tmp.name = "STATUS_NETWORK_SESSION_EXPIRED";
      break;
    case STATUS_SMARTCARD_WRONG_PIN:
      tmp.msg  = "An incorrect PIN was presented to the smart card.";
      tmp.name = "STATUS_SMARTCARD_WRONG_PIN";
      break;
    case STATUS_SMARTCARD_CARD_BLOCKED:
      tmp.msg  = "The smart card is blocked.";
      tmp.name = "STATUS_SMARTCARD_CARD_BLOCKED";
      break;
    case STATUS_SMARTCARD_NO_CARD:
      tmp.msg  = "No smart card is available.";
      tmp.name = "STATUS_SMARTCARD_NO_CARD";
      break;
    case STATUS_DOWNGRADE_DETECTED:
      tmp.msg  = "The system detected a possible attempt to compromise "
                 "security.  Ensure that you can contact the server that "
                 "authenticated you.";
      tmp.name = "STATUS_DOWNGRADE_DETECTED";
      break;
    case STATUS_PKINIT_CLIENT_FAILURE:
      tmp.msg  = "The smart card certificate used for authentication was not "
                 "trusted.  Contact your system administrator.";
      tmp.name = "STATUS_PKINIT_CLIENT_FAILURE";
      break;
    case STATUS_SMARTCARD_SILENT_CONTEXT:
      tmp.msg  = "The smart card provider could not perform the action because "
                 "the context was acquired as silent.";
      tmp.name = "STATUS_SMARTCARD_SILENT_CONTEXT";
      break;
    case STATUS_SERVER_UNAVAILABLE:
      tmp.msg  = "The file server is temporarily unavailable.";
      tmp.name = "STATUS_SERVER_UNAVAILABLE";
      break;
    case STATUS_FILE_NOT_AVAILABLE:
      tmp.msg  = "The file is temporarily unavailable.";
      tmp.name = "STATUS_FILE_NOT_AVAILABLE";
      break;
    case STATUS_HASH_NOT_SUPPORTED:
      tmp.msg  = "Hash generation for the specified version and hash type is "
                 "not enabled on server.";
      tmp.name = "STATUS_HASH_NOT_SUPPORTED";
      break;
    case STATUS_HASH_NOT_PRESENT:
      tmp.msg  = "The hash requested is not present or not up to date with the "
                 "current file contents.";
      tmp.name = "STATUS_HASH_NOT_PRESENT";
      break;
    default:
      tmp.msg  = "Unrecognized status code.";
      tmp.name = "UNRECOGNIZED";
      break;
    }
  return( tmp );
  } /* ntstatus_getInfo */

char const *ntstatus_getName( const NTSTATUS ntstatus )
  /** Return just the name of an #NTSTATUS code.
   *
   * @param[in] ntstatus  An #NTSTATUS code.
   *
   * @returns   A pointer to the error code name string.
   */
  {
  ntstatus_Text tmp = ntstatus_getInfo( ntstatus );

  return( tmp.name );
  } /* ntstatus_getName */

char const *ntstatus_getMessage( const NTSTATUS ntstatus )
  /** Return just the message text associated with a given #NTSTATUS code.
   *
   * @param[in] ntstatus  An #NTSTATUS code.
   *
   * @returns   A pointer to the error message string.
   */
  {
  ntstatus_Text tmp = ntstatus_getInfo( ntstatus );

  return( tmp.msg );
  } /* ntstatus_getMessage */

char *ntstatus_getSeverityText( const NTSTATUS ntstatus )
  /** Return a string indicating the severity of an #NTSTATUS code.
   *
   * @param[in] ntstatus  An #NTSTATUS code, to be weighed and measured.
   *
   * @returns   A pointer to a constant string indicating the severity level
   *            of the given \p ntstatus.  This will be one of the following:
   *            - \c OKAY
   *            - \c INFO
   *            - \c WARN
   *            - \c ERROR
   */
  {
  switch( ntstatus_getSeverity( ntstatus ) )
    {
    case 0:  return( "OKAY" );
    case 1:  return( "INFO" );
    case 2:  return( "WARN" );
    default: break;
    }
  return( "ERROR" );
  } /* ntstatus_getSeverityText */

ntstatus_SubVals ntstatus_getSubVals( const NTSTATUS ntstatus )
  /** Parse an #NTSTATUS code into its various subfields.
   *
   * @param[in] ntstatus  An #NTSTATUS code, to be decomposed.
   *
   * @returns   An #ntstatus_SubVals structure containing the
   *            extracted #NTSTATUS subfields.
   */
  {
  ntstatus_SubVals tmp;

  tmp.severity = (uint8_t)ntstatus_getSeverity( ntstatus );
  tmp.customer = (0x20000000 & ntstatus) ? 1 : 0;
  tmp.N        = (0x10000000 & ntstatus) ? 1 : 0;
  tmp.facility = (uint16_t)((ntstatus >> 16) & 0x0FFF);
  tmp.errcode  = (uint16_t)((ntstatus & 0xFFFF));

  return( tmp );
  } /* ntstatus_getSubVals */

/* ================================ Whatever ================================ */
