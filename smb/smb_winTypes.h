#ifndef SMB_WINTYPES_H
#define SMB_WINTYPES_H
/* ========================================================================== **
 *                               smb_winTypes.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Support for various Windows-compatible data types.
 *
 * Copyright (C) 2019, 2024 by Christopher R. Hertel
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file      smb_winTypes.h
 * @author    Christopher R. Hertel
 * @brief     Implementations of several Windows-compatible SMB datatypes.
 * @date      12 Apr 2019
 * @version   \$Id: smb_winTypes.h; 2024-11-28 21:34:59 -0600; crh$
 * @copyright Copyright (C) 2019, 2024 by Christopher R. Hertel
 *
 *  <b>Time Format Conversions</b>
 *
 *  Windows and POSIX store time values very differently.
 *
 *  The Windows epoch is 01-01-1601-00:00:00; Midnight, January 1st, 1601.
 *  Windows measures time in units of 10^-7 seconds.  There is no official
 *  prefix for these units, but "bozo-" has been proposed.  Thus,
 *  "bozoseconds".  One bozosecond is equal to 100 nanoseconds, or one
 *  tenth of a microsecond.
 *
 *  The POSIX epoch is 01-01-1970 00:00:00, 369 years later than Windows.
 *  The POSIX <tt>struct timespec</tt> format splits the timestamp into
 *  seconds and nanoseconds.  Seconds are stored in a \c time_t integer,
 *  and nanoseconds are stored in a \c long.  Both fields are signed values,
 *  allowing for dates prior to the POSIX epoch.
 *
 *  SMB reports time in the Windows \c FILETIME format, which is defined
 *  as a pair of DWORD values.  A DWORD, in turn is defined as an unsigned
 *  32-bit integer.  In practical terms, however, a \c FILETIME value is a
 *  _signed_ 64-bit integer.  On this point, there is some conflict in the
 *  documentation; see [MS-FSCC; 2.1.1] vs. [MS-DTYP; 2.3.3] for examples.
 *
 *  There is a lot of documentation covering \c FILETIME, but much of it is
 *  unclear and/or contradictory.  Until more definitive information is
 *  available, this module will assume that \c FILETIME supports the full
 *  range of values from \c INT64_MIN to \c INT64_MAX.
 *
 *  This set of functions is used to convert between \c FILETIME and POSIX
 *  <tt>struct timespec</tt> format.  The <tt>struct timespec</tt> structure
 *  is described in <tt>time.h</tt> and sometimes in <tt>clock_gettime(2)</tt>.
 *
 * @see smb_endian.h
 * @see <a href="@posix/basedefs/time.h.html">time.h</a>
 * @see <a href="@msdocs/ms-fscc/a69cc039-d288-4673-9598-772b6083f8bf">[MS-FSCC;
 *  2.1.1]: Time</a>
 * @see <a href="@posix/functions/clock_gettime.html">clock_gettime(2)</a>
 * @see <a href="@msdocs/ms-dtyp/262627d8-3418-4627-9218-4ffe110850b2">[MS-DTYP;
 *  2.2.9]: DWORD</a>
 * @see <a href="@msdocs/ms-dtyp/2c57429b-fdd4-488f-b5fc-9e4cf020fcdf">[MS-DTYP;
 *  2.3.3]: FILETIME</a>
 * @see <a href="@mslrn:/win32/api/minwinbase/ns-minwinbase-filetime">FILETIME
 *  structure (minwinbase.h)</a>
 * @see Wikipedia: <a href="https://en.wikipedia.org/wiki/NTFS">NTFS</a>
 * @see Wikipedia: <a href="https://en.wikipedia.org/wiki/1601">1601</a>
 * @see <a href="http://ubiqx.org/cifs/SMB.html#SMB.6.3.1">Implementing CIFS,
 *  section 2.6.3.1</a>, under "SystemTimeLow and SystemTimeHigh"
 *
 * @todo
 *  - Verify that we properly handle 64 vs. 32 bit values.
 */

#include <stdint.h>     /* Standard integer types.  */


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @typedef smb_FileTime
 * @brief   Windows FILETIME equivalent.
 * @details
 *  In [MS-DTYPE], the \c FILETIME structure is given as a pair of DWORDs; (a
 *  pair of unsigned 32-bit integers).  In [MS-FSCC], the definition is
 *  further refined by specifying that a \c FILETIME should be interpreted as
 *  a _signed_ 64-bit integer.  This typedef codifies that by defining
 *  \c smb2_FileTime as an \c int64_t.
 *
 *  The name of this typedef is prefixed with "smb_" rather than "smb2_"
 *  because the same structure is used for SMB1 (CIFS) and is, therefore,
 *  not specific to SMB2/3.
 *
 * @see <a href="@msdocs/ms-dtyp/2c57429b-fdd4-488f-b5fc-9e4cf020fcdf">[MS-DTYP;
 *  2.3.3]: FILETIME</a>
 * @see <a href="@msdocs/ms-fscc/a69cc039-d288-4673-9598-772b6083f8bf">[MS-FSCC;
 *  2.1.1]: Time</a>
 */
typedef int64_t smb_FileTime;


/* -------------------------------------------------------------------------- **
 * Exported Functions
 */

struct timespec ftime2tspec( smb_FileTime const ftime );

smb_FileTime tspec2ftime( struct timespec pt );

smb_FileTime ftimeofday( void );

/* ============================= smb_winTypes.h ============================= */
#endif /* SMB_WINTYPES_H */
