#ifndef SMB_HEADER_H
#define SMB_HEADER_H
/* ========================================================================== **
 *                               smb2_header.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: SMB2/3 message header packing and parsing.
 *
 * Copyright (C) 2019 by Christopher R. Hertel
 * $Id: smb2_header.h; 2024-03-11 18:14:31 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file      smb2_header.h
 * @author    Christopher R. Hertel
 * @brief     SMB2 and SMB3 message header packing and parsing.
 * @date      18 Apr 2019
 * @version   \$Id: smb2_header.h; 2024-03-11 18:14:31 -0500; crh$
 * @copyright Copyright (C) 2019 by Christopher R. Hertel
 *
 * @details
 *  This module provides the tools and types used to compose and decompose
 *  SMB2/3 message headers.
 *
 *  All SMB2/3 command messages (both requests and responses) start with a
 *  messasge header.  As of SMBv3.0.0, there are two types of message
 *  header defined in [MS-SMB2]:
 *  - A standard SMB2/3 Message Header [MS-SMB2; 2.2.1]
 *  - A Transform Header [MS-SMB2; 2.2.41]
 *
 *  Both of these message types begin with a four-byte Protocol Identifier.
 *  - 0xfeSMB identifies an SMB2/3 message.
 *  - 0xfdSMB identifies an SMB3 Transform header.\n
 *  The transform header is used when the SMB3 session is encrypted.
 *
 *  The standard header contains the protocol identifier, command code
 *  and context information including flags, command window (credit)
 *  allocations, and a few other things.  Response headers also include
 *  an #NTSTATUS return code to indicate the success or failure of the
 *  command.
 *
 *  The standard header also comes in two flavors: Sync and Async.  This
 *  module provides support for parsing and packing both Sync and Async
 *  headers.
 *
 * @see <a
 *  href="@msdocs/ms-smb2/5606ad47-5ee0-437a-817e-70c366052962">[MS-SMB2]:
 *  Server Message Block (SMB) Protocol Versions 2 and 3</a>
 * @see <a href="@msdocs/ms-smb2/5cd64522-60b3-4f3e-a157-fe66f1228052">[MS-SMB2;
 *  2.2.1] SMB2 Packet Header</a>
 * @see <a href="@msdocs/ms-smb2/d6ce2327-a4c9-4793-be66-7b5bad2175fa">[MS-SMB2;
 *  2.2.41] SMB2 Transform Header</a>
 *
 * @todo
 *  - Add support for the transform header.
 *    - It may be best to start off trying to get compression working.
 *    - The crypto stuff may take a bit longer to work out.
 *  - Consider:  We could set SMB2_VMAX to 0xFFFF.
 *    - Stacked layers would use that to indicate that they wanted all
 *      data.
 *    - Counter argument:  If we are at (e.g.) 0x0311 and something new is
 *      added without creating a new dialect, how to handle?  The current
 *      definition of SMB2_VMAX allows us to adapt to changes in the
 *      current top-level dialect.  Older dialects (if history is any
 *      guide) won't change.
 *    - Also consider how this impacts outgoing messages and our cleanup
 *      thereof.
 */

#include <stdbool.h>        /* Standard boolean type. */

#include "smb_util.h"       /* SMB1/SMB2 common utilities.          */
#include "smb_ntstatus.h"   /* Include support for NTSTATUS codes.  */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *//**
 * @def     SMB2_HEADER_SIZE
 * @brief   The fixed size of the wire-format SMB2 header.
 * @details The wire format of SMB2/3 header is a fixed 64 bytes in length.
 *          The trailing 16 bytes of the header contain the (optional)
 *          message signature.
 *
 * @def     SMB2_HEADER_STRUCT_SIZE
 * @brief   The fixed value for the \c #smb2_Header.StructureSize field.
 * @details In SMB2, all fixed-legth structures have a \c StructureSize.
 *          If there is a variable-length portion following the fixed-length
 *          structure, the \c StructureSize will \e typically be one greater
 *          than the actual size of the structure.  The header is followed
 *          by the message body, so does not have a variable portion.  The
 *          \c StructureSize is, therefore, the same as the actual size of
 *          the header structure (<tt>SMB2_HEADER_SIZE</tt>)
 *
 * @def     SMB2_SIGNATURE_OFFSET
 * @brief   The offset from the start of the header at which the signature
 *          will be found.
 * @details The offset at which the 16-byte signature is located within the
 *          SMB2/3 message header.  This signature field is at a fixed
 *          location within the message header.  This is also the number
 *          of bytes that will be read by \c smb2_parseHeader().
 *
 * @def     SMB2_SIGNATURE_LENGTH
 * @brief   The length, in bytes, of an SMB2 message signature.
 * @details The message signature, even if it is not used, is always placed
 *          into the last 16 bytes of the SMB2/3 message header.
 *
 * @def     SMB2_VMAX
 * @brief   The maximum supported dialect number.
 * @details If this file is updated, or if a library that supports a higher
 *          level dialect is stacked on top, the \c SMB2_VMAX value should
 *          be changed or overridden to refect the new maximum dialect that
 *          is supported.
 */

#define SMB2_HEADER_SIZE        64
#define SMB2_HEADER_STRUCT_SIZE SMB2_HEADER_SIZE
#define SMB2_SIGNATURE_OFFSET   48
#define SMB2_SIGNATURE_LENGTH   16
#define SMB2_VMAX               SMB2_V0311


/* -------------------------------------------------------------------------- **
 * Enumerated Constants
 */

/** SMB2/3 Protocol Identifier codes.
 *
 *  In the original SMB protocol (created by Barry Feigenbaum at IBM in the
 *  1980's), each SMB message was prefixed by the string "\xffSMB".  This
 *  four-byte array served to identify the message as an SMB message over
 *  the simpler network transports in use at the time.  A new Protocol Id
 *  string, "\xfeSMB", was introduced to identify SMB2.  In SMB3, "Transform
 *  Headers" are used to carry encrypted SMB3 messages.  Transform Headers
 *  are identified by Protocol Id "\xfdSMB".
 *
 *  Note the following:
 *  - <tt>(int8_t)(-1) == 0xff</tt> : SMB1
 *  - <tt>(int8_t)(-2) == 0xfe</tt> : SMB2
 *  - <tt>(int8_t)(-3) == 0xfd</tt> : SMB3 Transform Header
 *
 *  Cute, eh?
 *
 *  Anyway, this set of constants is used to identify the type of message
 *  that is being transmitted.
 *
 *  \b Notes
 *  - Once a connection has been established there should be no ambiguity
 *    about which message type is in use and an incorrect Protocol Id should
 *    cause the connection to be dropped.
 *  - Historical note:  The \c 0xff prefix was referred to as a Message Type
 *    identifier (<tt>SMB_MSGTYP</tt>) in early IBM documentation.  IBM
 *    reserved the range 0x80..0xFF (128..255) but only ever used 0xFF.
 *
 * @see <a href="@msdocs/ms-cifs/69a29f73-de0c-45a6-a1aa-8ceeea42217f">[MS-CIFS;
 *  2.2.3.1] The SMB Header</a>
 * @see <a href="@msdocs/ms-smb2/5cd64522-60b3-4f3e-a157-fe66f1228052">[MS-SMB2;
 *  2.2.1] SMB2 Packet Header</a>
 * @see <a href="@msdocs/ms-smb2/d6ce2327-a4c9-4793-be66-7b5bad2175fa">[MS-SMB2;
 *  2.2.41] SMB2 Transform Header</a>
 */
typedef enum
  {
  PROTOCOL_ID_UNKNOWN   = 0x00, /**< Used for reporting errors.               */
  PROTOCOL_ID_SMB1      = 0xFF, /**< SMB1 (CIFS) Protocol Identifier.         */
  PROTOCOL_ID_SMB2      = 0xFE, /**< SMB2 Protocol Identifier.                */
  PROTOCOL_ID_SMB3XFORM = 0xFD  /**< SMB3 Transform (Encrypted) Protocol Id.  */
  } smb_protoIdCode;


/** SMB2/3 command codes.
 *
 *  There are nineteen command codes defined for SMB2/3, approximately 1/4th
 *  the number of command codes used in SMB1.  For each code, there is
 *  (typically) both a request and response format.  In addition, an error
 *  response message may be sent in response to a command.  Error response
 *  messages have their own format.
 *
 * @see <a href="@msdocs/ms-smb2/5cd64522-60b3-4f3e-a157-fe66f1228052">[MS-SMB2;
 *  2.2.1] SMB2 Packet Header</a>
 */
typedef enum
  {
  SMB2_COM_NEGOTIATE        = 0x0000, /**< Dialect and feature negotiation.   */
  SMB2_COM_SESSION_SETUP    = 0x0001, /**< Authentication and session config. */
  SMB2_COM_LOGOFF           = 0x0002, /**< Close an SMB session.              */
  SMB2_COM_TREE_CONNECT     = 0x0003, /**< Mount a share.                     */
  SMB2_COM_TREE_DISCONNECT  = 0x0004, /**< Unmount a share.                   */
  SMB2_COM_CREATE           = 0x0005, /**< Create/Open a file system object.  */
  SMB2_COM_CLOSE            = 0x0006, /**< Close an open file handle.         */
  SMB2_COM_FLUSH            = 0x0007, /**< Force buffered data to disk.       */
  SMB2_COM_READ             = 0x0008, /**< Read data.                         */
  SMB2_COM_WRITE            = 0x0009, /**< Write data.                        */
  SMB2_COM_LOCK             = 0x000A, /**< Byte-range locking.                */
  SMB2_COM_IOCTL            = 0x000B, /**< I/O Control.                       */
  SMB2_COM_CANCEL           = 0x000C, /**< Cancel a pending operation.        */
  SMB2_COM_ECHO             = 0x000D, /**< Knock-knock, anybody home?         */
  SMB2_COM_QUERY_DIRECTORY  = 0x000E, /**< Query metadata.                    */
  SMB2_COM_CHANGE_NOTIFY    = 0x000F, /**< Watch for changes in a directory.  */
  SMB2_COM_QUERY_INFO       = 0x0010, /**< Get specific metadata.             */
  SMB2_COM_SET_INFO         = 0x0011, /**< Set specific metadata.             */
  SMB2_COM_OPLOCK_BREAK     = 0x0012, /**< Server->client relinquesh OpLock.  */
  SMB2_COM_MAX                        /**< Value used for sanity checking.    */
  } smb2_hdrCommands;

/** SMB2/3 header Flags field values.
 *
 *  These, with two exceptions listed below, are the flags that are defined
 *  for the Flags field in the SMB2/3 message header.  The exceptions are:
 *  - #SMB2_FLAGS_PRIORITY_MASK is not a flag, but a subfield mask.
 *    Three bits are used to provide a priority value in the range [0..7].
 *  - #SMB2_FLAGS_MASK masks the set of valid Flag bits.
 *
 * @see <a href="@msdocs/ms-smb2/5cd64522-60b3-4f3e-a157-fe66f1228052">[MS-SMB2;
 *  2.2.1] SMB2 Packet Header</a>
 */
typedef enum
  {
  SMB2_FLAGS_SERVER_TO_REDIR    = 0x00000001, /**< Message is a Response      */
  SMB2_FLAGS_ASYNC_COMMAND      = 0x00000002, /**< Async header               */
  SMB2_FLAGS_RELATED_OPERATIONS = 0x00000004, /**< Chained (ANDX) commands    */
  SMB2_FLAGS_SIGNED             = 0x00000008, /**< Message is signed          */
  SMB2_FLAGS_PRIORITY_MASK      = 0x00000070, /**< Priority bits (uint3_t)    */
  SMB2_FLAGS_DFS_OPERATIONS     = 0x10000000, /**< Distributed FS Support     */
  SMB2_FLAGS_REPLAY_OPERATION   = 0x20000000, /**< SMB3 Replay                */
  SMB2_FLAGS_MASK               = 0x3000007F  /**< Flags Bitmask              */
  } smb2_hdrFlags;

/** SMB2/3 Dialect.
 *
 *  The protocol dialect to use is determined during protocol negotiation.
 *  The selected dialect impacts parsing and packing of all subsequent
 *  messages.
 *
 *  If support for a new dialect is added, or another module is stacked
 *  above this one, the value of #SMB2_VMAX should be changed or superceeded.
 *
 * @see <a href="@msdocs/ms-smb2/e14db7ff-763a-4263-8b10-0c3944f52fc5">[MS-SMB2;
 *  2.2.3]: SMB2 NEGOTIATE Request</a>
 */
typedef enum
  {
  SMB2_VNONE = 0,         /**< Unspecified dialect. */
  SMB2_V0202 = 0x0202,    /**< SMB2 revision 2.0.2. */
  SMB2_V0210 = 0x0210,    /**< SMB2 revision 2.1.0. */
  SMB2_VWILD = 0x02FF,    /**< SMB2 wildcard.       */
  SMB2_V0300 = 0x0300,    /**< SMB3 revision 3.0.0. */
  SMB2_V0302 = 0x0302,    /**< SMB3 revision 3.0.2. */
  SMB2_V0311 = 0x0311     /**< SMB4 revision 3.1.1. */
  } smb2_Dialect;


/* -------------------------------------------------------------------------- **
 * Macros
 */

/**
 * @def     HDR2_IS_RESPONSE( FLAGS )
 * @param   FLAGS   A 32-bit unsigned integer, representing the SMB2 header
 *                  \c Flags field in host byte order.
 * @brief   Test the SMB2_FLAGS_SERVER_TO_REDIR bit in in \p FLAGS.
 * @returns Boolean; true if the message is a response, else false.
 * @hideinitializer
 */
#define HDR2_IS_RESPONSE( FLAGS ) ((bool)((FLAGS) & SMB2_FLAGS_SERVER_TO_REDIR))

/**
 * @def     HDR2_IS_REQUEST( FLAGS )
 * @param   FLAGS   A 32-bit unsigned integer, representing the SMB2 header
 *                  \c Flags field in host byte order.
 * @brief   Test the SMB2_FLAGS_SERVER_TO_REDIR bit in \p FLAGS.
 * @returns Boolean; true if the message is a request, else false.
 * @hideinitializer
 */
#define HDR2_IS_REQUEST( FLAGS ) (!(bool)((FLAGS) & SMB2_FLAGS_SERVER_TO_REDIR))

/**
 * @def     HDR2_IS_ASYNC( FLAGS )
 * @param   FLAGS   A 32-bit unsigned integer, representing the SMB2 header
 *                  \c Flags field in host byte order.
 * @brief   Test the SMB2_FLAGS_ASYNC bit in the \c Flags field.
 * @returns Boolean; true for an ASYNC header, false for a SYNC header.
 * @hideinitializer
 */
#define HDR2_IS_ASYNC( FLAGS ) ((bool)((FLAGS) & SMB2_FLAGS_ASYNC_COMMAND))

/**
 * @def     HDR2_IS_RELATED( FLAGS )
 * @param   FLAGS   An unsigned 32-bit integer; the SMB2 header \c Flags field.
 * @brief   Test the SMB2_FLAGS_RELATED_OPERATIONS bit in \p FLAGS.
 * @returns Boolean; true for compound requests and responses to compound
 *          requests, else false.
 * @details Explanations of how and when this bit is set are referenced in
 *          the description of the \p Flags field in sections 2.2.1.1 and
 *          2.2.1.2 of [MS-SMB2].
 * @hideinitializer
 */
#define HDR2_IS_RELATED( FLAGS ) ((bool)((FLAGS)&SMB2_FLAGS_RELATED_OPERATIONS))

/**
 * @def     HDR2_IS_SIGNED( FLAGS )
 * @param   FLAGS   An unsigned 32-bit integer; the SMB2 header \c Flags field.
 * @brief   Test the SMB2_FLAGS_SIGNED bit in \p FLAGS.
 * @returns Boolean; true if the message is signed, else false.
 * @hideinitializer
 */
#define HDR2_IS_SIGNED( FLAGS ) ((bool)((FLAGS) & SMB2_FLAGS_SIGNED))

/**
 * @def     HDR2_GET_PRIORITY( FLAGS )
 * @param   FLAGS   An unsigned 32-bit integer; the SMB2 header \c Flags field.
 * @brief   Read the bits given by SMB2_FLAGS_PRIORITY_MASK as an integer.
 * @returns An unsigned 8-bit integer in the range 0 to 7.
 * @hideinitializer
 */
#define HDR2_GET_PRIORITY( FLAGS ) \
        (uint8_t)(((FLAGS) & SMB2_FLAGS_PRIORITY_MASK) >> 4)

/**
 * @def     HDR2_SET_PRIORITY( FLAGS, PRIO )
 * @param   FLAGS   An unsigned 32-bit integer; the SMB2 header \c Flags field.
 * @param   PRIO    A priority value, in the range 0..7.
 * @brief   The \c Priority subfield within the \c Flags field is replaced, in
 *          the return value, with the new \c Priority value given in \p PRIO.
 * @returns An unsigned 32-bit \c Flags value, including the given priority.
 * @hideinitializer
 */
#define HDR2_SET_PRIORITY( FLAGS, PRIO ) \
      (uint32_t)(((FLAGS) & ~SMB2_FLAGS_PRIORITY_MASK) | (((PRIO) & 0x07) << 4))

/**
 * @def     HDR2_IS_DFS( FLAGS )
 * @param   FLAGS   An unsigned 32-bit integer; the SMB2 header \c Flags field.
 * @brief   Test the SMB2_FLAGS_DFS_OPERATIONS bit in \p FLAGS.
 * @returns Boolean; true if the command is a DFS operation, else false.
 * @note    The SMB2_FLAGS_DFS_OPERATIONS flag should not be set in response
 *          messages.
 * @hideinitializer
 */
#define HDR2_IS_DFS( FLAGS ) ((bool)((FLAGS) & SMB2_FLAGS_DFS_OPERATIONS))

/**
 * @def     HDR2_IS_REPLAY( FLAGS )
 * @param   FLAGS   An unsigned 32-bit integer; the SMB2 header \c Flags field.
 * @brief   Test the SMB2_FLAGS_REPLAY_OPERATION bit in \p FLAGS.
 * @returns Boolean; true if the request is a replay operation, else false.
 * @note    The SMB2_FLAGS_REPLAY_OPERATION flag should not be set in response
 *          messages.
 * @hideinitializer
 */
#define HDR2_IS_REPLAY( FLAGS ) ((bool)((FLAGS) & SMB2_FLAGS_REPLAY_OPERATION))

/**
 * @def     HDR2_SIGNATURE_PTR( MSG_PTR )
 * @param   MSG_PTR   A pointer to the start of the message buffer.
 * @brief   Calculate a pointer to the message signature.
 * @returns A pointer to an array of bytes (uint8_t).
 * @details The signature is always 16 bytes in length and is always found
 *          at offset 48 within the SMB2/3 message header.
 * @hideinitializer
 */
#define HDR2_SIGNATURE_PTR( MSG_PTR ) \
        (&(((uint8_t *)MSG_PTR)[SMB2_SIGNATURE_OFFSET]))


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @struct  smb2_Header
 * @brief   SMB2/3 message header attributes.
 * @details
 *  The structure layout is inspired by the layout given in [MS-SMB2].
 *  Unions are used to map fields that overlap one another in the protocol,
 *  and reserved fields are given as byte arrays.
 *
 *  The term "Reserved-MB0" means that a field is reserved.  It should be
 *  set to zero by the sender and ignored by the receiver.  As always,
 *  check the protocol documentation for Windows-specific exceptions.  In
 *  general, such fields are not parsed when reading a message, and the
 *  structure values are not written to outgoing messages.
 *
 * \b Notes
 *
 *  - In the SMB 2.0.2 dialect, the #CreditCharge field is treated as
 *    "reserved, must be zero".  For dialects above 2.0.2, the value of this
 *    field is calculated by a formula specified in [MS-SMB2].
 *
 *  - The #ChannelSequence field overlaps the #Status field.
 *    #ChannelSequence, however, is only sent in request messages, while
 *    #Status is only sent in responses.  #ChannelSequence is not used in
 *    dialects below 3.0.0.  For those dialects, #ChannelSequence must be
 *    zero.
 *
 *  - Similarly, the #TreeId field occupies the latter half of the same 8
 *    bytes that are used to store the #AsyncId.  The #TreeId cannot be
 *    specified in an Async header.
 *
 *  - The #Credits field contains either the \c CreditRequest or
 *    \c CreditResponse value, depending upon whether the message is a
 *    request or response message.
 *
 *  - The \c Signature field is given as a pointer.  When creating a
 *    message, this pointer should always be NULL (and will be ignored).
 *    The signature will be added to the outgoing message after the
 *    message has been composed.  When an incoming message is parsed,
 *    this pointer will be set to point to the location in the input
 *    buffer at which the signature is located.  This will always be
 *    an offset 48 (0x30) bytes beyond the start of the SMB2 header.
 */
typedef struct
  {
  uint8_t  ProtocolId[4];     /**< Protocol identification string.            */
  uint16_t StructureSize;     /**< Must be 64.                                */
  uint16_t CreditCharge;      /**< Number of credits consumed (> SMB2.0.2).   */
  union
    {
    struct
      {
      uint16_t ChannelSequence; /**< Used for Multichannel; requests only.    */
      uint8_t  Reserved1[2];    /**< Reserved-MB0; 2 bytes, requests only.    */
      };
    NTSTATUS Status;          /**< SMB command return code; responses only.   */
    };
  uint16_t Command;           /**< SMB2/3 Command Code.                       */
  uint16_t Credits;           /**< Number of credits requested/granted.       */
  uint32_t Flags;             /**< Context info for message processing.       */
  uint32_t NextCommand;       /**< Offset of next chained command, else zero. */
  uint64_t MessageId;         /**< Unique within the scope of the connection. */
  union
    {
    struct
      {
      uint8_t  Reserved2[4];  /**< Reserved-MB0; 4 bytes, sync header only.   */
      uint32_t TreeId;        /**< TreeConnect (mount) instance identifier.   */
      };
    uint64_t AsyncId;         /**< Async Response Id; async header only.      */
    };
  uint64_t SessionId;         /**< Unique ID for an authenticated session.    */
  uint8_t *Signature;         /**< Pointer to the 16-byte message signature.  */
  } smb2_Header;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int smb2_parseHeader( const uint16_t     dialect,
                      uint8_t *const     msg,
                      smb2_Header *const hdr );

int smb2_packHeader( const uint16_t           dialect,
                     const smb2_Header *const hdr,
                     uint8_t *const           bufr );


/* ============================= smb2_header.h ============================== */
#endif /* SMB_HEADER_H */
