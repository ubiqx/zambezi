/* ========================================================================== **
 *                              smb2_errorResp.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Marshall/unmarshall SMB2/3 Error Response messages.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_errorResp.c; 2024-11-25 21:03:56 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <assert.h>           /* For debugging; see assert(3).  */

#include "smb_endian.h"       /* SMB byte orderings, get/put.   */
#include "smb2_errorResp.h"   /* Module header.                 */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *
 *  ERR_CTX_HDR_SIZE          - The size, in bytes, of the fixed portion of
 *                              an SMB2 Error Response Context Header.
 *
 *  BUFR_TOO_SMALL_CTX_SIZE   - The size, in bytes, of the fixed portion of
 *                              an SMB2 Error Response "Buffer Too Small"
 *                              context.  This context type does not have a
 *                              variable portion.
 *
 *  STOP_ON_SYMLINK_CTX_SIZE  - The size, in bytes, of the fixed portion of
 *                              an SMB2 Error Response "Stop On Symlink"
 *                              context.
 */

#define ERR_CTX_HDR_SIZE          8
#define BUFR_TOO_SMALL_CTX_SIZE   4
#define STOP_ON_SYMLINK_CTX_SIZE 28


/* -------------------------------------------------------------------------- **
 * Functions
 */

int smb2_parseErrorResp( uint16_t      const dialect,
                         uint8_t      *const msg,
                         size_t        const msgLen,
                         smb2_ErrResp *const errResp )
  /** Unmarshall an SMB2/3 Error Response message.
   *
   * @param[in]   dialect The dialect used when parsing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   msg     The source buffer, containing the received
   *                      message in wire format.  This must indicate the
   *                      start of the Error Response block, which
   *                      immediately follows the SMB2 header.
   * @param[in]   msgLen  The length of the \p msg buffer, in bytes.
   * @param[out]  errResp A pointer to an \c #smb2_ErrResp structure into
   *                      which the message will be parsed.  As usual, this
   *                      structure should be zeroed by the caller.
   *
   * @returns   On success, a positive value indicating the number of bytes
   *            of \p msg that have been parsed.\n
   *            On error, a negative value is returned.
   *
   * \b Errors
   *  - -1  \b ERROR: Malformed message; the \c StructureSize field did not
   *                  contain the required value.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the input \p msg is
   *                  too small to contain the fixed portion of an SMB2/3
   *                  Error Response.  A minimum of #SMB2_ERROR_RESP_SIZE
   *                  bytes are required.
   *
   * \b Notes
   *  - If the ErrorData is not fully contained in \p msg, then the
   *    \c ErrorData field will be set to NULL.  Otherwise, it will point
   *    to the location with \p msg at which the ErrorData begins.
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != errResp );
  if( msgLen < SMB2_ERROR_RESP_SIZE )                 /* Check buffer size.   */
    return( -SMB2_ERROR_RESP_SIZE );

  /* Read and test the StructureSize. */
  errResp->StructureSize = smbget16( msg );
  if( errResp->StructureSize != SMB2_ERROR_RESP_STRUCT_SIZE )
    return( -1 );

  /* Parse the remaining fields.  */
  errResp->ErrorContextCount = msg[2];
  errResp->Reserved          = msg[3];
  errResp->ByteCount         = smbget32( &msg[4] );
  errResp->ErrorData         = NULL;
  if( msgLen >= (errResp->ByteCount + SMB2_ERROR_RESP_SIZE) )
    errResp->ErrorData = &msg[SMB2_ERROR_RESP_STRUCT_SIZE];
  return( SMB2_ERROR_RESP_SIZE );
  } /* smb2_parseErrorResp */

int smb2_packErrorResp( uint16_t      const dialect,
                        smb2_ErrResp *const errResp,
                        uint8_t      *const bufr,
                        uint32_t      const bSize )
  /** Marshall an SMB2/3 Error Response message.
   *
   * @param[in]   dialect The dialect used when packing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   errResp A pointer to an \c #smb2_ErrResp structure that
   *                      provides all of the interesting stuff we are
   *                      going to pack into \p bufr.
   * @param[out]  bufr    A pointer to an array of bytes into which the
   *                      Error Response message will be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   The number of bytes written to \p bufr; this will be
                \c #SMB2_ERROR_RESP_SIZE.
   *
   * \b Notes
   *  - The function returns ±SMB2_ERROR_RESP_SIZE.
   *  - The contents of \c ErrorData are not copied into \p bufr.  Only the
   *    fixed portion of the message are marshalled into \p bufr.
   */
  {
  uint8_t *p;

  /* Did your sanity check bounce? */
  assert( NULL != bufr );
  assert( NULL != errResp );
  assert( SMB2_ERROR_RESP_SIZE <= bSize );

  /* Do the things with the stuff. */
  p = smbset16( bufr, errResp->StructureSize );
  p = smbset8(     p, errResp->ErrorContextCount );
  p = smbset8(     p, errResp->Reserved );
  p = smbset32(    p, errResp->ByteCount );
  return( SMB2_ERROR_RESP_SIZE );
  } /* smb2_packErrorResp */

int smb2_parseErrCtxResp( uint16_t         const dialect,
                          uint8_t         *const msg,
                          size_t           const msgLen,
                          smb2_ErrCtxResp *const ctxHdr )
  /** Unpack an Error Response Context Header.
   *
   * In SMB2/3 dialects prior to 3.1.1, the \c #smb2_ErrResp::ErrorData
   * field will contain zero or one error contexts, depending upon the
   * command that was sent by the client and the error condition that
   * occurred.
   *
   * In SMBv3.1.1 and above, the \c #smb2_ErrResp::ErrorData field will
   * contain a list of zero or more error contexts.  Each of the error
   * contexts will be preceded by a header (see \c #smb2_ErrCtxResp)
   * that includes the size and type of the context it precedes.  Each
   * context header \e must start on an 8-byte offset.  This information,
   * along with the \c #smb2_ErrResp::ErrorContextCount, is sufficient to
   * parse the Error Context List.
   *
   * @param[in]   dialect The dialect used when parsing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   msg     The source buffer, containing the received
   *                      Context Header in wire format.
   * @param[in]   msgLen  The length of \p msg, in bytes.
   * @param[out]  ctxHdr  Pointer to a \c #smb2_ErrCtxResp structure into
   *                      which the Context Header will be extracted.
   *
   * @returns   The number of bytes of \p msg that were consumed (8) or a
   *            negative value on error.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; at least \c 8 bytes are required.
   *
   * \b Notes
   *  - The \c ErrorContextData pointer in \p ctxHdr will be set to NULL
   *    unless the entirety of the Error Context Data would fit into \p msg
   *    (as indicated by \p msgLen).  In the latter case, the
   *    \c ErrorContextData pointer will point to the start of the Error
   *    Context Data.
   */
  {
  assert( NULL != msg );
  assert( NULL != ctxHdr );
  if( msgLen < ERR_CTX_HDR_SIZE )
    return( -ERR_CTX_HDR_SIZE );

  ctxHdr->ErrorDataLength  = smbget32( msg );
  ctxHdr->ErrorId          = smbget32( &msg[4] );
  ctxHdr->ErrorContextData = NULL;
  if( (ERR_CTX_HDR_SIZE + ctxHdr->ErrorDataLength) <= msgLen )
    ctxHdr->ErrorContextData = &msg[8];
  return( ERR_CTX_HDR_SIZE ); /* 'nough said. */
  } /* smb2_parseErrCtxResp */

int smb2_packErrCtxResp( uint16_t         const dialect,
                         smb2_ErrCtxResp *const ctxHdr,
                         uint8_t         *const bufr,
                         uint32_t         const bSize )
  /** Compose an Error Response Context Header.
   *
   * @param[in]   dialect The dialect used when packing the Context Header.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   ctxHdr  A pointer to an \c #smb2_ErrCtxResp structure
   *                      that provides the goodies we are going to pack
   *                      into \p bufr.
   * @param[out]  bufr    A pointer to an array of bytes into which the
   *                      Error Response Context Header will be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   The number of bytes written to \p bufr; this will be eight
   *            (8) bytes.
   *
   * \b Notes
   *  - As usual, the contents of \c #smb2_ErrCtxResp::ErrorContextData are
   *    not copied into the output buffer.  Only the fixed portion of the
   *    Error Response Context Header are written to \p bufr.
   *
   * @see #smb2_parseErrCtxResp()
   */
  {
  uint8_t *p = bufr;

  assert( NULL != bufr );
  assert( NULL != ctxHdr );
  assert( ERR_CTX_HDR_SIZE <= bSize );

  p = smbset32( p, ctxHdr->ErrorDataLength );
  p = smbset32( p, ctxHdr->ErrorId );

  return( ERR_CTX_HDR_SIZE );
  } /* smb2_packErrCtxResp */

int smb2_parseBufferCtx( uint16_t           const dialect,
                         uint8_t           *const msg,
                         size_t             const msgLen,
                         smb2_ErrBufferCtx *const bufCtx )
  /** Unpack a "Buffer Too Small" Error Response Context.
   *
   * @param[in]   dialect The dialect used when parsing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   msg     The source buffer, containing the received
   *                      Context in wire format.
   * @param[in]   msgLen  The length of the source buffer, in bytes.
   * @param[out]  bufCtx  A pointer to an \c #smb2_ErrBufferCtx structure
   *                      into which the buffer will be parsed.
   *
   * @returns   On success, the return value is the number of bytes that
   *            were consumed (4).  On error, a negative value is returned.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; at least \c 4 bytes are required.
   *
   * \b Notes
   *  - The "Buffer Too Small" Context is a 32-bit unsigned integer.
   *    That's all there is to it.  This function provides some error
   *    detection, but otherwise only extracts the 32-bit value from \p msg.
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != bufCtx );
  if( msgLen < BUFR_TOO_SMALL_CTX_SIZE )
    return( -BUFR_TOO_SMALL_CTX_SIZE );

  bufCtx->minBuffer = smbget32( msg );
  return( BUFR_TOO_SMALL_CTX_SIZE );
  } /* smb2_parseBufferCtx */

int smb2_packBufferCtx( uint16_t           const dialect,
                        smb2_ErrBufferCtx *const bufCtx,
                        uint8_t           *const bufr,
                        uint32_t           const bSize )
  /** Pack a "Buffer Too Small" Error Response Context.
   *
   * @param[in]   dialect The dialect used when packing the message.
   *                      There are no dialect-specific variations to this
   *                      message type, so the \p dialect value is ignored.
   *                      This parameter is included for interface
   *                      consistency.
   * @param[in]   bufCtx  A pointer to an \c #smb2_ErrBufferCtx structure
   *                      that provides the one field to be packed into
   *                      \p bufr.
   * @param[out]  bufr    A pointer to an array of bytes into which the
   *                      context will be written.
   * @param[in]   bSize   The size, in bytes, of \p bufr.
   *
   * @returns   The number of bytes that were written to \p bufr (4 bytes).
   *
   * \b Notes
   *  - This function does little more than writing the 32-bit
   *    \c #smb2_ErrBufferCtx::minBuffer value to \p bufr in SMB byte order.
   */
  {
  /* Checks */
  assert( NULL != bufr );
  assert( NULL != bufCtx );
  assert( BUFR_TOO_SMALL_CTX_SIZE <= bSize );

  /* Do the one thing with the one stuff. */
  (void)smbset32( bufr, bufCtx->minBuffer );
  return( BUFR_TOO_SMALL_CTX_SIZE );
  } /* smb2_packBufferCtx */

int smb2_parseSymLinkCtx( uint16_t            const dialect,
                          uint8_t            *const msg,
                          size_t              const msgLen,
                          smb2_ErrSymLinkCtx *const sLinkCtx )
  /** Unpack a "Stop On Symlink" Error Response Context.
   *
   * @param[in]   dialect   The dialect used when parsing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   msg       The source buffer, containing the received
   *                        Context in wire format.
   * @param[in]   msgLen    The length of the source buffer, in bytes.
   * @param[out]  sLinkCtx  A pointer to an \c #smb2_ErrSymLinkCtx structure
   *                        into which the buffer will be parsed.
   *
   * @returns   On success, the return value is the number of bytes that
   *            were consumed (28).  On error, a negative value is returned.
   *
   * \b Errors
   *  - -n  \b ERROR: Short buffer; at least \c |n| bytes are required.
   *                  The fixed portion of the "Stop On Symlink" Error
   *                  Response Context is 28 bytes in length.
   *
   * \b Notes
   *  - The \c PathBuffer field will be NULL unless the entirety of the
   *    PathBuffer is included in \p msg.  This is determined by performing
   *    two tests:
   *    1.  First we check that \p msgLen is at least 28, which is the
   *        number of bytes in the fixed-length portion of this Context.
   *        It's a quick and easy test, and it reduces the risk that an
   *        invalid value in the \c SymLinkLength field will cause pain.
   *    2.  The next check is to calculate the size of the full Context,
   *        including the variable part.  The \c SymLinkLength field
   *        (for reasons that are probably steeped in Windows entrails)
   *        is meant to be 4 less than the total Context length.
   *
   *    If the input passes both tests, then the \p SymLinkLength field
   *    in \p sLinkCtx will be set to point to the location in \p msg at
   *    which the PathBuffer begins.
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != sLinkCtx );
  if( msgLen < STOP_ON_SYMLINK_CTX_SIZE )
    return( -STOP_ON_SYMLINK_CTX_SIZE );

  sLinkCtx->SymLinkLength         = smbget32( msg );
  sLinkCtx->SymLinkErrorTag       = smbget32( &msg[4] );
  sLinkCtx->ReparseTag            = smbget32( &msg[8] );
  sLinkCtx->ReparseDataLength     = smbget16( &msg[12] );
  sLinkCtx->UnparsedPathLength    = smbget16( &msg[14] );
  sLinkCtx->SubstituteNameOffset  = smbget16( &msg[16] );
  sLinkCtx->SubstituteNameLength  = smbget16( &msg[18] );
  sLinkCtx->PrintNameOffset       = smbget16( &msg[20] );
  sLinkCtx->PrintNameLength       = smbget16( &msg[22] );
  sLinkCtx->Flags                 = smbget32( &msg[24] );
  sLinkCtx->PathBuffer            = NULL;

  /* Check that we have enough bytes for the start of the PathBuffer and
   * the complete PathBuffer.  The latter is based upon the value of the
   * SymLinkLength field.  We add 4 (the size of the SymLinkLength field)
   * as prescribed in [MS-SMB2;2.2.2.2.1].
   */
  if( (msgLen >= STOP_ON_SYMLINK_CTX_SIZE)
   && (msgLen >= 4 + sLinkCtx->SymLinkLength) )
    {
    sLinkCtx->PathBuffer = &msg[28];
    }
  return( STOP_ON_SYMLINK_CTX_SIZE );
  } /* smb2_parseSymLinkCtx */

int smb2_packSymLinkCtx( uint16_t            const dialect,
                         smb2_ErrSymLinkCtx *const sLinkCtx,
                         uint8_t            *const bufr,
                         uint32_t            const bSize )
  /** Pack a "Stop On Symlink" Error Response Context.
   *
   * @param[in]   dialect   The dialect used when packing the message.
   *                        There are no dialect-specific variations to this
   *                        message type, so the \p dialect value is ignored.
   *                        This parameter is included for interface
   *                        consistency.
   * @param[in]   sLinkCtx  A pointer to an \c #smb2_ErrSymLinkCtx structure
   *                        that provides the one field to be packed into
   *                        \p bufr.
   * @param[out]  bufr      A pointer to an array of bytes into which the
   *                        context will be written.
   * @param[in]   bSize     The size, in bytes, of \p bufr.
   *
   * @returns   The number of bytes written to \p bufr (28 bytes).
   *
   * \b Notes
   *  - Only the fixed-length portion of the \c #smb2_ErrSymLinkCtx
   *    structure are marshalled into \p bufr.  The \c PathBuffer contents
   *    are not.
   */
  {
  uint8_t *p = bufr;

  /* Balances */
  assert( NULL != bufr );
  assert( NULL != sLinkCtx );
  assert( STOP_ON_SYMLINK_CTX_SIZE <= bSize );

  /* Pack the buffer. */
  p = smbset32( p, sLinkCtx->SymLinkLength );
  p = smbset32( p, sLinkCtx->SymLinkErrorTag );
  p = smbset32( p, sLinkCtx->ReparseTag );
  p = smbset16( p, sLinkCtx->ReparseDataLength );
  p = smbset16( p, sLinkCtx->UnparsedPathLength );
  p = smbset16( p, sLinkCtx->SubstituteNameOffset );
  p = smbset16( p, sLinkCtx->SubstituteNameLength );
  p = smbset16( p, sLinkCtx->PrintNameOffset );
  p = smbset16( p, sLinkCtx->PrintNameLength );
  p = smbset32( p, sLinkCtx->Flags );
  return( STOP_ON_SYMLINK_CTX_SIZE );
  } /* smb2_packSymLinkCtx */


/* ============================== Hetay Ndeay =============================== */
