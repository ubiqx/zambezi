/* ========================================================================== **
 *                               smb_winTypes.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Support for various Windows-compatible data types.
 *
 * Copyright (C) 2019, 2024 by Christopher R. Hertel
 * $Id: smb_winTypes.c; 2024-11-28 12:49:45 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <time.h>         /* For POSIX time functions.  */
#include <stddef.h>       /* For NULL.                  */

#include "smb_winTypes.h" /* Module header.             */


/* -------------------------------------------------------------------------- **
 * Macros
 *
 *  SIGNUM  - Quick macro to determine the sign of a value.
 *            Returns -1 for negative numbers, 1 for positive, and 0 for 0.
 *            Note that the input parameter may be evaluated twice.
 */

#define SIGNUM( A ) (((A)<0)?-1:((A)>0)?1:0)


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *
 *  BOZILLION   - Ten million, or 10^7.  The Windows FILETIME type is
 *                expressed in units of tenths of a microsecond.  Ten
 *                million tenths of a microsecond is one second.
 *
 *  ABILLION    - A billion, or 10^9.  The struct timespec structure
 *                stores seconds and nanoseconds.  The latter are one
 *                billionth of a second.
 *
 *  EPOCH_DIFF  - The difference, in seconds, between the Windows FILETIME
 *                and POSIX Epochs.
 */

#define BOZILLION   10000000L
#define ABILLION    1000000000L
#define EPOCH_DIFF  11644473600LL


/* -------------------------------------------------------------------------- **
 * Functions
 */

struct timespec ftime2tspec( smb_FileTime const ftime )
  /** Translate a Windows \c FILETIME value into a <tt>struct timespec</tt>.
   *
   * @param   [in] ftime  A time value in Windows \c FILETIME format.
   *
   * @returns A <tt>struct timespec</tt> value representing the same
   *          date/time as the input value.  The timespec value will
   *          be normalized.  The resulting date will be in the range
   *          [1-Jan-1601..28-May-60056].
   *
   * \b Notes
   *  - In this context, a normalized time means that the value of the
   *    \c tv_nsec field must be in the range 0...999999999.  That is, a
   *    non-negative integer less than one billion.  One billion
   *    nanoseconds is one second, so it works out just fine.
   *  - If the \c time_t type is less than 64bits in size, we assume that it
   *    is 32bits in size.  Any time value outside of the available range is
   *    forced to \c INT32_MIN or \c INT32_MAX depending, respectively, on
   *    whether it is negative or positive.
   *  - The POSIX specification for \c clock_gettime(2), et. al., defines
   *    the nanosecond range for the \c tv_nsec field in the description of
   *    the \c EINVAL error code (and a few other places as well).
   *
   * @see <a href="@posix/functions/clock_gettime.html">clock_gettime(2)</a>
   */
  {
  struct timespec pt;
  smb_FileTime seconds = (ftime / BOZILLION);     /* Seconds since Win epoch. */
  int64_t tvsec        = (seconds - EPOCH_DIFF);  /* Seconds since UNIX epoch.*/

  /* Check for 32bit time_t. */
  if( sizeof( time_t ) < 8 )
    {
    /* Limit results to the valid range. */
    if( tvsec <= INT32_MIN )
      {
      pt.tv_sec  = INT32_MIN;
      pt.tv_nsec = 0;
      return( pt );
      }
    if( tvsec >= INT32_MAX )
      {
      pt.tv_sec  = INT32_MAX;
      pt.tv_nsec = 0;
      return( pt );
      }
    }

  /* Normal conversion. */
  pt.tv_sec  = (time_t)tvsec;
  pt.tv_nsec = 100 * (long)(ftime % BOZILLION);

  /* Negative normalization. */
  if( ftime < 0 )
    {
    pt.tv_sec  -= 1;
    pt.tv_nsec += ABILLION;
    }
  return( pt );
  } /* ftime2tspec */

smb_FileTime tspec2ftime( struct timespec pt )
  /** Convert from POSIX \c timespec to Windows \c FILETIME format.
   *
   * @param   pt  A time value in POSIX <tt>struct timespec</tt> format.
   *
   * @returns The same time as the input, presented in FILETIME format.
   *
   * \b Notes
   *  - The resolution of the \c FILETIME format is in bozoseconds, but
   *    the POSIX timestamp has a resolution in nanoseconds.  Rounding
   *    may occur as a result.
   *  - On a POSIX platform, the \em real resolution of the underlying
   *    hardware can be retrieved by a call to clock_getres(2).
   *  - A 64bit \c time_t can represent a date well beyond the range
   *    of a 64bit \c FILETIME, since the former is in seconds.  In
   *    practice, dates beyond the \c FILETIME don't make sense, so
   *    we don't even check for them.
   */
  {
  smb_FileTime ftime = ((smb_FileTime)(EPOCH_DIFF + pt.tv_sec) * BOZILLION);

  return( (smb_FileTime)(ftime + (smb_FileTime)((pt.tv_nsec + 50) / 100)) );
  } /* tspec2ftime */

smb_FileTime ftimeofday( void )
  /** Return the current UTC time as a \c FILETIME value.
   *
   * @returns The current UTC time and date in FILETIME format.
   */
  {
  struct timespec pt;
  smb_FileTime    ftime;

  (void)clock_gettime( CLOCK_REALTIME, &pt );
  ftime = ((smb_FileTime)(pt.tv_sec + EPOCH_DIFF) * BOZILLION);
  return( ftime + (smb_FileTime)(pt.tv_nsec / 100) );
  } /* ftimeofday */


/* -------------------------------------------------------------------------- **
 * Unit Tests
 *
 *  Define UNIT_TEST to compile these unit tests.
 *    $ cc -Wall -D UNIT_TEST -o utest smb_winTypes.c
 *    $ ./utest
 */

#ifdef UNIT_TEST

#include <stdio.h>    /* For diagnostic output. */
#include <stdlib.h>   /* Exit codes and abs(3). */
#include <stdbool.h>  /* True/false.            */
#include <string.h>   /* For memset(3).         */
#include <assert.h>   /* For assert(3).         */

#define Say (void)printf  /* Shorthand. */


static void printTime( struct timespec pt )
  /* ------------------------------------------------------------------------ **
   * Print the timespec in seconds.nanoseconds format and as UTC time of day.
   *
   *  Input:  pt  - The time value to display.
   * ------------------------------------------------------------------------ **
   */
  {
  struct tm *tmp_tm = NULL;

  Say( "%13ld.%09ld; ", pt.tv_sec, pt.tv_nsec );
  if( NULL == (tmp_tm = gmtime( &(pt.tv_sec) )) )
    Say( "NULL== gmtime(): Date exceeds bounds; %lx.\n", pt.tv_sec );
  else
    Say( "%s", asctime( tmp_tm ) );
  } /* printTime */

static bool withinMicro( struct timespec a, struct timespec b )
  /* ------------------------------------------------------------------------ **
   * See if two timespec values are within a microsecond of one another.
   *
   *  Input:  a, b  - A pair of timespec values to be compared.
   *
   *  Output: False if the two input values differ by a microsecond or more,
   *          else true.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  int64_t delta_sec;
  int64_t delta_nsec;

  /* If we're _more than_ a second off, then even 999999999 nanoseconds won't
   * put us into the +/-1000ns range.  This is a quick check.
   */
  delta_sec = (int64_t)(a.tv_sec - b.tv_sec);
  if( (delta_sec < -1) || (delta_sec > 1) )
    return( false );

  /* Normalize to nanoseconds, then check again.
   */
  delta_nsec = (delta_sec * 1000000000) + (a.tv_nsec - b.tv_nsec);
  if( (delta_nsec < -1000) || (delta_nsec > 1000) )
    return( false );

  /* We're within the +/-1000ns (+/-1us) range. */
  return( true );
  } /* withinMicro */

static void timeTests( void )
  /* ------------------------------------------------------------------------ **
   * Run unit tests on the FILETIME/timespec conversion methods.
   * ------------------------------------------------------------------------ **
   */
  {
  struct tm       tm_struct[1]; /* POSIX Time Structure.    */
  struct timespec pt, pt2;      /* POSIX timespec struct.   */
  uint64_t        ftime;        /* Windows FILETIME value.  */

  /* Okay, so... epoch tests. */
  Say( "[Epoch Tests]\n" );

  /* Calculate the Epoch Offset.
   *  Calculate the difference, in seconds, between the Windows and POSIX
   *  epoch dates.  This should be equal to EPOCH_DIFF unless the
   *  sizeof( time_t ) is less than 64 bits.
   *  NOTE: The timegm(3) function is non-standard.  See the Linux, FreeBSD,
   *        and OpenBSD man pages.  [Use of timegm(3) is restricted to this
   *        unit test code.]
   *  NOTE: struct tm years begin at 1900, not 1970.
   */
  (void)memset( tm_struct, 0, sizeof( struct tm ) );
  tm_struct->tm_year = -299;  /* 1601; tm_struct years start at 1900. */
  tm_struct->tm_mon  =    0;  /* Jan; tm_struct months start at zero. */
  tm_struct->tm_mday =    1;  /* First day of the month.              */
  pt.tv_sec = -timegm( tm_struct );
  Say( "Calculated Epoch Offset: %13ld\n", pt.tv_sec );
  assert( EPOCH_DIFF == pt.tv_sec );

  /* The POSIX Epoch, using a timespec of zero. */
  pt.tv_sec = pt.tv_nsec = 0;
  Say( "POSIX Epoch............: " );
  printTime( pt );

  /* We cannot test epoch calculations if time_t is 32 bits. */
  if( sizeof( time_t ) < 8 )
    {
    Say( "The Windows Epoch exceeds the range of a 32bit `time_t`.\n" );
    Say( "32bit MIN Unix date....: " );
    pt = ftime2tspec( INT64_MIN );
    printTime( pt );
    assert( INT32_MIN == pt.tv_sec );

    Say( "32bit MAX Unix date....: " );
    pt = ftime2tspec( INT64_MAX );
    printTime( pt );
    assert( INT32_MAX == pt.tv_sec );
    }
  else
    {
    /* The Windows Epoch, using a FILETIME of zero. */
    Say( "Windows Epoch..........: " );
    pt = ftime2tspec( 0 );
    printTime( pt );
    assert( -EPOCH_DIFF == pt.tv_sec );

    /* This should result in a zero timespec; POSIX Epoch. */
    Say( "EPOCH_DIFF as FileTime.: " );
    pt = ftime2tspec( EPOCH_DIFF * BOZILLION );
    printTime( pt );
    assert( 0 == pt.tv_sec );

    /* This should result in the maximum FILETIME value. */
    Say( "Maximum FILETIME date..: " );
    ftime = INT64_MAX;
    pt    = ftime2tspec( ftime );
    printTime( pt );
    assert( ((ftime / BOZILLION) - EPOCH_DIFF) == pt.tv_sec );

    /* This should result in the minimum FILETIME value.  */
    Say( "Minimum FILETIME date..: " );
    ftime = INT64_MIN;
    pt    = ftime2tspec( ftime );
    printTime( pt );
    assert( tspec2ftime( pt ) == ftime );
    }
  Say( "[Passed]\n\n" );

  /* Check FILETIME now vs. actual now. */
  Say( "[Format Conversion Tests]\n" );
  Say( "Results should differ by a few hundred ns at most...\n" );
  (void)clock_gettime( CLOCK_REALTIME, &pt );
  ftime = ftimeofday();
  Say( "System Now (ns)........: " );
  printTime( pt );
  Say( "FILETIME Now (bs)......: " );
  pt2 = ftime2tspec( ftime );
  printTime( pt2 );
  assert( (ftime / BOZILLION) >= pt.tv_sec );
  assert( withinMicro( pt, pt2 ) );

  /* Convert a date to timespec, then to FILETIME, then back to timespec.
   *  Note: Once again, we're using the timegm(3) function.  This should
   *        be (but isn't) a standard function, since there's no other
   *        (good) way to convert struct tm times to UTC time values.
   *        Several example portable implementations can be found on
   *        on the web.
   */
  Say( "50ns rounds up to 1bs...\n" );
  (void)memset( tm_struct, 0, sizeof( struct tm ) );
  tm_struct->tm_year = 63;  /* 1963; Dates start at 1900. */
  tm_struct->tm_mon  = 10;  /* Nov; Months start at zero. */
  tm_struct->tm_mday = 23;
  tm_struct->tm_hour = 17;
  tm_struct->tm_min  = 16;
  tm_struct->tm_sec  = 20;
  pt.tv_sec = timegm( tm_struct );
  pt.tv_nsec = 50;
  assert( -192696220 == pt.tv_sec );
  Say( "Original...............: " );
  printTime( pt );
  pt2 = ftime2tspec( tspec2ftime( pt ) );
  Say( "Twice Converted........: " );
  printTime( pt2 );
  assert( withinMicro( pt, pt2 ) );
  Say( "[Passed]\n" );

  } /* timeTests */

int main( void )
  /* ------------------------------------------------------------------------ **
   * Run a series of unit tests.
   * ------------------------------------------------------------------------ **
   */
  {
  timeTests();
  return( EXIT_SUCCESS );
  } /* main */

#endif /* UNIT_TEST */


/* ============================ Time for Dessert ============================ */
