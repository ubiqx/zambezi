/* ========================================================================== **
 *                               smb2_header.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: SMB2/3 message header packing and parsing.
 *
 * Copyright (C) 2019 by Christopher R. Hertel
 * $Id: smb2_header.c; 2024-11-10 20:20:38 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <assert.h>       /* For debugging.             */
#include <string.h>       /* For memcmp(3) & memcpy(3). */

#include "smb_endian.h"   /* Convert between host and SMB byte order. */
#include "smb2_header.h"  /* Module header.                           */


/* -------------------------------------------------------------------------- **
 * Static Constants
 *
 *  PROTOCOL_STR  - The first field of all SMB message headers is the
 *                  ProtocolId field.  In SMB1, this field contains the
 *                  string "\xffSMB".  In SMB2 and SMB3, the ProtocolId
 *                  field MUST contain "\xfeSMB".  Read as a signed 8-bit
 *                  integer, 0xff is -1.  Similarly, 0xfe would be -2.
 */

static char const *const PROTOCOL_STR = "\xfeSMB";


/* -------------------------------------------------------------------------- **
 * Functions
 */

int smb2_parseHeader( const uint16_t     dialect,
                      uint8_t *const     msg,
                      smb2_Header *const hdr )
  /** Given an input buffer, unpack the fields of a standard SMB2/3 header.
   *
   * @param[in]     dialect The dialect revision number that has been
   *                        negotiated for the connection.  This \b MUST be
   *                        \c #SMB2_VNONE if the protocol dialect has not
   *                        yet been negotiated, in which case the message
   *                        \b MUST be an SMB2 Negotiate Request.
   * @param[in]     msg     A (constant) pointer to an array of bytes.  The
   *                        buffer \b MUST contain a complete SMB2/3 header,
   *                        in wire format.
   * @param[in,out] hdr     A (constant) pointer to an \c #smb2_Header
   *                        structure.  The structure will be filled in with
   *                        values extracted from \p msg.
   *
   * @returns   \c #SMB2_HEADER_SIZE, or a negative number on error. \n
   *            On success, the size of the wire-format header, in bytes,
   *            is returned.  This indicates the number of bytes of \p msg
   *            that were "consumed" by message parsing.
   *
   * @par Errors
   *   The following error codes indicate that the input \p msg does not
   *   contain a valid SMB2/3 message header:
   *   - \b -1 \b ERROR: The \c ProtocolId did not match the value defined
   *                     for standard SMB2/3 headers (<tt>"\xfeSMB"</tt>).
   *                     The invalid \c ProtocolId value will be returned
   *                     in the \c hdr->ProtocolId field.
   *   - \b -2 \b ERROR: The \c StructureSize value was incorrect.  The
   *                     invalid \c StructureSize value will be returned
   *                     in the \c hdr->StructureSize field.
   *   @par
   *      See <a
   *      href="@msdocs/ms-smb2/853e5234-d5d6-4a18-a2f5-1c792e562947">[MS-SMB2;
   *      3.3.5.2.6]</a> for information on handling these errors.
   *
   * \b Notes
   *  - This function parses both sync and async headers.
   *  - It is assumed that basic sanity checking has been performed before
   *    this function is called.  However, the \c assert(3) macro is used to
   *    check that the input parameters are not NULL.  For production use,
   *    compile with \c NDEBUG set.
   *  - For dialects _prior to_ #SMB2_VMAX, the #smb2_Header::Flags are
   *    masked so that only flags defined for the specified dialect are
   *    returned.  This is "correct"; undefined bits should be ignored.
   *    For the #SMB2_VMAX dialect, however, no mask is applied.  This
   *    supports parser stacking.  All values (including undefined flag
   *    bits and reserved fields) are returned so that a higher level
   *    can perform further parsing if needed.
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != hdr );
  /* ProtocolID. */
  (void)memcpy( hdr->ProtocolId, PROTOCOL_STR, 4 );
  if( 0 != memcmp( PROTOCOL_STR, msg, 4 ) )     /* Must be "\xfeSMB". */
    return( -1 );
  /* StructureSize. */
  hdr->StructureSize = smbget16( msg + 4 );
  if( SMB2_HEADER_STRUCT_SIZE != hdr->StructureSize )
    return( -2 );

  /* The easy-to-parse fields of the header. */
  hdr->Command     = smbget16( &msg[12] );
  hdr->Credits     = smbget16( &msg[14] );
  hdr->Flags       = smbget32( &msg[16] );
  hdr->NextCommand = smbget32( &msg[20] );
  hdr->MessageId   = smbget64( &msg[24] );
  hdr->SessionId   = smbget64( &msg[40] );
  hdr->Signature   = HDR2_SIGNATURE_PTR( msg );

  /* Dialect-specific parsing. */
  if( dialect < SMB2_VMAX )
    {
    /* Except for the currently supported maximum dialect, apply the mask. */
    hdr->Flags &= SMB2_FLAGS_MASK;
    if( dialect < SMB2_V0311 )
      {
      hdr->Flags &= ~(uint32_t)SMB2_FLAGS_PRIORITY_MASK;
      if( dialect < SMB2_V0300 )
        hdr->Flags &= ~(uint32_t)SMB2_FLAGS_REPLAY_OPERATION;
      }
    }

  /* Parse fields depending upon dialect and request/response status. */
  if( (dialect < SMB2_V0300) || HDR2_IS_RESPONSE( hdr->Flags ) )
    hdr->Status = smbget32( &msg[8] );
  else
    {
    hdr->ChannelSequence = smbget16( &msg[8] );
    (void)memcpy( hdr->Reserved1, &msg[10], 2 );
    }

  if( dialect > SMB2_V0210 )
    hdr->CreditCharge = smbget16( &msg[6] );

  /* Header-type-specific parsing. */
  if( HDR2_IS_ASYNC( hdr->Flags ) )
    hdr->AsyncId = smbget64( &msg[32] );
  else
    {
    (void)memcpy( hdr->Reserved2, &msg[32], 4 );
    hdr->TreeId  = smbget32( &msg[36] );
    }

  /* All done; success! */
  return( SMB2_HEADER_SIZE );
  } /* smb2_parseHeader */


int smb2_packHeader( const uint16_t           dialect,
                     const smb2_Header *const hdr,
                     uint8_t           *const bufr )
  /** Format a standard SMB2/3 header.
   *
   * @param[in]     dialect The dialect revision number that has been
   *                        negotiated for the connection.
   * @param[in]     hdr     A (constant) pointer to a (constant)
   *                        \c smb2_Header structure containing the values
   *                        to be packed into the given \p bufr.
   * @param[in,out] bufr    A (constant) pointer to an array of bytes into
   *                        which the message header will be written.
   *                        - The \p bufr \b MUST be at least
   *                          \c #SMB2_SIGNATURE_OFFSET bytes in length.
   *                          No check is performed to validate the buffer
   *                          size.
   *
   * @returns   \c #SMB2_SIGNATURE_OFFSET \n
   *            This function always returns the offset of the message
   *            signature, relative to the start of the header.  This value
   *            also represents the number of bytes of \c bufr that have, so
   *            far, been filled with header data.
   *
   * \b Notes
   *  - This function <em>does not</em> fill in the message signature.
   *    That step should be completed separately.
   *  - <tt>(#SMB2_SIGNATURE_OFFSET + #SMB2_SIGNATURE_LENGTH) ==
   *         #SMB2_HEADER_SIZE</tt>
   *  - This function composes both sync and async headers, depending upon
   *    the value of the value of the \c #SMB2_FLAGS_ASYNC_COMMAND bit in
   *    the \c Flags field.
   *  - It is assumed that basic sanity checking has been performed before
   *    this function is called.  However, the \c assert(3) macro is used to
   *    check that the input parameters are not NULL, and that the command
   *    code is valid.  For production use, compile with \c NDEBUG set.
   */
  {
  uint8_t *p       = NULL;
  uint16_t creditC = hdr->CreditCharge;
  uint16_t chanSeq = hdr->ChannelSequence;
  uint32_t flags   = hdr->Flags;

  /* Sanity checks. */
  assert( NULL != hdr );
  assert( NULL != bufr );
  assert( hdr->Command < SMB2_COM_MAX );

  /* Force clean values for older dialects. */
  if( dialect < SMB2_VMAX )
    flags &= SMB2_FLAGS_MASK;
  if( dialect < SMB2_V0311 )
    {
    flags &= ~(uint32_t)SMB2_FLAGS_PRIORITY_MASK;
    if( dialect < SMB2_V0300 )
      {
      flags &= ~(uint32_t)SMB2_FLAGS_REPLAY_OPERATION;
      chanSeq = 0;
      }
    }
  if( dialect < SMB2_V0210 )
    creditC = 0;

  /* Pack the buffer. */
  p = smbcpmem( bufr, PROTOCOL_STR, 4 );      /* ProtocolId string        */
  p = smbset16( p, SMB2_HEADER_STRUCT_SIZE ); /* StructureSize            */
  p = smbset16( p, creditC );                 /* CreditCharge             */
  if( HDR2_IS_RESPONSE( flags ) )
    p = smbset32( p, hdr->Status );           /* Status                   */
  else
    {
    p = smbset16( p, chanSeq );               /* ChannelSequence          */
    p = smbcpmem( p, hdr->Reserved1, 2 );     /* Reserved1                */
    }
  p = smbset16( p, hdr->Command );            /* Command                  */
  p = smbset16( p, hdr->Credits );            /* Credit[Request|Response] */
  p = smbset32( p, flags );                   /* Flags                    */
  p = smbset32( p, hdr->NextCommand );        /* NextCommand              */
  p = smbset64( p, hdr->MessageId );          /* MessageId                */
  if( HDR2_IS_ASYNC( flags ) )
    p = smbset64( p, hdr->AsyncId );          /* AsyncId                  */
  else
    {
    p = smbcpmem( p, hdr->Reserved2, 4 );     /* Reserved2                */
    p = smbset32( p, hdr->TreeId );           /* TreeId                   */
    }
  p = smbset64( p, hdr->SessionId );          /* SessionId                */

  return( SMB2_SIGNATURE_OFFSET );  /* Success. */
  } /* smb2_packHeader */

/* =========================== That's all, folks! =========================== */
