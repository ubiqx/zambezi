#ifndef SMB2_ECHO_H
#define SMB2_ECHO_H
/* ========================================================================== **
 *                                smb2_echo.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Marshall/unmarshall SMB2/3 Echo exchanges.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_echo.h; 2020-09-22 22:14:05 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    smb2_echo.h
 * @author  Christopher R. Hertel
 * @brief   Parse/pack echo request and response messages.
 * @date    15 Sep 2020
 * @version \$Id: smb2_echo.h; 2020-09-22 22:14:05 -0500; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 */

#include "smb2_baseMsg.h"


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @typedef smb2_EchoReq
 * @brief   SMB2/3 Echo Request message structure.
 * @details This maps directly to a \c #smb2_BaseMsg.
 * @see     #smb2_BaseMsg
 */
typedef smb2_BaseMsg smb2_EchoReq;

/**
 * @typedef smb2_EchoResp
 * @brief   SMB2/3 Echo Response message structure.
 * @details This maps directly to a \c #smb2_BaseMsg.
 * @see     #smb2_BaseMsg
 */
typedef smb2_BaseMsg smb2_EchoResp;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int smb2_parseEchoReq( uint16_t      const dialect,
                       uint8_t      *const msg,
                       size_t        const msgLen,
                       smb2_EchoReq *const echoReq );

int smb2_packEchoReq( uint16_t      const dialect,
                      smb2_EchoReq *const echoReq,
                      uint8_t      *const bufr,
                      uint32_t      const bSize );

int smb2_parseEchoResp( uint16_t       const dialect,
                        uint8_t       *const msg,
                        size_t         const msgLen,
                        smb2_EchoResp *const echoResp );

int smb2_packEchoResp( uint16_t       const dialect,
                       smb2_EchoResp *const echoResp,
                       uint8_t       *const bufr,
                       uint32_t       const bSize );


/* ============================== smb2_echo.h =============================== */
#endif /* SMB2_ECHO_H */
