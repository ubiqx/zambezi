/* ========================================================================== **
 *                              smb1_negotiate.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Just enough SMB1 to negotiate SMB2.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb1_negotiate.c; 2024-11-25 21:03:56 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 */

#include <assert.h>           /* For debugging; see assert(3).  */
#include <string.h>           /* For memcmp(3), strcmp(3), etc. */

#include "smb_endian.h"       /* SMB byte order conversions.    */
#include "smb1_negotiate.h"   /* Module header.                 */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *
 *  V0202DIASTR - The SMB1 Dialect Identifier string used to negotiate the
 *                original SMB 2.0.2 dialect, as first released (in Vista).
 *  VWILDDIASTR - The SMB1 Dialect Identifier string used to throw up your
 *                hands and say "what the heck!?".  That is, to switch to
 *                SMB2 dialect negotiation.
 */

#define V0202DIASTR "\x022.002\0"   /* SMB1 dialect string for SMBv2.0.2.     */
#define VWILDDIASTR "\x022.???\0"   /* SMB1 dialect string for SMBv2 and up.  */


/* -------------------------------------------------------------------------- **
 * Static Constants
 *
 *  PROTOCOL_STR  - The first field of all SMB message headers is the
 *                  4-byte ProtocolId field.  In SMB1, this field contains
 *                  the string "\xffSMB".
 *
 *  defHdr        - Default SMB1 Request header.  Most of these values
 *                  will be correct for a simple SMB1 Negotiate Request.
 *                  + The actual PID should be used (see getpid(3)).
 *                  + A better MID value (maybe pseudo-random) should
 *                    be used.
 */

static char const *const PROTOCOL_STR = "\xffSMB";

static smb1_Header defHdr[1] =
  {{
  { 0xFF, 'S', 'M', 'B' },      /* SMB1 Protocol Id string.                   */
  SMB1_COM_NEGOTIATE,           /* SMB1 NegProt Command Code.                 */
  0,                            /* Status; unused in requests.                */
  0x18,                         /* Flags; CaseInsensitive & CannonicalPaths.  */
  0xC001,                       /* Flags2; Long Names, NTStatus, Unicode.     */
  NULL,                         /* Signature Buffer.                          */
  { '\0', '\0' },               /* Reserved bytes.                            */
  0xFFFF,                       /* The "No TID" value.                        */
  0,                            /* Initial PID value (ULONG).                 */
  0,                            /* Initial UID value (default pre-auth).      */
  0                             /* Initial MID value (USHORT).                */
  }};


/* -------------------------------------------------------------------------- **
 * Static Functions
 */

static int smb1_parseHeader( uint8_t     *const msg,
                             smb1_Header *const hdr )
  /* ------------------------------------------------------------------------ **
   * Quick-parse an SMB1 message header.
   *
   *  Input:  msg - Pointer to a memory buffer at least SMB1_HEADER_SIZE
   *                bytes in length.  This buffer must contain the received
   *                SMB1 header that is to be parsed.
   *          hdr - A pointer to an smb1_Header structure into which the
   *                parsed data will be copied.
   *
   *  Output: On success, the number of bytes read, which must always be
   *          equal to SMB1_HEADER_SIZE.  A negative value is returned on
   *          failure.
   *
   *  Errors:
   *    -1  - Incorrect Protocol Id.
   *          The first four bytes of the SMB1 header must be the SMB1
   *          ProtocolId: "\xffSMB".
   *    -2  - Not an SMB1 Negotiate message.
   *          The command code must be 0x72.
   *
   *  Notes:
   *    - The hdr->Signature field will be set to point to the location of
   *      the signature within <msg>.  The signature is calculated using
   *      data generated during authentication, which occurs after protocol
   *      negotiation.  The signature field should, therefore, contain
   *      SMB1_SIGNATURE_LENGTH nul bytes.
   *    - Some assumptions are made, and some values are left unchecked.
   *      + There is no check to ensure that <msg> is SMB1_HEADER_SIZE bytes
   *        in length.  That's the caller's job.
   *      + The Status field is assumed to be in 32-bit NTSTATUS format.
   *      + The PIDHigh and PIDLow fields listed in the documentation are
   *        combined into a single PID field in the <smb1_Header> structure.
   *      + The Signature field is a pointer into the received buffer, the
   *        contents of which are unverified (they should be nul bytes).
   *    - Some sanity checks are made using the assert(3) call.  These checks
   *      are intended to catch critical bugs in the testing phase and should
   *      be compiled out for production use (which is exactly what assert(3)
   *      is all about).
   *
   * ------------------------------------------------------------------------ **
   */
  {
  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != hdr );

  /* Validate the message. */
  if( 0 != memcmp( PROTOCOL_STR, msg, 4 ) )         /* Must be "\xffSMB". */
    return( -1 );
  /* Command Code. */
  if( SMB1_COM_NEGOTIATE != msg[4] )
    return( -2 );

  /* ProtocolID and command code. */
  (void)memcpy( hdr->ProtocolId, PROTOCOL_STR, 4 );
  hdr->Command = SMB1_COM_NEGOTIATE;

  /* Unpack some more... */
  hdr->Status     = smbget32( &msg[5] );
  hdr->Flags      = msg[9];
  hdr->Flags2     = smbget16( &msg[10] );
  hdr->PID        = ((uint32_t)smbget16( &msg[12] ) << 16);   /* PIDHigh. */
  hdr->Signature  = &msg[SMB1_SIGNATURE_OFFSET];
  (void)memcpy( hdr->Reserved, &msg[22], 2 );
  hdr->TID        = smbget16( &msg[24] );
  hdr->PID       |= smbget16( &msg[26] );                     /* PIDLow.  */
  hdr->UID        = smbget16( &msg[28] );
  hdr->MID        = smbget16( &msg[30] );
  return( SMB1_HEADER_SIZE );
  } /* smb1_parseHeader */

static int smb1_packHeader( const smb1_Header *const hdr,
                            uint8_t           *const bufr )
  /* ------------------------------------------------------------------------ **
   * Compose an SMB1 message header from the provided input.
   *
   *  Input:  hdr   - Pointer to an smb1_Header structure containing the
   *                  message header data to be packed.
   *          bufr  - Pointer to a buffer that is at least SMB1_HEADER_SIZE
   *                  in size.  The wire format of the SMB1 header will be
   *                  written into this buffer.
   *
   *  Output: The number of bytes of <bufr> that were consumed; this will
   *          always be SMB1_HEADER_SIZE.
   *
   *  Notes:
   *    - The signature bytes are zero-filled.
   *      + The comparable function SMB2 header packing function does not
   *        zero-fill the signature bytes.  Here's why:
   *        * The Signature field is in the middle of the SMB1 header,
   *          rather than at the end.
   *        * Packet signing cannot occur until after Negotiation and
   *          Authentication have been completed.  The signing keys are
   *          generated by the authentication process.  The SMB1 Signature
   *          field should, therefore, be zero-filled during protocol
   *          negotiation.
   *        * In general, signature bytes MUST be zero-filled in the SMB1
   *          header prior to calculating the message signature.  The actual
   *          signature then overwrites the zeroed bytes before transmission.
   *      + That's probably more information than you wanted just now.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  uint8_t *p = bufr;

  /* Sanity checks. */
  assert( NULL != hdr );
  assert( NULL != bufr );
  assert( 0 == memcmp( PROTOCOL_STR, hdr->ProtocolId, 4 ) );
  assert( SMB1_COM_NEGOTIATE == hdr->Command );

  /* Build the message. */
  p = smbcpmem( p, hdr->ProtocolId, 4 );
  p = smbset8(  p, hdr->Command );
  p = smbset32( p, hdr->Status );
  p = smbset8(  p, hdr->Flags );
  p = smbset16( p, hdr->Flags2 );
  p = smbset16( p, (uint16_t)(hdr->PID >> 16) & 0xFFFF );   /* PIDHigh.   */
  p = smbmset(  p, 0x00, SMB1_SIGNATURE_LENGTH );           /* Signature. */
  p = smbcpmem( p, hdr->Reserved, 2 );
  p = smbset16( p, hdr->TID );
  p = smbset16( p, (uint16_t)(hdr->PID & 0xFFFF) );         /* PIDLow.    */
  p = smbset16( p, hdr->UID );
  p = smbset16( p, hdr->MID );

  return( SMB1_HEADER_SIZE );
  } /* smb1_packHeader */

static int smb1_parseNegotiateReq( uint8_t         *const msg,
                                   size_t           const msgLen,
                                   smb1_NegProtReq *const negReq )
  /* ------------------------------------------------------------------------ **
   * Parse the body of an SMB1 Negotiate Request.
   *
   *  Input:  msg     - Pointer to the message buffer containing the message
   *                    payload (that is, following the header).
   *          msgLen  - Number of bytes in <msg>.
   *          negReq  - Pointer to an \c smb1_NegProtReq structure that will
   *                    receive the parsed data.
   *
   *  Output: On failure, a negative value is returned.
   *          On success, the result indicates the number of bytes of <msg>
   *          that were consumed, which will always be greater than or equal
   *          to three (3).
   *
   *  Errors:
   *  -1  - The WordCount field in the NegProt request must have a zero
   *        value, but a non-zero value was found in the <msg> buffer.
   *  -3  - Based on <msgLen>, the <msg> buffer is not large enough to
   *        contain the WordCount and ByteCount fields.
   *  -n  - Based on <msgLen> and the <ByteCount> field, the <msg> buffer
   *        is not large enough to contain the entire Dialects list.  At
   *        least <n> bytes are required.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  int pLen;

  /* Sanity checks. */
  assert( NULL != msg );
  assert( NULL != negReq );

  /* Basic checks. */
  if( msgLen < 3 )
    return( -3 );
  if( 0 != msg[0] )
    return( -1 );

  /* Grab ByteCount, and see if we've got enough bytes. */
  negReq->ByteCount = smbget16( &msg[1] );
  pLen = (int)(3 + negReq->ByteCount);
  if( msgLen < pLen )
    return( -pLen );
  negReq->Dialects = &msg[3];
  return( pLen );
  } /* smb1_parseNegotiateReq */


/* -------------------------------------------------------------------------- **
 * Functions
 */

int smb1_mkNegProtReq( smb1_NegProtReqParams *const negReqParams,
                       uint8_t               *const bufr,
                       uint32_t               const bSize )
  /** Create a complete SMB1 NegProt Request message from defaults and parts.
   *
   * @param[in]   negReqParams  A pointer to an \c #smb1_NegProtReqParams
   *                            structure that contains the minimum set of
   *                            data required to build a simple SMB1
   *                            Negotiate Request message for negotiating
   *                            SMB2.
   * @param[out]  bufr          A data buffer into which the composed
   *                            message will be written.
   * @param[in]   bSize         The number of bytes available in \p bufr.
   *
   * @returns   On error, a negative value is returned.  On success, the
   *            result is the number of bytes used to compose the outgoing
   *            message.  This should be either 42 or 49, depending upon
   *            whether one or two dialect strings have been selected.
   *
   * @details
   *  Input values are combined with default values to compose a baseline
   *  SMB1 Negotiate Request message that offers SMBv2.0.2 and/or SMB2+.
   *
   * \b Errors
   *  - -1  \b ERROR: At least one SMB2 dialect identifier must be selected
   *                  in \p negReqParams.
   *  - -n  \b ERROR: Short buffer; based on \p bSize, \p bufr is not large
   *                  for the entire composed message.  At least \<n> bytes
   *                  are required.
   *
   * \b Notes
   *  - There are two SMB1 dialect strings that may be sent to negotiate
   *    SMB2/SMB3.  The first of these offers SMBv2.0.2 only, while the
   *    second is a wildcard that indicates that SMB2 Negotiation should
   *    be attempted.
   *  - Both of the SMB2 dialect strings, including the prefix and
   *    terminating \c nul byte, are 7 bytes in length.
   */
  {
  uint8_t *p         = bufr;
  int      byteCount = 7;     /* Fits 1 dialect string. */
  int      msgLen;

  /* Sanity checks. */
  assert( NULL != bufr );
  assert( NULL != negReqParams );

  /* Validity checks. */
  if( !(negReqParams->v0202 || negReqParams->vWild ) )
    return( -1 );             /* Must have at least one selected. */
  if( negReqParams->v0202 && negReqParams->vWild )
    byteCount += 7;           /* Fits two SMB2 dialect strings.   */
  msgLen = SMB1_HEADER_SIZE + 3 + byteCount;
  if( bSize < msgLen )
    return( -msgLen );

  /* Compose the header. */
  (void)smb1_packHeader( defHdr, bufr );
  (void)smbset16( &bufr[12], (uint16_t)(negReqParams->PID >> 16) & 0xFFFF );
  (void)smbset16( &bufr[26], (uint16_t)(negReqParams->PID & 0xFFFF) );
  (void)smbset16( &bufr[30], negReqParams->MID );

  /* Compose the payload. */
  p = smbset8( &bufr[32], 0 );              /* WordCount (zero).    */
  p = smbset16( p, (uint16_t)byteCount );   /* ByteCount (7 or 14). */
  if( negReqParams->v0202 )
    p = smbcpmem( p, V0202DIASTR, 7 );
  if( negReqParams->vWild )
    p = smbcpmem( p, VWILDDIASTR, 7 );

  /* Return the total message length. */
  return( msgLen );
  } /* smb1_mkNegProtReq */

int smb1_deriveDialects( uint8_t               *const msg,
                         size_t                 const msgLen,
                         smb1_NegProtReqParams *const negReqParams )
  /** Parse a NegProt Request and derive the offered SMB2 dialects.
   *
   * @param[in] msg           Pointer to a buffer containing the SMB1
   *                          Negotiate Request message, including the
   *                          header.  This function will parse the entire
   *                          message.
   * @param[in] msgLen        The length, in bytes, of \p msg.  The message
   *                          must be at least 35 bytes in length in order to
   *                          be a proper SMB1 NegProt Request.
   * @param[out] negReqParams A pointer to an \c #smb1_NegProtReqParams
   *                          structure which will be populated with data
   *                          extracted from \p msg.
   *
   * @returns   On error, a negative value is returned.
   *            On success, the number of bytes of \p msg that were read.
   *
   * \b Errors
   *  - -1  \b ERROR: Incorrect Protocol Id.
   *                  The first four bytes of the SMB1 header must be the
   *                  SMB1 ProtocolId: "\xffSMB".
   *  - -2  \b ERROR: Not an SMB1 Negotiate message.
   *                  The command code must be (but isn't) 0x72.
   *  - -3  \b ERROR: The \c WordCount field in the NegProt request \b must
   *                  be zero, but a non-zero \c WordCount was found in
   *                  \p msg.
   *  - -4  \b ERROR: Unterminated dialect list.  SMB1 dialect strings are
   *                  nul-terminated.  The last byte of the dialects array,
   *                  therefore, must be a \c nul byte.  If this is not the
   *                  case, then the message is malformed and should be laid
   *                  to rest.
   *  - -n  \b ERROR: Short buffer; based on \p msgLen, the \p msg buffer is
   *                  two small to contain a complete SMB1 NegProt message.
   *                  The value of \c n depends upon how much of the message
   *                  could be parsed, but will be a minimum of
   *                  \c #SMB1_HEADER_SIZE (32).
   */
  {
  int             pos;
  int             rslt;
  smb1_Header     hdr[1];
  smb1_NegProtReq npReq[1];

  /* Sanity Checks. */
  assert( NULL != msg );
  assert( NULL != negReqParams );

  /* Check <msg> size, then parse the header.
   *  Initialize the Params structure using returned or default values.
   */
  if( msgLen < SMB1_HEADER_SIZE )
    return( -SMB1_HEADER_SIZE );
  rslt = smb1_parseHeader( msg, hdr );
  if( rslt < 0 )
    return( rslt );
  negReqParams->PID   = hdr->PID;
  negReqParams->MID   = hdr->MID;
  negReqParams->v0202 = false;
  negReqParams->vWild = false;

  /* Parse and validate the payload. */
  rslt = smb1_parseNegotiateReq( &msg[SMB1_HEADER_SIZE],
                                 (msgLen - SMB1_HEADER_SIZE),
                                 npReq );
  if( rslt < 0 )
    return( (-1 == rslt) ? -3 : (rslt - SMB1_HEADER_SIZE) );

  /* If there is a Dialects list, search through it for SMB2 dialect strings. */
  if( npReq->ByteCount > 0 )
    {
    if( '\0' != npReq->Dialects[npReq->ByteCount - 1] )
      return( -4 );

    for( pos = 0; pos < npReq->ByteCount; /* -- */ )
      {
      char *p;

      p = (char *)&(npReq->Dialects[pos]);
      if( 0 == strcmp( V0202DIASTR, p ) )
        {
        negReqParams->v0202 = true;
        if( negReqParams->vWild )
          break;
        }
      else
        {
        if( 0 == strcmp( VWILDDIASTR, p ) )
          {
          negReqParams->vWild = true;
          if( negReqParams->v0202 )
            break;
          }
        }
      pos += 1 + (int)strlen( p );
      }
    }

  return( rslt + SMB1_HEADER_SIZE );
  } /* smb1_deriveDialects */


#ifdef SMB1_NEGOTIATE_UNIT_TEST
/* -------------------------------------------------------------------------- **
 * Unit Tests
 * To compile:
 *  $ cc -Wall -D SMB1_NEGOTIATE_UNIT_TEST smb1_negotiate.c -o utest
 * To run:
 *  $ ./utest
 * -------------------------------------------------------------------------- **
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main( void )
  {
  int     rslt;
  uint8_t bufr[1024];
  smb1_NegProtReqParams npReqP[2];

  /* Pack npReqP */
  srand( (unsigned int)(npReqP->PID = (uint32_t)getpid()) );
  npReqP->MID = (uint16_t)(rand() & 0xFFFF);
  npReqP->v0202 = false;
  npReqP->vWild = false;

  /* Test basic error checking. */
  rslt = smb1_mkNegProtReq( npReqP, bufr, 1024 );
  assert( rslt == -1 );   /* Should be -1; at least one SMB2 string selected. */
  npReqP->v0202 = true;
  rslt = smb1_mkNegProtReq( npReqP, bufr, 10 );
  assert( rslt == -42 );  /* Should return -42; buffer too small. */

  /* Pack and unpack. */
  rslt = smb1_mkNegProtReq( npReqP, bufr, 1024 );
  assert( rslt == 42 );   /* Should be 42; Header=32, payload=10. */
  rslt = smb1_deriveDialects( bufr, rslt, &npReqP[1] );
  assert( rslt == 42 );   /* Should also be 42. */
  /* Did we get the same thing back? */
  assert( npReqP[0].PID   == npReqP[1].PID );
  assert( npReqP[0].MID   == npReqP[1].MID );
  assert( npReqP[0].v0202 == npReqP[1].v0202 );
  assert( npReqP[0].vWild == npReqP[1].vWild );

  /* Do it again, with a different dialect string. */
  npReqP->v0202 = false;
  npReqP->vWild = true;
  rslt = smb1_mkNegProtReq( npReqP, bufr, 1024 );
  assert( rslt == 42 );   /* Should be 42; Header=32, payload=10. */
  rslt = smb1_deriveDialects( bufr, rslt, &npReqP[1] );
  assert( rslt == 42 );   /* Should also be 42. */
  /* Did we get the same thing back? */
  assert( npReqP[0].PID   == npReqP[1].PID );
  assert( npReqP[0].MID   == npReqP[1].MID );
  assert( npReqP[0].v0202 == npReqP[1].v0202 );
  assert( npReqP[0].vWild == npReqP[1].vWild );

  /* Once again, with both dialect strings. */
  npReqP->v0202 = true;
  rslt = smb1_mkNegProtReq( npReqP, bufr, 1024 );
  assert( rslt == 49 );   /* Should be 49; Header=32, payload=17. */
  rslt = smb1_deriveDialects( bufr, rslt, &npReqP[1] );
  assert( rslt == 49 );   /* Should also be 49. */
  /* Did we get the same thing back? */
  assert( npReqP[0].PID   == npReqP[1].PID );
  assert( npReqP[0].MID   == npReqP[1].MID );
  assert( npReqP[0].v0202 == npReqP[1].v0202 );
  assert( npReqP[0].vWild == npReqP[1].vWild );

  (void)fprintf( stderr, "Passed all unit tests.\n" );
  return( EXIT_SUCCESS );
  } /* main */

#endif /* SMB1_NEGOTIATE_UNIT_TEST */

/* ================================ Endirinn ================================ */
