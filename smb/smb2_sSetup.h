#ifndef SMB2_SSETUP_H
#define SMB2_SSETUP_H
/* ========================================================================== **
 *                               smb2_sSetup.h
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Marshall/unmarshall SMB2/3 Session Setup messages.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: smb2_sSetup.h; 2024-03-11 18:19:05 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file    smb2_sSetup.h
 * @author  Christopher R. Hertel
 * @brief   SMB2/3 Session Setup marshalling/unmarshalling.
 * @date    14 Sep 2020
 * @version \$Id: smb2_sSetup.h; 2024-03-11 18:19:05 -0500; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 *
 * @details
 *  The Session Setup exchange establishes an authenticated SMB2/3 session
 *  between the client and the server.  It also finalizes the negotiation
 *  of features and capabilities.
 *
 *  Multiple sessions may exist over a single transport connection (e.g., a
 *  single TCP connection).  As of SMBv3, the client may also bind a single
 *  session to multiple connections (Multichannel).  This provides enhanced
 *  failover and improved performance.  If one connection goes down, the
 *  session will continue over the remaining connections.  Multichannel can
 *  also make use of the additional bandwidth provided by multiple physical
 *  connection paths.
 */

#include "smb2_header.h"    /* SMB2/3 Message Header module.                  */
#include "smb2_negotiate.h" /* Provides Security Mode and Capabilities bits.  */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *//**
 * @def     SMB2_SSETUP_REQ_SIZE
 * @brief   Size, in bytes, of the fixed-length portion of the SMB2 Session
 *          Setup Request message.
 *
 * @def     SMB2_SSETUP_REQ_STRUCT_SIZE
 * @brief   Structure size, in bytes, of the SMB2 Session Setup Request
 *          payload.
 * @details
 *  The actual size of the message will vary depending upon the number of
 *  bytes in the \c SecurityBuffer field.  The \c StructureSize is one
 *  byte more than the size of the fixed-length payload.  The one byte is
 *  a placeholder for the variable-length portion.
 *
 * @def     SMB2_SSETUP_RESP_SIZE
 * @brief   Size, in bytes, of the fixed-length portion of the SMB2 Session
 *          Setup Response message.
 *
 * @def     SMB2_SSETUP_RESP_STRUCT_SIZE
 * @brief   Structure size, in bytes, of the SMB2 Session Setup Response
 *          payload.
 * @details
 *  The actual message size depends upon the size of the \c SecurityBuffer
 *  field.
 */

#define SMB2_SSETUP_REQ_SIZE         24
#define SMB2_SSETUP_REQ_STRUCT_SIZE  25

#define SMB2_SSETUP_RESP_SIZE         8
#define SMB2_SSETUP_RESP_STRUCT_SIZE  9


/* -------------------------------------------------------------------------- **
 * Enumerated Constants
 */

/**
 * @enum  smb2_ssFlags
 *        SMB3 Session Request flags.
 *
 * \b Notes
 *  - This field is used only in SMB3 and above.  In SMB2 dialects, the
 *    \c Flags field is reserved and must be zero.
 *
 * @var SMB2_SESSION_FLAG_BINDING
 *  When set, this flag indicates that the purpose of the request is to bind
 *  an existing session to a new connection, rather than to establish a new
 *  session.
 */
typedef enum
  {
  SMB2_SESSION_FLAG_BINDING = 0x01
  } smb2_ssFlags;

/**
 * @enum  smb2_ssSessionFlags
 *        SMB2/3 Session Response Flags.
 *
 * These flags provide information regarding the results of authentication.
 *
 * \b Notes
 *  - Guest and anonymous logins are mutually exclusive.
 *  - The ENCRYPT_DATA flag is only valid for SMB3 and above.
 *
 * @var SMB2_SESSION_FLAG_IS_GUEST
 *  When set, indicates \b guest user authentication has succeeded.
 *
 * @var SMB2_SESSION_FLAG_IS_NULL
 *  When set, indicates \b anonymous user authentication has succeeded.
 *
 * @var SMB2_SESSION_FLAG_ENCRYPT_DATA
 *  When set, the server requires encryption of messages on this session.
 * @see <a href="@msdocs/ms-smb2/b495e2da-8711-4772-b292-453be0394b49">[MS-SMB2;
 * 3.3.5.2.9]: Verifying the Session</a>
 */
typedef enum
  {
  SMB2_SESSION_FLAG_IS_GUEST      = 0x0001,
  SMB2_SESSION_FLAG_IS_NULL       = 0x0002,
  SMB2_SESSION_FLAG_ENCRYPT_DATA  = 0x0004
  } smb2_ssSessionFlags;


/* -------------------------------------------------------------------------- **
 * Typedefs
 */

/**
 * @struct  smb2_sSetupReq
 * @brief   SMB2/3 Session Setup Request structure.
 * @details
 *  This structure represents the Session Setup Request message, which is
 *  used to initiate a new authenticated session over a transport connection.
 *  In SMB3, the Session Setup Request may also be used to indicate that an
 *  existing session should be carried over an additional transport
 *  connection (see \c #smb2_ssFlags).
 *
 * \b Pointers
 *  - \b \c #SecurityBuffer - Pointer to the security buffer within the
 *                            received message.
 *
 * @var smb2_sSetupReq::Flags
 *      This \c Flags field provides support for features added in SMB3 and
 *      above.
 * @var smb2_sSetupReq::SecurityMode
 *      @see \c smb2_SecurityMode
 * @var smb2_sSetupReq::Capabilities
 *      Only the \c #SMB2_GLOBAL_CAP_DFS bit is valid in the Session Setup
 *      request.
 */
typedef struct
  {
  uint16_t  StructureSize;          /**< Fixed at 25 bytes.                   */
  uint8_t   Flags;                  /**< Request flags.                       */
  uint8_t   SecurityMode;           /**< Session security requirements.       */
  uint32_t  Capabilities;           /**< Client capabilities.                 */
  uint8_t   Reserved[4];            /**< The \c Channel field; Reserved-MB0.  */
  uint16_t  SecurityBufferOffset;   /**< Security buffer offset.              */
  uint16_t  SecurityBufferLength;   /**< Security buffer size, in bytes.      */
  uint64_t  PreviousSessionId;      /**< Session restoration token.           */
  uint8_t  *SecurityBuffer;         /**< Pointer to the security buffer.      */
  } smb2_sSetupReq;

/**
 * @struct  smb2_sSetupResp
 * @brief   SMB2/3 Session Setup Response structure.
 * @details
 *  These are the fields of the Session Setup Response message.
 *
 *  In some cases, the server may send a STATUS_MORE_PROCESSING_REQUIRED,
 *  error code in the response, in which case additional Session Setup
 *  request/response exchanges will need to be completed.
 */
typedef struct
  {
  uint16_t  StructureSize;          /**< Fixed at 9 bytes.                    */
  uint16_t  SessionFlags;           /**< Session context flags.               */
  uint16_t  SecurityBufferOffset;   /**< Security buffer location.            */
  uint16_t  SecurityBufferLength;   /**< Security buffer size, in bytes.      */
  uint8_t  *SecurityBuffer;         /**< Pointer to the security buffer.      */
  } smb2_sSetupResp;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int smb2_parseSSetupReq( uint16_t        const dialect,
                         uint8_t        *const msg,
                         size_t          const msgLen,
                         smb2_sSetupReq *const ssReq );

int smb2_packSSetupReq( uint16_t        const dialect,
                        smb2_sSetupReq *const ssReq,
                        uint8_t        *const bufr,
                        uint32_t        const bSize );

int smb2_parseSSetupResp( uint16_t         const dialect,
                          uint8_t         *const msg,
                          size_t           const msgLen,
                          smb2_sSetupResp *const ssResp );

int smb2_packSSetupResp( uint16_t         const dialect,
                         smb2_sSetupResp *const ssResp,
                         uint8_t         *const bufr,
                         uint32_t         const bSize );


/* ============================= smb2_sSetup.h ============================== */
#endif /* SMB2_SSETUP_H */
