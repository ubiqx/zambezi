/* ========================================================================== **
 *                                  HexOct.c
 *
 * Copyright:
 *  Copyright (C) 2002, 2010, 2017, 2020 by Christopher R. Hertel
 *  Copyright (C) 2010 by José Rivera
 *
 * $Id: HexOct.c; 2024-11-25 21:03:56 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 *
 * Description:
 *  Tools to convert to/from Hexadecimal and Octal notations, and to produce
 *  hexdumps.
 *
 * -------------------------------------------------------------------------- **
 *
 * License:
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 *
 * Notes:
 *
 *  - To compile the mainline and produce a test program:
 *    cc -DHEXDUMPMAIN -o HexDump HexOct.c
 *
 * ========================================================================== **
 */

#include <ctype.h>      /* For isprint(3).    */
#include <string.h>     /* For memset(3).     */

#include "HexOct.h"     /* Module header.     */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *
 *  LINEWIDTH - The buffer size used for a HexDump line buffer.  This is
 *              used internally when composing a single line of output.
 */

#define LINEWIDTH 80


/* -------------------------------------------------------------------------- **
 * Macros
 *
 *  hiNibble  - Return the value of a byte's high nibble.
 *  loNibble  - Return the value of a byte's low nibble.
 */

#define hiNibble( I ) ((uint8_t)(((I) & 0xF0) >> 4))
#define loNibble( I ) ((uint8_t)((I) & 0x0F))


/* -------------------------------------------------------------------------- **
 * Static Constants
 */

static const uint8_t hxo_HexDigits[] = "0123456789abcdef";


/* -------------------------------------------------------------------------- **
 * Static Functions
 */

size_t static HxDmp( size_t         lcount,
                     FILE    *const Outf,
                     const uint8_t *src,
                     const size_t   len )
  /* ------------------------------------------------------------------------ **
   * Print out a hexadecimal dump of the contents of the input buffer.
   *
   *  Input:  lcount  - The line number at which to start counting.  This
   *                    is the count of lines already printed in this set.
   *          Outf    - A pointer to the output file to which the dump
   *                    should be written.
   *          src     - A pointer to the array of bytes to be dumped.
   *          len     - The size of the array of bytes.
   *
   *  Output: The number of lines that have been written to <Outf>.
   *          This number includes the initial value of <lcount>.
   *
   *  Notes:  The output is modeled on WireShark's hex dump output
   *          and that of the BSD hexdump utility with the -C option.
   *
   *          Each line represents 16 bytes worth of input.  Each
   *          line starts with a number in hex indicating the byte
   *          offset of the input at which the line starts.
   *
   *          Next, there are 16 columns of two-digit hex numbers followed
   *          by 16 columns of single characters.  If the input byte is a
   *          printable character it is printed, otherwise it is represented
   *          by a dot.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  size_t  bcount = 0;
  uint8_t bufr[LINEWIDTH];

  /* Iterate through the buffer, 16 bytes at a time. */
  while( bcount < len )
    {
    bcount += (size_t)hxo_HexDumpLn( bufr, &src[bcount], len - bcount );
    (void)fprintf( Outf, "%.6X: %s\n", (uint32_t)(lcount * 16), bufr );
    lcount++;
    }
  /* ...and return the updated line count.  */
  return( lcount );
  } /* HxDmp */


/* -------------------------------------------------------------------------- **
 * Functions
 */

int hxo_XlateOdigit( const uint8_t digit )
  /** Convert an octal digit character to its respective three-bit value.
   *
   * @param[in] digit   A character, which must be a valid octal digit.
   *                    That is, one of "01234567".
   *
   * @returns   An integer value in the range 0..7, or a negative number.
   *
   * \b Errors
   *  - -n  \b ERROR: The input character was not a valid octal digit.
   */
  {
  int i;

  i = (int)digit - '0';
  if( i > 7 )
    return( -i );
  return( i );
  } /* hxo_XlateOdigit */

int hxo_XlateXdigit( const uint8_t digit )
  /** Converts a hex character to its respective nibble (four-bit) value.
   *
   * @param[in] digit   A character, which must be a valid hex digit.
   *                    That is, one of "0123456789ABCDEFabcdef".
   *
   * @returns   On error, a negative value is returned.  On success, the
   *            return value will be an integer in the range 0..15.
   *            Eg. 'A' ==> 10.
   *
   * \b Errors
   *  - -n  \b ERROR: The input character was not a valid hexadecimal digit.
   */
  {
  int i;

  i = (int)digit - '0';
  if( i < 0xA )   /* Handle the 0..9 (and lower) range.               */
    return( i );  /* If digit was less than '0', this'll be negative. */

  /* This looks bulky but it should compile down to very simple code. */
  switch( digit )
    {
    case 'A':
    case 'a': i = 0xA; break;
    case 'B':
    case 'b': i = 0xB; break;
    case 'C':
    case 'c': i = 0xC; break;
    case 'D':
    case 'd': i = 0xD; break;
    case 'E':
    case 'e': i = 0xE; break;
    case 'F':
    case 'f': i = 0xF; break;
    default:
      return( -i );   /* We took care of negative values of <i> earlier. */
    }
  return( i );
  } /* hxo_XlateXdigit */

int hxo_UnEscSeq( const uint8_t *src, int *esclen )
  /** Translate a C-style escape sequence into the value it represents.
   *
   * @param[in]   src     Source string to read.
   * @param[out]  esclen  A pointer to an int that will receive the length
   *                      of the escape sequence.  If NULL, it will be
   *                      faithfully ignored.
   *
   * @returns   The integer value indicated by the escape sequence.
   *            If no escape sequence was found, the value of src[0] is
   *            returned and \p esclen will have a value of 1.
   *
   * \b Notes
   *  - Escape sequences vary in length.  A valid escape sequence has a
   *    minimum length of 2 (eg. "\n") and a maximum length of four (eg.
   *    "\176").  That's why we need to return a value in \p esclen.
   */
  {
  int c;  /* A character value. */
  int i;  /* A temporary int.   */
  int myesclen = 0;

  if( !esclen )
    esclen = &myesclen;

  /* If first char is not '\\' then just return first character. */
  if( '\\' != src[0] )
    {
    *esclen = 1;
    return( src[0] );
    }

  /* First char was, in fact, '\\'. */
  *esclen = 2;
  switch( src[1] )
    {
    case 'a': return( '\a' );
    case 'b': return( '\b' );
    case 'f': return( '\f' );
    case 'n': return( '\n' );
    case 'r': return( '\r' );
    case 't': return( '\t' );
    case 'v': return( '\v' );
    case 'x':
    case 'X':
      {
      i = hxo_XlateXdigit( src[2] );
      if( i < 0 )
        return( src[1] );  /* x or X */
      c = i;
      i = hxo_XlateXdigit( src[3] );
      if( i < 0 )
        {
        /* src[3] is not a hex digit, so we have a single-char escape
         * sequence; \xH, where H is the Hex Digit from src[1].
         */
        *esclen = 3;
        return( c );
        }
      /* Two-char hex escape sequence; \xHH. */
      c <<= 4;
      c |= i;
      *esclen = 4;
      return( c );
      }
    case '0':   /* Octal */
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
      {
      c = hxo_XlateOdigit( src[1] );
      i = hxo_XlateOdigit( src[2] );
      if( i < 0 )
        {
        /* Single-char octal number; \O, where O is the Octal Digit. */
        return( c );
        }

      /* At least two octal digits; \OO. */
      c <<= 3;
      c |= i;
      i = hxo_XlateOdigit( src[3] );
      if( i < 0 )
        {
        *esclen = 3;
        return( c );
        }
      /* Three octal digits; \OOO. */
      c <<= 3;
      c |= i;
      *esclen = 4;
      return( c );
      }
    }

  /* We had "\C", where C is not a recognized escape, so we return C. */
  return( src[1] );
  } /* hxo_UnEscSeq */

int hxo_XlateInput( const uint8_t *src )
  /** Convert from various hex string formats into an integer value.
   *
   * @param[in] src Pointer to a character array containing the text to be
   *                converted.  It is assumed that the input will be in one
   *                of the following formats:
   *  <pre>
   *                "A"     = A single non-hex-digit character, which is
   *                          returned as-is.
   *                "X"     = A single hex digit, returned as if it were
   *                          "0X".
   *                "XX"    = Two characters such that isxdigit(src[n])
   *                          is true for n=0 and n=1.
   *                "%XX"   = As above, with a leading % sign.
   *                "#XX"   = As above, with a leading # sign.
   *                "\xXX"  = As above, with a leading "\x".
   *                "0xXX"  = As above, with a leading "0x".
   *                "<XX>"  = Two hex digits within angle brackets.</pre>
   * @returns The one-byte translation of the input, or a negative value if
   *          there was an error interpreting the input.
   *
   * \b Errors
   *  - -1  \b ERROR: The input string was NULL or the empty string ("").
   *  - -2  \b ERROR: Interpretation of the input string failed because the
   *                  string did not match one of the above formats.
   *  - -3  \b ERROR: Interpretation of the input string failed because a
   *                  character that was expected to be a hex digit was not
   *                  a hex digit.
   *
   * \b Notes
   *  - The goal is to provide a whole mess of easy ways for the user to
   *    specify a byte value, in hex, on the command line.  The code is
   *    quite forgiving--perhaps too forgiving.
   *  - If the string length of the input string, \p src, is 1, and if that
   *    one character is not a hex digit, then the value of that character
   *    is returned.  This enables code that allows the user to enter a
   *    limited set of literal values (eg. \c ' ').
   */
  {
  int    lonibble = 0;
  int    hinibble = 0;
  int    start;
  size_t len;

  /* Check for invalid input. */
  if( (NULL == src) || (len = strlen( (char *)src )) < 1 )
    return( -1 );

  /* If the string is only one byte long, and it isn't a hex digit,
   * take that byte literally.  Of course, if it *is* a hex digit,
   * return its translated value.
   */
  if( 1 == len )
    {
    hinibble = hxo_XlateXdigit( *src );
    return( (hinibble < 0) ? *src : hinibble );
    }

  /* Look for our lead-in strings ('%','#','<','\\',"0x").
   * If we get no lead-in then assume that the input is in the form "XX".
   * The result of this switch() statement is that <start> will be set
   * to the starting position of the hex string to be translated.
   */
  switch( *src )
    {
    case '%':
    case '#':
    case '<':
      /* In these three cases, ignore the first byte and attempt to read
       * the next two characters as hex digits.
       */
      start = 1;
      break;
    case '\\':
    case '0':
      /* If the first char is '\\', then the second byte should be 'x' (or 'X').
       * If the first char is '0', then the second byte *might* be 'x' (or 'X').
       */
      if( ('x' == src[1]) || ('X' == src[1]) )
        {
        if( len < 3 )
          return( -2 );
        start = 2;
        }
      else
        {
        if( '\\' == *src )
          return( -2 );
        start = 0;
        }
      break;
    default:
      start = 0;
      break;
    }

  /* Okay, now translate one or two hex digits into a single byte value. */
  hinibble = hxo_XlateXdigit( src[start] );
  if( hinibble < 0 )
    return( -3 );         /* Character not a hex digit; return an error code. */

  lonibble = hxo_XlateXdigit( src[start+1] );
  if( lonibble < 0 )
    return( hinibble );   /* Char not a hex digit; return the high nibble.    */

  return( (hinibble << 4) | lonibble );   /* All good. */
  } /* hxo_XlateInput */

int hxo_UnEscStr( uint8_t *str )
  /** Convert escape sequences in a string to byte values.
   *
   * @param[in,out] str   String to translate.
   *
   * @returns The string length of the resultant string (excluding the
   *          terminating NUL).
   *
   * \b Notes
   *  - The new string will be of equal or shorter length.
   *  - This function operates *on* the input string.  If the goal is to
   *    create a new string, duplicate the original string first, then pass
   *    the duplicate into this function.
   *  - The new string may contain NULs within the span indicated by the
   *    returned length.  Eg.: If the original string was `"\0Fooberry"`
   *    the resulting string would have a string length of 9, and NULs in
   *    the 0th and 9th positions.
   */
  {
  int    i, j;
  int    esclen;
  size_t len;

  i = j = 0;
  len = strlen( (char *)str );
  while( i < len )
    {
    if( str[i] == '\\' )
      {
      str[j++] = (uint8_t)hxo_UnEscSeq( &str[i], &esclen );
      i += esclen;
      }
    else
      str[j++] = str[i++];
    }

  str[j] = '\0';
  return( j-1 );
  } /* hxo_UnEscStr */

int hxo_Hexify( uint8_t *dst, const uint8_t *src, const size_t len )
  /** Nasty function to write a hex-escaped string from a source string.
   *
   * @param[out]  dst   The target string.  To accommodate the worst case,
   *                    this buffer should be a minimum of
   *                    <tt>1 + (4 x len)</tt> bytes long.
   * @param[in]   src   Source string.
   * @param[in]   len   Number of bytes of the source string to be converted.
   *
   * @returns   The string length of the resulting string (excludes the
   *            terminating NUL).
   *
   * \b Errors
   *  - -1  \b ERROR: Either \p dst or \p src were NULL.
   *
   * \b Notes
   *  - This function is specific to ascii strings.  It converts all bytes
   *    except those in the range 0x20..0x7E (inclusive) to an escape
   *    sequence.
   */
  {
  int i, j;

  if( NULL == dst || NULL == src )
    return( -1 );

  for( i = j = 0; i < len; i++ )
    {
    if( (src[i] < 0x20) || (0x7E < src[i]) )
      {
      dst[j++] = '\\';
      if( '\0' == src[i] )
        dst[j++] = '0';
      else
        {
        dst[j++] = 'x';
        dst[j++] = hxo_HexDigits[(src[i] & 0xF0) >> 4];
        dst[j++] = hxo_HexDigits[src[i] & 0x0F];
        }
      }
    else
      {
      dst[j++] = src[i];
      if( '\\' == src[i] )  /* Must escape the escape. */
        dst[j++] = '\\';
      }
    }
  dst[j] = '\0';

  return( j );
  } /* hxo_Hexify */

int hxo_HexDumpLn( uint8_t *dst, const uint8_t *src, const size_t len )
  /** Generate one line of hexdump output from the given input.
   *
   * @param[out]  dst   Destination string (minimum 70 bytes).
   * @param[in]   src   Array of bytes to be dumped.
   * @param[in]   len   Length of the array (maximum number of bytes to dump).
   *
   * @returns   The number of bytes dumped (which is min( \p len, 16 )).
   *
   * \b Notes
   *  - This function builds a string that is 69 bytes long, plus one byte
   *    for the NUL terminator.  There is no newline appended.
   *  - The resultant string is typical of hex dump output (e.g., from
   *    \c hexdump(1)).  The input octets are given in hex notation followed
   *    by dot/character notation (dots for non-printing characters).
   */
  {
  int i, j;
  int maxbytes = (len > 16) ? 16 : (int)len;

  /* Write the hex listing. */
  for( i = j = 0; i < 16; i++ )
    {
    if( i < maxbytes )
      {
      dst[j++] = hxo_HexDigits[hiNibble( src[i] )];
      dst[j++] = hxo_HexDigits[loNibble( src[i] )];
      }
    else
      {
      dst[j++] = ' ';
      dst[j++] = ' ';
      }
    dst[j++] = ' ';
    if( 7 == (i % 8) )
      dst[j++] = ' ';
    }

  /* Produce the dot map. */
  dst[j++] = '|';
  for( i = 0; i < 16; i++ )
    {
    dst[j++] = (i < maxbytes)
             ? (isprint( src[i] )) ? src[i] : '.'
             : ' ';
    if( 7 == i )
      dst[j++] = ' ';
    }
  dst[j++] = '|';
  dst[j] = '\0';

  return( maxbytes );
  } /* hxo_HexDumpLn */

size_t hxo_DumpBufr( FILE *Outf, const uint8_t *s, const size_t len )
  /** Produce a hexadecimal dump of the contents of the input buffer.
   *
   * @param[in] Outf  A pointer to the output file to which the dump
   *                  should be written.
   * @param[in] s     A pointer to the array of bytes to be dumped.
   * @param[in] len   The size of the array of bytes given by \p s.
   *
   * @returns   The number of lines that were written to \p Outf.
   *
   * @details
   *  This function prints a single HexDump from a single input buffer.
   *  The output is modeled on WireShark's hex dump output and that of the
   *  BSD \c hexdump(1) utility with the -C option.
   *
   *  Each line represents 16 bytes worth of input.  Each line starts with
   *  a number in hex indicating the byte offset of the input at which the
   *  line starts.
   *
   *  Next, there are 16 columns of two-digit hex numbers followed by 16
   *  columns of single characters.  If the input byte is a printable
   *  character it is printed, otherwise it is represented by a dot.
   *
   * \b Notes
   *  - If the input requires multiple buffers, use \c #hxo_InitCtx(),
   *    \c #hxo_DumpCtx(), and \c #hxo_CloseCtx().
   */
  {
  return( HxDmp( 0, Outf, s, len ) );
  } /* HexDump */

hxo_dumpCtx *hxo_InitCtx( hxo_dumpCtx *const Ctx, FILE *Outf )
  /** Initialize a HexDump Context structure.
   *
   * @param[out]  Ctx   A pointer to a \c #hxo_dumpCtx structure to be
   *                    initialized.
   * @param[in]   Outf  A pointer to the output file to which the dump
   *                    should be written.
   *
   * @returns   A pointer to the initialized Context structure; the same as
   *            \p Ctx.  If \p Ctx is NULL, no structure is initialized and
   *            NULL is returned.
   *
   * @details
   *  The HexDump context is used to maintain state over multiple calls to
   *  \c #hxo_DumpCtx().
   *
   * @see \c #hxo_DumpCtx()
   * @see \c #hxo_CloseCtx()
   */
  {
  if( NULL != Ctx )
    {
    (void)memset( Ctx, 0, sizeof( hxo_dumpCtx ) );
    Ctx->Outf = Outf;
    }
  return( Ctx );
  } /* hxo_InitCtx */

size_t hxo_DumpCtx( hxo_dumpCtx *const Ctx,
                    const uint8_t     *s,
                    const size_t       len )
  /** Provide a partial HexDump, saving extra bytes for future calls.
   *
   * @param[in,out] Ctx   A pointer to a \c #hxo_dumpCtx structure, which
   *                      maintains the state of the current Hex Dump.
   * @param[in]     s     A pointer to the array of bytes to be dumped.
   * @param[in]     len   The size of the array of bytes.
   *
   * @returns The number of lines written to the output stream by this call.
   *
   * \b Notes
   *  - The return value is the number of lines written by the individual
   *    call to this function.  The total number of lines written within
   *    the context given by \p Ctx, is stored within \p Ctx.
   *
   * @details
   *  HexDump the input buffer, \p s, but only produce complete HexDump
   *  lines.  This will generate \p len % 16 lines of output.  If there are
   *  any bytes of \p s that are not used, they are stored within the
   *  provided context, \p Ctx, and will be processed at the start of the
   *  next call to this function or by \c #hxo_CloseCtx().
   *
   * @see \c #hxo_InitCtx()
   * @see \c #hxo_CloseCtx()
   */
  {
  size_t lcount    = Ctx->lcount;
  size_t dumpbytes = len;
  size_t cpybytes  = 0;

  /* Clean out leftovers from the previous call.
   */
  if( (Ctx->len) > 0 )
    {
    /* Figure out how many bytes to copy into Ctx->s to fill it.  */
    cpybytes = 16 - (Ctx->len);
    if( len < cpybytes )
      cpybytes = len;
    dumpbytes = len - cpybytes; /* Number of dumpable bytes to remain in <s>. */
    while( cpybytes-- )
      Ctx->s[(Ctx->len)++] = *(s++);        /* Moves <s> as bytes are copied. */
    if( 16 == (Ctx->len) )
      {
      Ctx->lcount = HxDmp( Ctx->lcount, Ctx->Outf, Ctx->s, 16 );
      Ctx->len = 0;
      }
    else
      return( 0 );          /* There are still less than 16 bytes in Ctx->s.  */
    }

  /* HexDump the bulk of the buffer contents.
   */
  cpybytes   = dumpbytes % 16;
  dumpbytes -= cpybytes;
  Ctx->lcount = HxDmp( Ctx->lcount, Ctx->Outf, s, dumpbytes );

  /* Clean up.
   * Store any remaining undumpted bytes.
   */
  while( (cpybytes--) > 0)
    {
    Ctx->s[(Ctx->len)++] = s[dumpbytes++];
    }
  return( (Ctx->lcount) - lcount );
  } /* hxo_DumpCtx */

size_t hxo_CloseCtx( hxo_dumpCtx *const Ctx,
                     const uint8_t     *s,
                     const size_t       len )
  /** Complete a multi-buffer hex dump.
   *
   * @param[in,out] Ctx   A pointer to a hxo_dumpCtx structure, which
   *                      maintains the state of the current Hex Dump.
   * @param[in]     s     A pointer to the array of bytes to be dumped.
   * @param[in]     len   The size of the array of bytes.
   *
   * @returns The total cumulative number of lines output for this HexDump.
   *
   * @details
   *  This function continues the Hex Dump indicated by \p Ctx.  It dumps
   *  the additional bytes given in \p s including the final partial line,
   *  if any.
   */
  {
  /* If there is a final buffer, dump its contents. */
  if( (NULL != s) && (len > 0) )
    (void)hxo_DumpCtx( Ctx, s, len );

  /* Clean up anything left over in the Context.  */
  if( Ctx->len > 0 )
    Ctx->lcount = HxDmp( Ctx->lcount, Ctx->Outf, Ctx->s, Ctx->len );

  /* Total number of lines printed. */
  return( Ctx->lcount );
  } /* hxo_CloseCtx */


#ifdef HEXDUMPMAIN
/* ========================================================================== **
 * Simple HexDump tool (and/or module test program).
 * ========================================================================== **
 */

#include <stdio.h>      /* Standard I/O stuff.  */
#include <stdarg.h>     /* Variable arglists.   */
#include <errno.h>      /* For errno(3).        */
#include <stdlib.h>     /* EXIT_SUCCESS/FAILURE */

/* -------------------------------------------------------------------------- **
 * Macros:
 *
 *  Err()   - Shorthand for (void)fprintf( stderr, ... )
 *  ErrStr  - Shorthand for the error message associated with <errno>.
 */

#define Err( ... ) (void)fprintf( stderr, __VA_ARGS__ )
#define ErrStr     (strerror( errno ))

/* -------------------------------------------------------------------------- **
 * Defined constants.
 *
 *  bSIZE - Read buffer size.
 */

#define bSIZE 4096

/* -------------------------------------------------------------------------- **
 * Program Mainline.
 */

int main( int argc, char *argv[] )
  /* ------------------------------------------------------------------------ **
   * Program Mainline.
   *
   *  Input:  argc  - You know what this is.
   *          argv  - You know what to do.
   *
   *  Output: Zero (0).
   *
   *  Notes:  This is a simple mainline for testing purposes.
   *
   *          It reads from <stdin> or the file specified as argv[1], and
   *          writes a hex dump of the input to <stdout>.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  uint8_t     bufr[bSIZE];
  size_t      result;
  size_t      lcount;
  hxo_dumpCtx Ctx[1];
  FILE       *Inf;

  /* If they tried to provide command-line options, they need help. */
  if( argc > 2 )
    {
    Err( "Usage: HexDump [<input file>]\n" );
    return( 0 );
    }

  /* Figure out from whence our input will arrive.
   */
  if( 1 == argc )
    {
    Inf = stdin;
    (void)fprintf( stderr, "[Reading from <stdin>.]\n" );
    }
  else
    {
    Inf = fopen( argv[1], "r" );
    if( NULL == Inf )
      {
      Err( "Failure opening file \"%s\"; %s\n", argv[1], ErrStr );
      return( EXIT_FAILURE );
      }
    Err( "[Reading from <%s>.]\n", argv[1] );
    }

  /* Read one buffer.
   */
  result = fread( bufr, 1, bSIZE, Inf );
  if( result < 1 )
    {
    /* No data. */
    Err( "Unable to read data from input.\n" );
    return( EXIT_FAILURE );
    }

  if( result < bSIZE )
    {
    /* The buffer isn't full, so use the single-block hxo_DumpBufr() call. */
    lcount = hxo_DumpBufr( stdout, bufr, result );
    }
  else
    {
    /* The first buffer was full. That means we should use multiple buffers.  */
    (void)hxo_InitCtx( Ctx, stdout );
    do
      {
      (void)hxo_DumpCtx( Ctx, bufr, result );
      result = fread( bufr, 1, bSIZE, Inf );
      } while( bSIZE == result );
    /* Now close out the multi-part dump. */
    lcount = hxo_CloseCtx( Ctx, bufr, result );
    }

  /* Finish up. */
  Err( "[%ld line%s produced.]\n", lcount, (1==lcount)?"":"s" );
  return( 0 );
  } /* main */
#endif /* HEXDUMPMAIN */

/* ========================================================================== */
