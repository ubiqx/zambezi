#ifndef HEXOCT_H
#define HEXOCT_H
/* ========================================================================== **
 *                                  HexOct.h
 *
 * Copyright:
 *  Copyright (C) 2002, 2010, 2017, 2020 by Christopher R. Hertel
 *  Copyright (C) 2010 by José Rivera
 *
 * $Id: HexOct.h; 2024-11-25 21:03:56 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 *
 * Description:
 *  Tools to convert to/from Hexadecimal and Octal notations, and to produce
 *  hexdumps.
 *
 * -------------------------------------------------------------------------- **
 *
 * License:
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 *
 * Notes:
 *
 *  - To compile the mainline and produce a test program:
 *    cc -DHEXDUMPMAIN -o HexDump HexOct.c
 *
 * ========================================================================== **
 *//**
 * @file      HexOct.h
 * @author    Christopher R. Hertel
 * @date      2 Jun 2002
 * @version   \$Id: HexOct.h; 2024-11-25 21:03:56 -0600; crh$
 * @copyright Copyright (C) 2002, 2010, 2017, 2020 by Christopher R. Hertel
 * @copyright Copyright (C) 2010 by José Rivera
 * @brief     Convert to/from Hex and Octal notations, and produce hexdumps.
 * @details
 *  This module provides a set of utilities that format binary data in a
 *  programmer-readable way, and interpret user-provided escape sequences.
 *  Basically, conversion to and from raw binary data and printable/typable
 *  text.
 *
 *  This module is a bit of a mashup between two different utility modules
 *  written many years ago for different purposes.  After a decade or two
 *  of copying the code from project to project it seemed sensible to just
 *  formalize it all into a single reusable and maintainable module.
 *
 *  There are several Hex Dump toolkits that can be found via a quick
 *  Internet search.  This one is fairly limited, and little effort has
 *  been put into making it fast and efficient.  Modularizing the whole
 *  thing will make it easier to improve and build upon.
 */

#include <stdio.h>      /* Standard I/O operations and the FILE type. */
#include <stddef.h>     /* For size_t and other standard stuff.       */
#include <stdint.h>     /* Standard integer types, like uint8_t, etc. */


/* -------------------------------------------------------------------------- **
 * Typedefs
 *//**
 * @struct  hxo_dumpCtx
 * @brief   Context structure.
 * @details This structure maintains state across multiple calls to the
 *          hexdump functions, making it possible to dump more than one
 *          buffer's worth of data.
 */
typedef struct
  {
  FILE   *Outf;     /**< File to which to write the output.             */
  size_t  lcount;   /**< Number of lines written to the output stream.  */
  size_t  len;      /**< Number of bytes in the <s[]> field.            */
  uint8_t s[16];    /**< Buffer to hold "leftover" bytes.               */
  } hxo_dumpCtx;


/* -------------------------------------------------------------------------- **
 * Function Prototypes
 */

int hxo_XlateOdigit( const uint8_t digit );

int hxo_XlateXdigit( const uint8_t digit );

int hxo_UnEscSeq( const uint8_t *src, int *esclen );

int hxo_XlateInput( const uint8_t *src );

int hxo_UnEscStr( uint8_t *str );

int hxo_Hexify( uint8_t *dst, const uint8_t *src, const size_t len );

int hxo_HexDumpLn( uint8_t *dst, const uint8_t *src, const size_t len );

size_t hxo_DumpBufr( FILE *Outf, const uint8_t *s, const size_t len );

hxo_dumpCtx *hxo_InitCtx( hxo_dumpCtx *const Ctx, FILE *Outf );

size_t hxo_DumpCtx( hxo_dumpCtx *const Ctx,
                    const uint8_t     *s,
                    const size_t       len );

size_t hxo_CloseCtx( hxo_dumpCtx *const Ctx,
                     const uint8_t     *s,
                     const size_t       len );

/* ========================================================================== */
#endif /* HEXOCT_H */
