/* ========================================================================== **
 *                                 nbt_names.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: NetBIOS over TCP (NBT) Transport; NBT Names.
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: nbt_names.c; 2024-11-25 20:35:33 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *  - Years ago, I started working on a thing called libcifs.  The library
 *    itself didn't go very far, but it did have a complete (though overly
 *    complex) low-level NBT implementation.  That older code is the basis
 *    for this NBT implementation.
 *
 * ========================================================================== **
 */

#include <string.h>     /* For strlen(3). */

#include "nbt_names.h"  /* Module header. */


/* -------------------------------------------------------------------------- **
 * Macros:
 *
 * EncHiNibble( I )
 * EncLoNibble( I )
 *    - Macros for encoding bytes using RFC1001/1002 half-ascii encoding.
 *
 *      Input:  I - Byte value to be encoded.
 *      Output: The encoded nibble, returned as an integer.
 *      Notes:  EncHiNibble() encodes the high-order four bits,
 *              EncLoNibble() encodes the low-order four bits.
 *      Example:
 *              Given the NetBIOS name NEKO<00>, the first byte would be
 *              encoded to two bytes as follows:
 *
 *              target[0] = EncHiNibble( 'N' );
 *              target[1] = EncLoNibble( 'N' );
 */

#define EncHiNibble( I ) (uint8_t)('A' + (((I) & 0xF0) >> 4))
#define EncLoNibble( I ) (uint8_t)('A' + ((I) & 0x0F))


/* -------------------------------------------------------------------------- **
 * Functions
 */

int nbt_UpCaseStr( uint8_t *src, uint8_t *dst, int max )
  /** Copy a string of bytes, converting to upper case in the process.
   *
   * @param[in,out] src - String to be copied and upcased.
   *
   * @param[out]    dst - Destination string into which the \p src string
   *                      will be copied.  This may be the same as \p src,
   *                      in which case the original value of \p src will
   *                      be overwritten.  If \p dst is NULL, \p src to be
   *                      converted in place.  The destination string must
   *                      be at least \p max bytes in length if \p max is
   *                      positive.  If \p max is -1, the destination must
   *                      be at least <tt>strlen( src ) + 1</tt> bytes.
   *
   * @param[in]     max - The maximum number of bytes to copy.  Use -1 to
   *                      upcase until a NUL terminator is encountered.
   *
   * @returns   The string length of the output string, or a negative value
   *            on error.
   *
   * \b Errors
   *  - \b \c nbt_errNullInput - \p src was NULL.
   *
   * \b Notes
   *  - This function is intended to be used to convert un-encoded NetBIOS
   *    names and Scope IDs to upper case.  It operates on ASCII (octet)
   *    strings only, and does not handle Unicode properly.
   *
   *  - When \p max is given as a positive value, this function <em>does
   *    not</em> ensure NUL termination of the destination string.
   *    Exactly \p max octets are written to the destination string.
   *
   *  - When \p max is given as a \c -1, <tt>strlen( src ) + 1 </tt> octets
   *    will be copied to the destination string.  The result \e will
   *    will include the NUL terminator (which \e must be present in the
   *    source string).
   *
   *  - By convention, scope IDs should be converted to upper case before
   *    use on the wire, though there are exceptions in the wild.
   *
   *  - NetBIOS names are almost always converted to upper case before they
   *    are L1 Encoded.  In some rare instances (eg., the malformed names
   *    that IIS registers, or for testing) you may need to leave the case
   *    of the name intact.
   *
   * @see #nbt_L1Encode()
   * @see #nbt_L2Encode()
   * @see #nbt_UpString()
   * @see <a href="@posix/functions/toupper.html">toupper(3)</a>
   */
  {
  int i;

  if( NULL == src )
    return( nbt_errNullInput );

  if( NULL == dst )     /* If <dst> is NULL, then we convert in-place. */
    dst = src;

  if( max < 0 )
    {
    /* Converting a NUL-delimited string. */
    for( i = 0; '\0' != src[i]; i++ )
      dst[i] = (uint8_t)toupper( src[i] );
    dst[i] = '\0';
    }
  else
    {
    /* Converting a fixed-length range of bytes--no imposed NUL!  */
    for( i = 0; i < max; i++ )
      dst[i] = (uint8_t)toupper( src[i] );
    }
  return( i );
  } /* nbt_UpCaseStr */

int nbt_CheckNbName( const uint8_t *name, int len )
  /** Validate an un-encoded NetBIOS name.
   *
   * @param[in] name  - The NetBIOS name to be validated.
   * @param[in] len   - The length, in bytes, of the NetBIOS name or
   *                    \c -1 to use a NUL byte as terminator.
   *
   * @returns   A negative return value indicates a name error or warning.
   *            A positive return value represents the string length of
   *            the given name.  If \p len was given as a positive value,
   *            then \p len will be returned, else we return
   *            <tt>strlen( \p name )</tt>).
   *
   * \b Errors
   *  - \b \c nbt_errNullInput    - \p name was NULL.
   *  - \b \c nbt_errNameTooLong  - \p len is greater than 15.
   *
   * \b Warnings
   *  - \b \c nbt_warnEmptyStr    - \p name is the empty string, "".
   *  - \b \c nbt_warnAsterisk    - \p name begins with an asterisk ('*').
   *  - \b \c nbt_warnContainsDot - \p name contains a dot.
   *  - \b \c nbt_warnNulByte     - \p name contains one or more NUL bytes.
   *
   * \b Notes
   *  - Errors take precedence over warnings.  Warnings are returned only
   *    if no errors are detected.  Only one error or warning value is
   *    returned.
   *
   *  - According to old IBM documentation, an empty string is not a valid
   *    NetBIOS name.  In practice, all sorts of odd names are used, so we
   *    just return a warning if \p len is zero (0) or the string is "".
   *
   *  - [STD19] states that NetBIOS names may not begin with an asterisk
   *    ('*').  The exception, however, is the standard wildcard name
   *    ('*' with NUL padding and suffix).
   *
   *  - Some systems will interpret names that contain dots as DNS names
   *    by default.
   *
   *  - Implementations may choose to follow Microsoft's lead and interpret
   *    long names or names with dots as an indication that \p name should
   *    be interpreted as a DNS name.
   *
   *  - If \p len is greater than zero, a warning will be returned if the
   *    string contains a NUL byte prior to \p name[len].  NetBIOS names
   *    are not supposed to contain NUL bytes but anomalies have been
   *    spotted in the wild, probably because of the Unicode encoding
   *    format used in Windows.
   *
   * @see #nbt_CheckScope()
   * @see #nbt_CheckL2Name()
   */
  {
  int i;

  /* Make sure there really is a name.  */
  if( NULL == name )
    return( nbt_errNullInput );

  /* Simplify...  */
  if( len < 0 )
    len = (int)strlen( (char *)name );

  /* Long names should never get this far, but if they do...  */
  if( len > 15 )
    return( nbt_errNameTooLong );

  /* If the name is the empty string, no other warnings will apply. */
  if( len == 0 )
    return( nbt_warnEmptyStr );

  /* Check for leading asterisk.  */
  if( '*' == *name )
    return( nbt_warnAsterisk );

  /* Look for dots or NULs. */
  for( i = 0; (i < len); i++ )
    {
    if( '.' == name[i] )
      return( nbt_warnContainsDot );
    if( '\0' == name[i] )
      return( nbt_warnNulByte );
    }

  return( len );
  } /* nbt_CheckNbName */

int nbt_CheckScope( const uint8_t *scope )
  /** Validate the syntax of the ScopeID.
   *
   *  @param[in]  scope - The scope string (unencoded) to be validated; a
   *                      NUL-terminated string.
   *
   *  @returns  A negative value is returned to indicate a syntax error in
   *            the scope string.  A positive value is the string length of
   *            \p scope.
   *
   * \b Errors
   *  - \b \c nbt_errNullInput      - \p scope is NULL (empty input).
   *  - \b \c nbt_errLeadingDot     - \p scope starts with an empty label
   *                                  (a leading dot).
   *  - \b \c nbt_errDoubleDot      - \p scope contains an empty label
   *                                  (dot pairs; "..").
   *  - \b \c nbt_errInvalidLblLen  - \p scope contains a label that exceeds
   *                                  the 63 byte limit.
   *  - \b \c nbt_errScopeTooLong   - \p The complete ScopeID exceeds the
   *                                  maximum length.
   *  - \b \c nbt_errEndDot         - \p scope ends with an empty label (a
   *                                  trailing dot).
   *
   * \b Warnings
   *  - \b \c nbt_warnNonPrint    - A label contains a non-printing character.
   *  - \b \c nbt_warnNonAlpha    - A label starts with a non-alpha character.
   *  - \b \c nbt_warnInvalidChar - A label contains an invalid character.
   *  - \b \c nbt_warnNonAlphaNum - A label does not end with an alpha-numeric.
   *
   * \b Notes
   *  - A scope length of 0 is perfectly valid.
   *
   *  - Neither the syntax nor the semantics of the ScopeID were well
   *    understood by the authors of some of the most popular early NBT
   *    implementations.  That's a polite way of saying that Windows really
   *    messed things up.  Some early versions of Windows did very little
   *    (almost no) syntax checking, despite the fact that [STD19] was
   *    fairly clear about what was and what was not permitted.  Scope was,
   *    however, rarely used and is used even less as NBT fades into the
   *    sunset.  The warnings returned from this function are simply for
   *    pedanticism, though it might be wise to pass them along to a user.
   *
   *  - The error values returned by this function represent fatal syntax
   *    errors.  If an error is returned, the ScopeID is invalid, and
   *    \c #nbt_L2Encode() will not be able to encode it.
   *
   *  - Only one error/warning code is returned, so a weak attempt has been
   *    made to prioritize.  Errors, of course, are higher priority than
   *    warnings.
   *
   * @see #nbt_CheckNbName()
   * @see #nbt_CheckL2Name()
   */
  {
  int  i;
  int  j;
  bool nonprint     = false;
  bool leadnonalpha = false;
  bool midnonalnum  = false;
  bool endnonalnum  = false;

  /* Check for NULL input.  */
  if( NULL == scope )
    return( nbt_errNullInput );

  /* Check for leading dot. */
  if( '.' == *scope )
    return( nbt_errLeadingDot );

  /* Check for various problems in the body of the scope string.  */
  for( i = j = 0; ('\0' != scope[i]) && i < 221; i++ )
    {
    if( !isprint( scope[i] ) )
      nonprint = true;                /* Non-printing character. */

    if( '.' == scope[i] )
      {
      if( 0 == j )
        return( nbt_errDoubleDot );   /* Found ".." */
      j = 0;
      if( !isalnum( scope[i-1] ) )    /* Found non-alphanumeric at label end. */
        endnonalnum = true;
      }
    else
      {
      if( ++j > 63 )
        return( nbt_errInvalidLblLen ); /* Label too long. */
      if( 1 == j )
        {
        if( !isalpha( scope[i] ) )
          leadnonalpha = true;        /* First label char non-alpha. */
        }
      else
        {
        if( !(('-' == scope[i]) || isalnum( scope[i] )) )
          midnonalnum = true;         /* Found invalid char in label. */
        }
      }
    }

  /* Check to see if total ScopeID length is too long.  */
  if( i > 220 )
    return( nbt_errScopeTooLong );    /* ScopeID exceeds max.   */

  /* The Scope ID should not end in a dot.  */
  if( (0 == j) && (i > 0) )
    return( nbt_errEndDot );          /* ScopeID ends in a dot. */

  /* Check for non-alphanumeric at final label end.
   */
  if( (i > 0) && !isalnum( scope[i-1] ) )
    endnonalnum = true;

  /* Report a warning, if any.  */
  if( nonprint )
    return( nbt_warnNonPrint );
  if( leadnonalpha )
    return( nbt_warnNonAlpha );
  if( midnonalnum )
    return( nbt_warnInvalidChar );
  if( endnonalnum )
    return( nbt_warnNonAlphaNum );

  /* The ScopeID is good.  Return it's encoded length.  */
  return( i );
  } /* nbt_CheckScope */

int nbt_CheckL2Name( const uint8_t *src, int srcpos, const int srcmax )
  /** Validate the Level Two Encoded NBT name in a NBT NS packet.
   *
   * @param[in] src     - Pointer to a byte array containing the Name
   *                      Service packet.
   *
   * @param[in] srcpos  - Starting position of the encoded NBT name within
   *                      the \p src buffer.
   *
   * @param[in] srcmax  - The size of the buffer.
   *
   * @returns   If positive, the return value is the length of the encoded
   *            NBT name *including* the terminating NUL label.\n
   *            If negative, an error code.
   *
   * \b Errors
   *  - \b \c nbt_errInvalidLblLen  - The starting label was invalid.  The
   *                                  first label must always be 0x20,
   *                                  indicating a length of 32 bytes.
   *  - \b \c nbt_errBadLblFlag     - A non-zero label flag was found.
   *                                  (It could be a label string pointer.)
   *  - \b \c nbt_errOutOfBounds    - The value of \p srcpos is at or beyond
   *                                  the end of the buffer.
   *  - \b \c nbt_errTruncatedBufr  - The NBT name string exceeds the end of
   *                                  \p src. (Truncated buffer.)
   *  - \b \c nbt_errNameTooLong    - The NBT name exceeds the 255 byte maximum.
   *
   * \b Notes
   *  - \p src should point to a buffer containing a complete Name Service
   *    packet.  \p srcpos is the byte offset, relative to the start of the
   *    packet, at which the L2-encoded NBT name is expected to be found.
   *
   *  - The first label is always the encoded NetBIOS name, which must be
   *    32 bytes long.  The first length field must, therefore, have a value
   *    of 0x20 (32).  If not, an error is returned.
   *
   *  - In NBT, label string pointers (LSPs) are only used at the start of
   *    names, and only in certain record types.  Use the \c #nbt_CheckLSP()
   *    function to determine whether the first label is an LSP.
   *
   *  - On success, this function returns the length (including the final
   *    root label) of the encoded NBT name.  The length can also be
   *    calculated (without the overhead of syntax checking) by using
   *    \c strlen(3):
   *    \code{.c}
   *      (strlen( &src[srcpos] ) + 1);
   *    \endcode
   *    The #nbt_L2NameLen() macro implements the above formula.
   *
   * @see #nbt_CheckScope()
   * @see #nbt_CheckNbName()
   * @see #nbt_CheckLSP()
   * @see #nbt_L2NameLen()
   */
  {
  int len;
  int startpos = srcpos;

  /* Make sure we are within the limits.
   */
  if( srcpos >= srcmax )
    return( nbt_errOutOfBounds );

  /* First label should always have length 0x20.
   */
  if( 0 != (src[srcpos] & 0xC0) )
    return( nbt_errBadLblFlag );
  if( 0x20 != src[srcpos] )
    return( nbt_errInvalidLblLen );
  len = 32;

  /* Check each additional label in turn.
   */
  while( len > 0 )
    {
    srcpos += len + 1;
    if( srcpos > srcmax )
      return( nbt_errTruncatedBufr );
    if( 0 != (src[srcpos] & 0xC0) )
      return( nbt_errBadLblFlag );
    len = (int)(src[srcpos] & 0x3F);
    }

  /* We stopped with srcpos indicating the final label.
   */
  len = (srcpos + 1) - startpos;
  if( len > 255 )
    return( nbt_errNameTooLong );

  /* All okay.
   */
  return( len );
  } /* nbt_CheckL2Name */

int nbt_CheckLSP( uint8_t lablen )
  /** Check a label length octet to see if it contains a label string pointer.
   *
   * @param[in] lablen  - An octet, assumed to be a label length field from
   *                      a 2nd level (L2) encoded NBT name.
   *
   * @returns   If positive, the offset given by the LSP.\n
   *            If zero, then \p lablen was not an LSP and the label length
   *            is simply <tt>(int)lablen</tt>.\n
   *            If negative, an error.
   *
   * \b Errors
   *  - \b \c nbt_errBadLblFlag - Indicates that \p lablen has an invalid
   *                              flag set, so it can be neither a valid
   *                              label length nor a label string pointer.
   * @see #nbt_CheckL2Name()
   */
  {
  int flags = lablen & 0xC0;

  if( 0xC0 == flags )
    return( lablen & 0x3F );

  if( flags )
    return( nbt_errBadLblFlag );

  return( 0 );
  } /* nbt_CheckLSP */

int nbt_L1Encode( uint8_t *dst, const nbt_NameRec *src )
  /** Encode a NetBIOS name using First Level Encoding.
   *
   * @param[out] dst  - A pointer to a target buffer into which the encoded
   *                    name will be written.  Minimum buffer size is 33 bytes.
   * @param[in]  src  - A pointer to an #nbt_NameRec structure.
   *                    The \c src->scopeId field is ignored, but all other
   *                    fields are used to create the L1 encoded name.
   *
   * @returns   The string length of the resultant string (always 32).
   *            The returned length <em>does not</em> include the
   *            terminating NUL byte.
   *
   * \b Notes
   *  - This function does \b no syntax checking.  Use #nbt_CheckNbName()
   *    to check the name before calling this function.  (Syntax checking
   *    is skipped so that nonstandard names can be encoded.)
   *
   *  - This function does *not* convert the NetBIOS name to uppercase.
   *    That must be done in a separate step, prior to encoding the name.
   *    (There are odd cases in which names are not up-cased before being
   *    encoded.)
   *
   *  - To be pedantic, when the RFCs talk about First Level (L1) Encoding,
   *    they mean both the encoded (32-byte form) NetBIOS name \e and the
   *    appended Scope ID.  The fully qualified name is rarely used in its
   *    L1 Encoded form, however, so it seems simpler to perform only the
   *    half-ascii encoding in this function.  If you need the complete L1
   *    name, it is simply:
   *
   *            L1_NetBIOS_name + "." + scopeId
   *
   *  - The encoded NetBIOS name is always 33 bytes long, including the
   *    terminating NUL byte.  If the ScopeID is the the empty string, then
   *    those 33 bytes map directly to the Second Level (L2) Encoded NBT
   *    name, sans the leading length byte.  In L2 encoding, the terminating
   *    NUL is interpreted as a NUL label.
   *
   * @see #nbt_UpCaseStr()
   * @see #nbt_L2Encode()
   * @see #nbt_L1Decode()
   * @see <a href=
   *    "https://www.ietf.org/rfc/rfc1001.html#section-14.1">[RFC1001; 14.1]</a>
   */
  {
  int     i,  j;
  uint8_t hi, lo;

  /* Encode the name using RFC 1001/1002 First Level Encoding.  */
  hi = ( src->nameLen > 15 ) ? 15 : src->nameLen;   /* Ensure max 15 bytes. */
  for( i = 0, j = 0; i < hi; i++ )
    {
    dst[j++] = EncHiNibble( src->name[i] );
    dst[j++] = EncLoNibble( src->name[i] );
    }

  /* Encode the pad byte and fill in any unused name bytes. */
  hi = EncHiNibble( src->pad );
  lo = EncLoNibble( src->pad );
  while( j < 30 )
    {
    dst[j++] = hi;
    dst[j++] = lo;
    }

  /* Encode the suffix byte, then terminate the string. */
  dst[30] = EncHiNibble( src->sfx );
  dst[31] = EncLoNibble( src->sfx );
  dst[32] = '\0';

  /* Return the resulting string length. */
  return( 32 );
  } /* nbt_L1Encode */

int nbt_L1Decode( uint8_t       *dst,
                  const uint8_t *src,
                  const int      srcpos,
                  const uint8_t  pad,
                  uint8_t       *sfx )
  /** Decode a Level One Encoded NetBIOS name.
   *
   * @param[out]  dst     - Target into which the name will be written.
   *                        This must be a minimum of 16 bytes.  The decoded
   *                        name will be a maximum of 15 bytes, plus one byte
   *                        for the NUL byte string terminator.
   *
   * @param[in]   src     - A pointer to a buffer which contains an L1
   *                        encoded NetBIOS name.
   *
   * @param[in]   srcpos  - Offset into \p src at which the L1 encoded
   *                        NetBIOS name begins.
   *
   * @param[in]   pad     - The padding character.
   *                        Trailing \p pad bytes will be replaced with NULs.
   *                        Eg., if \p pad is the space character
   *                        (0x20 == ' ') then trailing spaces will be
   *                        replaced with NULs in the decoded NetBIOS name.
   *                        If the NUL byte is given, then no trailing bytes
   *                        will be trimmed.\n
   *                        Typically, either ' ' or '\0' should be used.
   *
   * @param[out]  sfx     - A pointer to an unsigned byte to receive the
   *                        value of the suffix byte.
   *
   * @returns   If negative, an error code.
   *            Otherwise, the length of the decoded NetBIOS name.
   *
   * \b Errors
   *  - \b \c nbt_errBadL1Value - Indicates that the source name contains a
   *                              byte value outside of the 'A'..'P' range.
   *
   * \b Notes
   *  - \p src may either point to the L1 encoded form of the NBT name or to
   *    the second byte of the L2 encoded form.  (The second byte of an L2
   *    encoded name is the start of the L1 name.)
   *
   *  - See the notes in #nbt_L2Decode() regarding decoding of an L2 encoded
   *    ScopeID.
   *
   *  - You can use this function to decode a string into the same location
   *    as the source.  That is, <tt>(dst == &src[srcpos])</tt> is safe.
   *
   *  - The decoded string \b may contain non-printing characters.
   *    Consider, for example, the "\x1\x2__MSBROWSE__\x2<01>" name, commonly
   *    used by Browser nodes.
   *
   *  - The resulting \p dst string may also include NUL bytes (because some
   *    systems encode Unicode UTF-16LE strings directly).  Be sure to use
   *    the return value (assuming it's not an error code) as the string
   *    length.
   *
   * @see #nbt_L2Decode()
   * @see #nbt_L1Encode()
   */
  {
  int i, j;
  int nibble;

  /* Every two encoded bytes reduces to a single NetBIOS name byte. */
  for( i = 0, j = srcpos; i < nbt_NB_NAME_MAX; i++ )
    {
    nibble = (src[j++] - 'A');              /* First nibble. */
    if( (nibble < 0) || (nibble > 0x0F) )
      return( nbt_errBadL1Value );
    dst[i] = (uint8_t)(nibble << 4);

    nibble = (src[j++] - 'A');              /* Second nibble. */
    if( (nibble < 0) || (nibble > 0x0F) )
      return( nbt_errBadL1Value );
    dst[i] |= (uint8_t)nibble;
    }

  /* Move the suffix out of the way and terminate the string. */
  *sfx = dst[15];
  dst[15] = '\0';

  /* Return 15 if we're not stripping the padding.  */
  if( '\0' == pad )
    return( 15 );

  /* Trim padding from end and return the resulting string length.  */
  for( i = 14; (i >= 0) && (pad == dst[i]); i-- )
    dst[i] = '\0';
  return( i + 1 );
  } /* nbt_L1Decode */


int nbt_L2Encode( uint8_t *dst, const nbt_NameRec *namerec )
  /** Encode a NetBIOS name and Scope ID using Second Level Encoding.
   *
   * @param[out]  dst     - A pointer to a target buffer into which the
   *                        encoded name will be written.  The buffer should
   *                        be at least 255 bytes in size.
   *
   * @param[in]   namerec - A pointer to an nbt_NameRec structure which
   *                        contains all of parts to be assembled into an
   *                        L2 NBT name.
   *
   * @returns   The length, in bytes, of the resulting encoded name.  This
   *            count \b includes the final NUL byte, because the trailing
   *            NUL represents the root of the DNS namespace and is,
   *            therefore, part of the NBT name.
   *
   * \b Notes
   *  - No syntax checking is performed on the NetBIOS name or the ScopeID.
   *    They should be checked before they get this far.  Syntax checks are
   *    skipped at this stage to accommodate unusual or non-standard values.
   *
   *  - This function does not convert either the ScopeID or the NetBIOS
   *    name to upper-case before encoding.  Conversion to upper case should
   *    be performed in a separate step before this function is called.
   *
   *  - The ScopeID in \p namerec may be NULL.  A value of NULL is handled
   *    as if it were the empty string ("").
   *
   * @see #nbt_UpCaseStr()
   * @see #nbt_L2Decode()
   * @see <a href=
   *    "https://www.ietf.org/rfc/rfc1002.html#section-4.1">[RFC1002; 4.1]</a>
   */
  {
  int      lenpos;
  int      i;
  int      j;
  uint8_t *scope = namerec->scopeId;

  /* First-level encode the NetBIOS name,
   * add label length, and move lenpos to the end.
   */
  (void)nbt_L1Encode( &dst[1], namerec );
  dst[0] = 0x20;
  lenpos = 33;

  /* Encode each label in the scope ID.
   */
  if( (NULL != scope) && ('\0' != *scope) )
    {
    do
      {
      /* Count label length as it is being copied to dst.
       */
      for( i = 0, j = (lenpos + 1);
           ('.' != scope[i]) && ('\0' != scope[i]);
           i++, j++)
        dst[j] = scope[i];

      /* Write the length one byte before of the copied label,
       * advance lenpos to the next writeable space, and
       * advance scope to the next terminator.  If that terminator
       * is a '.', then we will increment scope and continue.
       */
      dst[lenpos] = (uint8_t)(0x3F & i);
      lenpos     += i + 1;
      scope      += i;
      } while( '.' == *(scope++) );
    dst[lenpos] = '\0';
    }

  /* Add one to lenpos to get actual length of the encoded name.
   */
  return( lenpos + 1 );
  } /* nbt_L2Encode */

int nbt_L2Decode( uint8_t *dst, const uint8_t *src, int srcpos )
  /** Decode an L2 encoded NBT name string.
   *
   * @param[out]  dst     - Target buffer.  To be safe, this should be at
   *                        least #nbt_L2_NAME_MAX bytes in size.  It is
   *                        sufficient to provide a buffer that is
   *                        <tt>strlen( &(src[srcpos]) )</tt> bytes long
   *                        (that is, the same length as the string being
   *                        decoded).
   *
   * @param[in]   src     - Source buffer. Probably a Name Service packet.
   *
   * @param[in]   srcpos  - The location of the NBT name within the packet.
   *                        This function <em>does not</em> follow Label
   *                        String Pointers (LSPs).
   *
   * @returns   The string length of the decoded NBT name.
   *
   * \b Notes
   *  - This function does not validate the NBT name.
   *    Use the #nbt_CheckL2Name() function to validate the name.
   *
   *  - To decode just the scope of an NBT name, set \p srcpos 33 bytes
   *    beyond the start of the name in \p src.  The first label should
   *    always be the 32-byte encoded NetBIOS name plus its label length
   *    byte.  You can L2Decode the entire NBT name, but then you still
   *    need to decode the L1-encoded NetBIOS name.  It may be easier to
   *    decode the two parts separately.
   *
   *  - Regarding the size of the \p dst buffer:  One byte is lost when
   *    decoding L2 encoding.  For example, consider this L2 encoded name:
   *    \code
   *          "\x20EOGFGLGPCACACACACACACACACACACAAA\x03CAT\x03ORG\0"
   *    \endcode
   *    The length of that name is 42 bytes.  If you use the strlen(3)
   *    function on the above string you will get 41, because strlen(3) sees
   *    the trailing '\0' as a terminator, not a label.  That's all okay,
   *    though, because when the string is decoded the result will be:
   *    \code
   *          "EOGFGLGPCACACACACACACACACACACAAA.CAT.ORG"
   *    \endcode
   *    which has a \c strlen() of 40, but requires 41 bytes so that the
   *    terminating NUL can be stored.  So, <tt>strlen( &(src[srcpos]) )</tt>
   *    is the minimum required to store the resultant string.
   *
   * @see #nbt_CheckL2Name()
   * @see #nbt_L2Encode()
   */
  {
  int len, i, j;

  i   = 0;
  len = src[srcpos++];
  while( len > 0 )
    {
    for( j = 0; j < len; j++ )
      dst[i++] = src[srcpos++];
    len = src[srcpos++];
    if( len > 0 )
      dst[i++] = '.';
    }
  dst[i] = '\0';

  return( i );
  } /* nbt_L2Decode */

int nbt_EncodeName( uint8_t           *dst,
                    const int          dstpos,
                    const int          dstlen,
                    const nbt_NameRec *namerec )
  /** Fully encode an NBT name from the parts provided.
   *
   * @param[out]  dst     - A pointer to the destination buffer into which
   *                        the encoded name will be written.
   *
   * @param[in]   dstpos  - Offset into \p dst at which to write the encoded
   *                        name.
   *
   * @param[in]   dstlen  - Total bytes in \p dst.  The encoded name must fit
   *                        into <tt>(dstlen - dstpos)</tt> bytes.
   *
   * @param[in]   namerec - A pointer to a name record.
   *                        The \c name, \c pad, \c sfx, and \c scopeId
   *                        fields should be properly initialized (a
   *                        \c scopeId of NULL is equivalent to the empty
   *                        scope: "").
   *
   * @returns   If negative, an error or warning code.\n
   *            Otherwise, the number of bytes in the resultant encoded
   *            name.  The count includes the final NUL byte, because the
   *            trailing NUL represents the root of the DNS namespace and
   *            is, therefore, part of the NBT name.
   *
   * \b Errors
   *  - \b \c nbt_errNullInput      - One or more input parameters were (and
   *                                  should not have been) NULL.  Check
   *                                  \p namerec, \p dst, and \c namerec->name.
   *  - \b \c nbt_errNameTooLong    - \c namerec->nameLen exceeds 15 bytes.
   *  - \b \c nbt_errLeadingDot     - \c namerec->scopeId starts with empty
   *                                  label (leading dot).
   *  - \b \c nbt_errDoubleDot      - \c namerec->scopeId contains an empty
   *                                  label (multiple dots).
   *  - \b \c nbt_errEndDot         - \c namerec->scopeId ends with empty
   *                                  label (trailing dot).
   *  - \b \c nbt_errInvalidLblLen  - A label in \c namerec->scopeId exceeds
   *                                  the 63 byte maximum.
   *  - \b \c nbt_errScopeTooLong   - \c namerec->scopeId exceeds the maximum
   *                                  length.
   *  - \b \c nbt_errBufrTooSmall   - Not enough room in \p dst to hold the
   *                                  encoded result.
   *
   * \b Warnings
   *  - \b \c nbt_warnEmptyStr    - \c namerec->name is the empty string, "".
   *  - \b \c nbt_warnAsterisk    - \c namerec->name begins with an asterisk
   *                                ('*').
   *  - \b \c nbt_warnContainsDot - \c namerec->name contains a dot.
   *                                Some Windows systems will interpret names
   *                                with dots as DNS names.
   *  - \b \c nbt_warnNulByte     - \c namerec->name contains one or more NUL
   *                                bytes.
   *  - \b \c nbt_warnNonPrint    - \c namerec->scopeId contains a
   *                                non-printing character.
   *  - \b \c nbt_warnNonAlpha    - A \c namerec->scopeId label does not
   *                                start with an alpha character (as it
   *                                should).
   *  - \b \c nbt_warnInvalidChar - A \c namerec->scopeId label contains an
   *                                invalid character (use alphanumeric or
   *                                '-').
   *  - \b \c nbt_warnNonAlphaNum - A \c namerec->scopeId label does not end
   *                                with an alpha-numeric.
   *
   * \b Notes
   *  - This function is a convenient front-end to the #nbt_L2Encode()
   *    function.  It also performs all of the recommended syntax checks.
   */
  {
  int result;
  int length  = nbt_L2_NB_NAME_MIN;
  int warning = 0;

  /* Check for valid input.
   */
  if( NULL == namerec || NULL == dst )
    return( nbt_errNullInput );

  /* Validate the NetBIOS name.
   */
  result = nbt_CheckNbName( namerec->name, namerec->nameLen );
  if( result < 0 )
    return( result );

  /* If there's a non-empty Scope ID, check it.
   * Store warnings, but continue with the encoding anyway.
   */
  if( (NULL != namerec->scopeId) && ('\0' != *(namerec->scopeId)) )
    {
    result = nbt_CheckScope( namerec->scopeId );
    if( result < 0 && nbt_errISERROR( result ) )
      return( result );
    if( nbt_errISWARN( result ) )
      {
      warning = result;
      result  = (int)strlen( (char *)namerec->scopeId );
      }
    length += result + 1;
    }

  /* Verify that the buffer is large enough.
   */
  if( ((dstlen - dstpos) - length) < 0 )
    return( nbt_errBufrTooSmall );

  /* Okay.  Let's go.
   */
  result = nbt_L2Encode( dst + dstpos, namerec );
  if( (result > 0) && warning )
    return( warning );
  return( result );
  } /* nbt_EncodeName */


/* =================================== ETX ================================== */
