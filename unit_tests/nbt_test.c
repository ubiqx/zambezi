/* ========================================================================== **
 *                                 nbt_test.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Zambezi NBT Subsystem Unit Tests
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: nbt_test.c; 2020-11-04 09:29:08 -0600; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file      nbt_test.c
 * @author    Christopher R. Hertel
 * @date      28 Oct 2020
 * @version   \$Id: nbt_test.c; 2020-11-04 09:29:08 -0600; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 * @brief     Unit tests for the Zambezi NBT toolkit.
 * @details
 *  Basic "does it work" unit tests for the NBT subsystem, providing minimal
 *  code validation.
 *
 * @cond  Doxygen disabled from here to EOF.
 */

#include <stdio.h>          /* Yer basic I/O stuff.         */
#include <stdint.h>         /* Standard integer types.      */
#include <string.h>         /* For memcmp(3), et al.        */
#include <stdarg.h>         /* Variable arglists.           */

#include "nbt/nbt_names.h"  /* NBT name manipulations.      */
#include "nbt/nbt_nbns.h"   /* NBT Name Service support.    */
#include "nbt/nbt_nbss.h"   /* NBT Session Service support. */
#include "util/HexOct.h"    /* Useful when debugging.       */


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *  bSIZE   - Generic buffer size.
 */

#define bSIZE 512


/* -------------------------------------------------------------------------- **
 * Macros
 *
 *  Err()   - Shorthand for (void)fprintf( stderr, ... )
 *  ErrStr  - Shorthand for the error message associated with <errno>.
 *  pl()    - Plural or not?  If so, return "s", else the empty string.
 *            (Useful for presenting output.)
 */

#define Err( ... ) (void)fprintf( stderr, __VA_ARGS__ )
#define ErrStr     (strerror( errno ))
#define pl( X )    ((1==(X))?"":"s")


/* -------------------------------------------------------------------------- **
 * Static Functions
 */

static int names_test( void )
  /* ------------------------------------------------------------------------ **
   * Unit tests for the nbt_names module.
   * ------------------------------------------------------------------------ **
   */
  {
  int   rslt;
  int   failcount   = 0;
  char *test_name   = "*Noodle boat";       /* Intentional leading asterisk.  */
  char *test_scope  = "greenbean.pasta.icecream";

  Err( "nbt_names: NetBIOS and NBT Name Handling.\n" );

  /* nbt_CheckNbName() and nbt_L1Encode(). */
  nbt_L1Name  L1_name;
  nbt_NameRec name_rec[] = { { strlen( test_name ), (uint8_t *)test_name,
                               ':', '!', (uint8_t *)test_scope } };

  if( nbt_warnAsterisk != (rslt = nbt_CheckNbName( (uint8_t *)test_name, -1 )) )
    {
    Err( "nbt_CheckNbName( \"%s\" ) ==> %d.\n", test_name, rslt );
    failcount++;
    }
  if( 32 != (rslt = nbt_L1Encode( L1_name, name_rec )) )
    {
    Err( "nbt_L1Encode( \"%s\"[pad:'%c',sfx:'%c'] ) ==> %d (err)\n",
          test_name, name_rec->pad, name_rec->sfx, rslt );
    failcount++;
    }

  /* Test nbt_EncodeName(), which calls nbt_L2Encode() (and others). */
  nbt_L2Name L2_name;
  if( nbt_errNullInput != (rslt = nbt_EncodeName( NULL, 0, 1000, name_rec )) )
    {
    failcount++;
    Err( "nbt_EncodeName( NULL ) ==> -0x%.4x.\n", -rslt );
    }
  rslt = nbt_EncodeName( L2_name, 0, nbt_L2_NAME_MAX, name_rec );
  if( (rslt < 0) && (nbt_warnAsterisk != rslt) )
    {
    Err( "nbt_EncodeName()  ==> -0x%.4x\n", -rslt );
    return( ++failcount );
    }

  /* Force L2 encoding, and trudge onward.  */
  (void)nbt_L2Encode( L2_name, name_rec );
  rslt = nbt_CheckL2Name( L2_name, 0, 1+strlen( (char *)L2_name ) );
  if( rslt < 0 )
    {
    Err( "nbt_CheckL2Name() ==> -0x%.4x\n", -rslt );
    return( ++failcount );
    }

  /* Test name decoding. */
  nbt_L2Name deBufr;

  if( (rslt = nbt_L2Decode( deBufr, L2_name, 0 )) < 0 )
    {
    Err( "nbt_L2Decode() ==> -0x%.4x\n", -rslt );
    return( ++failcount );
    }
  if( memcmp( test_scope, &deBufr[33], 1+strlen( test_scope ) ) )
    {
    failcount++;
    Err( "nbt_L2Decode(): ScopeId missmatch on decode.\n" );
    Err( "  L2 Decoded: [%s]\n", deBufr );
    }
  if( (rslt = nbt_L1Decode( L1_name, deBufr, 0, ':', L2_name )) < 0 )
    {
    Err( "nbt_L1Decode() ==> -0x%.4x\n", -rslt );
    Err( "  L1 Decoded: [%s]\n", L1_name );
    return( ++failcount );
    }
  if( '!' != *L2_name )
    {
    Err( "nbt_L1Decode() misread suffix byte.  [Got '%c' not '!'.]\n",
         *L2_name );
    return( ++failcount );
    }

  /* Finish up. */
  return( failcount );
  } /* names_test */

static int nbns_test( void )
  /* ------------------------------------------------------------------------ **
   * Name Service primatives testing.
   * ------------------------------------------------------------------------ **
   */
  {
  int       rslt;
  uint8_t   bufr[bSIZE];
  int       failcount = 0;
  nbt_nsMsg nsMsg[1]  =
    { {
      nbt_nsNULL_MSG_TYPE,                    /* type         */
      0xfeed,                                 /* tid          */
      (nbt_nsOPCODE_REGISTER | nbt_nsRD_BIT), /* flags        */
      (nbt_nsQUERYREC | nbt_nsADDREC),        /* rmap         */
      NULL,                                   /* Q_name       */
      0,                                      /* Q_name_len   */
      nbt_nsQTYPE_NB,                         /* Q_type       */
      true,                                   /* lsp          */
      NULL,                                   /* RR_name      */
      0,                                      /* RR_name_len  */
      nbt_nsRRTYPE_NB,                        /* RR_type      */
      0x4FFF7,                                /* ttl          */
      NULL,                                   /* rdata        */
      6                                       /* rdata_len    */
      } };

  Err( "nbt_nbns:  NBT Name Service.\n" );

  /* Build the L2 name.  */
  int             msgLen;
  nbt_L2Name      L2_name;
  nbt_NameRec     name_rec[] = {{ 8, (uint8_t *)"Foobunny", ' ', 0x87, NULL }};
  nbt_nsAddrEntry addr_ent[] = {{ nbt_nsGROUP_BIT | nbt_nsONT_P, 0x7f000001 }};

  if( (rslt = nbt_EncodeName( L2_name, 0, nbt_L2_NAME_MAX, name_rec )) < 0 )
    {
    Err( "nbt_EncodeName() ==> -0x%.4x\n", -rslt );
    return( ++failcount );
    }

  /* Build a P-mode (Unicast) Name Registration Request message.  */
  nsMsg->Q_name     = L2_name;
  nsMsg->Q_name_len = rslt;
  if( (rslt = nbt_nsPackMsg( nsMsg, bufr, bSIZE )) < 0 )
    {
    Err( "nbt_nsPackMsg() ==> -0x%.4x\n", -rslt );
    return( ++failcount );
    }

  /* Add the Address Entry RDATA blob. */
  msgLen = rslt;
  (void)memset( &bufr[msgLen], 001, (bSIZE - msgLen) );
  rslt = nbt_nsPackAddrEntry( addr_ent, &bufr[msgLen], (bSIZE-msgLen) );
  if( rslt < 0 )
    {
    Err( "nbt_nsPackAddrEntry() ==> -0x%.4x\n", -rslt );
    return( ++failcount );
    }
  msgLen += rslt;

  /* Unpack the same message. */
  if( (rslt = nbt_nsParseMsg( bufr, msgLen, nsMsg )) < 0 )
    {
    Err( "nbt_nsParseMsg() ==> -0x%.4x\n", -rslt );
    return( ++failcount );
    }

  /* Unpack the RDATA blob. */
  rslt = nbt_nsParseAddrEntry( &bufr[rslt], nsMsg->rdata_len, addr_ent );
  if( rslt < 0 )
    {
    Err( "nbt_nsParseAddrEntry() ==> -0x%.4x\n", -rslt );
    return( ++failcount );
    }

  /* Verify contents of the RDATA blob. */
  if( (addr_ent->NB_flags != (nbt_nsGROUP_BIT | nbt_nsONT_P))
   || (addr_ent->NB_addr != 0x7f000001) )
    {
    failcount++;
    Err( "nbt_nsParseAddrEntry() mangled the RDATA.\n" );
    Err( "  NB_flags: 0x%.4x\n", addr_ent->NB_flags );
    Err( "  NB_addr:  0x%.8x\n", addr_ent->NB_addr );
    }
  /* Verify contents of nsMsg. */
  if( nbt_nsNAME_REG_REQST != nsMsg->type )
    {
    failcount++;
    Err( "nbt_ParseMsg() wrong message type (%d).\n", nsMsg->type );
    }

  /* Finish up. */
  return( failcount );
  } /* nbns_test */

static int nbss_test( void )
  /* ------------------------------------------------------------------------ **
   * Test NBT Session Service primatives.
   * ------------------------------------------------------------------------ **
   */
  {
  int rslt;
  int msgLen;
  int failcount = 0;
  uint8_t bufr[bSIZE];
  nbt_ssMsg ssMsg[1];

  Err( "nbt_nbss:  NBT Session Service.\n" );

  /* Build and test a Negative Session Response message. */
  ssMsg->Type    = NBT_SS_SESS_NEG_RESP;
  ssMsg->Length  = nbt_ssTrailBytes( ssMsg->Type );
  ssMsg->ErrCode = NBT_SS_ERR_UNSPECIFIED;
  if( (rslt = nbt_ssPackMsg( ssMsg, bufr, bSIZE )) < 0 )
    {
    Err( "nbt_ssPackMsg() ==> -0x%.4x\n", -rslt );
    return( ++failcount );
    }
  if( (msgLen = rslt) != (4 + ssMsg->Length) )
    {
    failcount++;
    Err( "nbt_ssPackMsg(): Returned %d, Expected: %d.\n",
          msgLen, (4 + ssMsg->Length) );
    }

  /* Parse and validate the message we created. */
  if( (rslt = nbt_ssParseMsg( bufr, ssMsg )) < 0 )
    {
    Err( "nbt_ssParseMsg() ==> -0x%.4x\n", -rslt );
    return( ++failcount );
    }
  if( rslt != 4 )
    {
    failcount++;
    Err( "nbt_ssParseMsg(): Returned %d, Expected: 4.\n", rslt );
    }

  /* Parse the trailer. */
  if( (rslt = nbt_ssParseTrailer( &bufr[4], msgLen-4, ssMsg )) < 0 )
    {
    failcount++;
    Err( "nbt_ssParseTrailer(): ==> -0x%.4x\n", -rslt );
    }
  if( !nbt_ssCheckEcode( ssMsg->ErrCode ) )
    {
    failcount++;
    Err( "nbt_ssParseTrailer(): Mangled error code: %.2x\n", ssMsg->ErrCode );
    }

  return( failcount );
  } /* nbss_test */


/* -------------------------------------------------------------------------- **
 * Functions
 */
int main( void )
  {
  int rslt;
  int failcount = 0;

  failcount += (rslt = names_test());
  Err( "nbt_names: Unit test;  %d failure%s.\n", rslt, pl(rslt) );

  failcount += (rslt = nbns_test());
  Err( "nbt_nbns:  Unit test;  %d failure%s.\n", rslt, pl(rslt) );

  failcount += (rslt = nbss_test());
  Err( "nbt_nbss:  Unit test;  %d failure%s.\n", rslt, pl(rslt) );

  Err( "Total:  %d failure%s.\n", failcount, pl( failcount ) );
  return( (failcount > 0) ? EXIT_FAILURE : EXIT_SUCCESS );
  } /* main */

/* ========================== ...and so it goes... ========================== */
/** @endcond */
