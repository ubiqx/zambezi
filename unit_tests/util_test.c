/* ========================================================================== **
 *                                 util_test.c
 * -------------------------------------------------------------------------- **
 * Author:      Christopher R. Hertel
 * Description: Zambezi Utility Modules Unit Tests
 *
 * Copyright (C) 2020 by Christopher R. Hertel
 * $Id: util_test.c; 2020-10-29 15:04:31 -0500; crh$
 *
 * -------------------------------------------------------------------------- **
 * License:
 *
 *  This file is part of Zambezi.
 *
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3.0 of the License, or (at
 *  your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 * Notes:
 *
 * ========================================================================== **
 *//**
 * @file      util_test.c
 * @author    Christopher R. Hertel
 * @date      28 Oct 2020
 * @version   \$Id: util_test.c; 2020-10-29 15:04:31 -0500; crh$
 * @copyright Copyright (C) 2020 by Christopher R. Hertel
 * @brief     Simple unit tests for Zambezi project utility modules.
 * @details
 *  These are basic "does it work" unit tests, not comprehensive validation.
 *  New tests may be added as needed.
 *
 * @cond  Doxygen disabled from here to EOF.
 */

#include <stdio.h>        /* Standard I/O stuff.      */
#include <stdarg.h>       /* Variable arglists.       */
#include <errno.h>        /* For errno(3).            */
#include <stdlib.h>       /* EXIT_SUCCESS/FAILURE.    */
#include <string.h>       /* For strlen(3).           */
#include <stdint.h>       /* Standard integer types.  */
#include <stdbool.h>      /* Standard boolean type.   */

#include "util/HexOct.h"  /* HexOct utility module.   */


/* -------------------------------------------------------------------------- **
 * Macros
 *
 *  Err()   - Shorthand for (void)fprintf( stderr, ... )
 *  ErrStr  - Shorthand for the error message associated with <errno>.
 *  pl()    - Plural or not?  If so, return "s", else the empty string.
 *            (Useful for presenting output.)
 */

#define Err( ... ) (void)fprintf( stderr, __VA_ARGS__ )
#define ErrStr     (strerror( errno ))
#define pl( X )    ((1==(X))?"":"s")


/* -------------------------------------------------------------------------- **
 * Defined Constants
 *
 *  bSIZE - Read buffer size.
 */

#define bSIZE 4096

/* -------------------------------------------------------------------------- **
 * Typedefs
 *
 *  escpair - Used in testing escape sequence translations.
 */

typedef struct
  {
  int   rawval; /* The expected result.                   */
  char *escstr; /* The escape sequence to be translated.  */
  } escpair;


/* -------------------------------------------------------------------------- **
 * Static Functions
 */

static int HexOct_test( void )
  /* ------------------------------------------------------------------------ **
   * A set of tests intended to verify basic/correct behavior from the
   * HexOct module.
   *
   * The hxo_DumpBufr(), hxo_InitCtx(), hxo_DumpCtx(), and hxo_CloseCtx()
   * functions are all unit tested by compiling and running the test
   * mainline provided in the HexOct.c file.
   * ------------------------------------------------------------------------ **
   */
  {
  int i, failcount = 0;

  /* Test hxo_Xlate{X|O}digit().  */
  int   ret_o, ret_x;
  char *t_str = "0123456789aBcDeFgH";

  for( i = 0; '\0' != t_str[i]; i++ )
    {
    ret_o = hxo_XlateOdigit( (uint8_t)t_str[i] );
    ret_x = hxo_XlateXdigit( (uint8_t)t_str[i] );
    if( !(((i < 8) && (i == ret_o)) || ((i > 7) && (ret_o < 0))) )
      {
      failcount++;
      Err( "hxo_XlateOdigit( %d ) ==> %d\n", i, ret_o );
      }
    if( !(((i < 16) && (i == ret_x)) || ((i > 0xF) && (ret_x < 0))) )
      {
      failcount++;
      Err( "hxo_XlateXdigit( %d ) ==> %d\n", i, ret_x );
      }
    }

  /* Test hxo_UnEscSeq(). */
  int rslt, esc_len;
  escpair escarray[] =  {
    { '~',    "~"     },
    { '\176', "\\176" },
    { '\x7e', "\\~"   },
    { '\176', "\\x7E" },
    { '\a',   "\a"    },
    { '\a',   "\\a"   },
    { '\04',  "\\04"  },
    { '\04',  "\\04z" },
    { '\0',   "\\0"   },
    { '\0',   NULL    } };

  for( i = 0; NULL != escarray[i].escstr; i++ )
    {
    rslt = hxo_UnEscSeq( (uint8_t *)escarray[i].escstr, &esc_len );
    if( rslt != escarray[i].rawval )
      {
      failcount++;
      Err( "hxo_UnEscSeq( \"%s\", [%d] ) ==> 0x%.2x (%d)\n",
           escarray[i].escstr, esc_len, rslt, rslt );
      }
    }

  /* Test hxo_XlateInput(). */
  escpair xlatearray[] =
    { { -1,     ""          },
      { -2,     "0x"        },
      { -2,     "\\FF"      },
      { -3,     "0xQ"       },
      { -3,     "Quack"     },
      { 'Q',    "Q"         },
      { '\xA',  "A"         },
      { '\xA',  "0A"        },
      { '\xA',  "Aluminumy" },
      { '\x1b', "0x1b"      },
      { '\x1c', "<1C>"      },
      { '\x1d', "#1D"       },
      { '\x1e', "%1E"       },
      { '\x1f', "\x1F"      },
      { '\0',   NULL        } };

  for( i = 0; NULL != xlatearray[i].escstr; i++ )
    {
    rslt = hxo_XlateInput( (uint8_t *)xlatearray[i].escstr );
    if( rslt != xlatearray[i].rawval )
      {
      failcount++;
      Err( "hxo_XlateInput( \"%s\" ) ==> 0x%.2x (%d)\n",
           xlatearray[i].escstr, rslt, rslt );
      }
    }

  /* Test hxo_Hexify(), hxo_UnEscStr(), and hxo_HexDumpLn(). */
  char *s = "\0\\0 \a\b \x1e \176\n";
  char  bufr[80];

  i = 2 + (int)strlen( s+1 );   /* Account for the leading NUL. */
  rslt = hxo_Hexify( (uint8_t *)bufr, (uint8_t *)s, (size_t)i );
  rslt = hxo_UnEscStr( (uint8_t *)bufr );
  if( memcmp( bufr, s, (size_t)i ) != 0 )
    {
    failcount++;
    Err( "hxo_UnEscStr( hxo_Hexify( str ) ) != str [%d,%d]\n", 1, rslt );
    }
  rslt = hxo_HexDumpLn( (uint8_t *)bufr, (uint8_t *)s, (size_t)i );
  if( memcmp( bufr, "00 5c 30 20 07 08 20 1e  20 7e 0a 00", 36 ) != 0 )
    {
    failcount++;
    Err( "hxo_HexDumpLn() outout does not match the expected output.\n" );
    }

  /* Finish up. */
  Err( "HexOct unit test:  %d failure%s.\n", failcount, pl(failcount) );
  return( failcount );
  } /* HexOct_test */

/* -------------------------------------------------------------------------- **
 * Program Mainline.
 */

int main( void )
  /* ------------------------------------------------------------------------ **
   * Program Mainline.
   *
   *  Output: The return value is EXIT_SUCCESS (0) unless test failures
   *          occured, in which case EXIT_FAILURE is returned.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  int failcount = 0;

  failcount += HexOct_test();

  Err( "Total:  %d failure%s.\n", failcount, pl( failcount ) );
  return( (failcount > 0) ? EXIT_FAILURE : EXIT_SUCCESS );
  } /* main */

/* ================================== Blip ================================== */
/** @endcond */
